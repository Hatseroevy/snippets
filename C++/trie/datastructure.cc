#include "datastructure.hh"
#include <iostream>



Datastructure::Datastructure()
{
    root_ = initializeNewNode();
    wordCount_ = 0;
}



Datastructure::~Datastructure()
{
    releaseMemory(root_);
}



// Initializes a new node by creating a new node, making it's children aa
// nullpointers and returning the pointer to a created node.

Datastructure::Node *Datastructure::initializeNewNode()
{
    Node* newNode = new Node;
    for( unsigned int i(0); i < 26; ++i )
    {
        newNode->children[i] = nullptr;
    }
    newNode->isWord = false;
    return newNode;
}



// Returns true if word was added and false otherwise.

bool Datastructure::add(std::string &word)
{
    /* Following course pseudocode:
    TRIE-INSERT(p; A; n)
    1   for i := 1 to n do (k�yd��n lis�tt�v�n sanan merkkej� l�pi)
    2       if p->C[A[i]] = NIL then (tarvittaessa lis�t��n uusi solmu)
    3           > varaa uusi solmutietue ja alusta kunkin osoittimen uusi->C[i]
                    arvoksi NIL ja bit->FALSE
    4           p->C[A[i]] := uusi (linkitet��n uusi solmu paikalleen)
    5       p := p->C[A[i]] (siirryt��n alasp�in)
    6   p->bit := TRUE (asetetaan viimeisen solmun totuusarvoksi TRUE)
    */

    // a character to index: static_cast<unsigned int>(myChar) - 97
    // while using letters from 'a' to 'z'

    Node* currentPoint = root_;
    unsigned long index = 0;

    for( unsigned long i(0); i < word.size(); ++i )
    {
        index = static_cast<unsigned long>(word.at(i)) - 97;
        if( currentPoint->children[ index ] == nullptr )
        {
            Node* newNode = initializeNewNode();
            currentPoint->children[ index ] = newNode;
        }
        currentPoint = currentPoint->children[ index ];
    }

    if(currentPoint->isWord){ return false; }

    currentPoint->isWord = true;
    ++wordCount_;
    return true;
}



// Returns true if word was removed and false otherwise.

bool Datastructure::remove(std::string &word)
{
    /* Following the course pseudocode but not releasing memory at this
     * point (rows 7-12). The nodes will be left there even thought the
     * word is removed:
    TRIE-DELETE(p; A; n)
    1   for i := 1 to n do (k�yd��n poistettavan sanan merkkej� l�pi)
    2       if p->C[A[i]] = NIL then (jos valittu haara p��ttyy...)
    3           return FALSE (...tiedet��n, ettei sanaa l�ydy)
    4       P[i] := p (otetaan solmun i is�n osoite talteen)
    5       p := p->C[A[i]] (siirryt��n alasp�in)
    6   p->bit := FALSE (asetetaan l�ydetyn solmun totuusarvoksi FALSE)
    7   i := n
    8   while i > 1 and Leafnode(p) and p->bit = FALSE do
    9       > vapautetaan p:n osoittama alkio
    10      p := P[i] (jatketaan vapauttamista talteen otetusta is�st�)
    11      p->C[A[i]] = NIL (nollataan osoitin poistettuun solmuun)
    12      i := i - 1
    13  return TRUE
    */

    Node* currentPoint = root_;
    unsigned long index = 0;

    for( unsigned long i(0); i < word.size(); ++i )
    {
        index = static_cast<unsigned long>(word.at(i)) - 97;
        if( currentPoint->children[ index ] == nullptr )
        {
            return false;
        }
        currentPoint = currentPoint->children[ index ];
    }

    // The searched word has been there earlier but then it'�'s been deleted,
    // so the removing process is unsuccessful.
    if( !currentPoint->isWord )
    {
        return false;
    }

    // The searched word has been found and it will be deleted.
    currentPoint->isWord = false;
    --wordCount_;
    return true;

}



// Returns true if word was found and false otherwise.

bool Datastructure::search(std::string &word)
{
    /* Following course pseudocode:
    TRIE-SEARCH(p; A; n)
    1   for i := 1 to n do (k�yd��n lis�tt�v�n sanan merkkej� l�pi)
    2       if p->C[A[i]] = NIL then (jos valittu haara p��ttyy...)
    3           return FALSE (...tiedet��n, ettei sanaa l�ydy)
    4       p := p->C[A[i]] (siirryt��n alasp�in)
    5   return p->bit (palautetaan l�ydetyn solmun totuusarvo)
    */

    Node* currentPoint = root_;
    unsigned long index = 0;

    for( unsigned long i(0); i < word.size(); ++i )
    {
        index = static_cast<unsigned long>(word.at(i)) - 97;
        if( currentPoint->children[ index ] == nullptr )
        {
            return false;
        }
        currentPoint = currentPoint->children[ index ];
    }
    return currentPoint->isWord;

}



// Solves and returns amount of words in datastructure.

unsigned long Datastructure::amountOfWords()
{
    return wordCount_;
}



// Empties the datastructure.

void Datastructure::empty()
{
    recursiveEmpty( root_ );
    wordCount_ = 0;
}


// Set all of the Trie-node bit to false.

void Datastructure::recursiveEmpty(Node *pos )
{
    for( unsigned int i(0); i < 26; ++i )
    {
        if( pos->children[i] != nullptr )
        {
            recursiveEmpty( pos->children[i] );
        }
    }
    pos->isWord = false;
}




// A recursive method used while emptying the structure.
// Goes through the nodes and their children to the tip of the branches
// and destroys them while coming back.

void Datastructure::releaseMemory(Datastructure::Node *pos)
{
    for( unsigned int i(0); i < 26; ++i )
    {
        if( pos->children[i] != nullptr )
        {
            releaseMemory( pos->children[i] );
        }
    }
    delete pos;
    pos = nullptr;
}



// Prints contents of the data structure.

void Datastructure::print()
{
    if( wordCount_ != 0 ){
        recursivePrint(root_, "");
        std::cout << std::endl;
    }
}



// A recursive method used while printing the structure.
// Parameters: pos = the node where we currently are
//             prefix = the word "collected" so far meaning the letters
//              the recursion has gone throught

void Datastructure::recursivePrint(Node *pos, std::string prefix)
{
    char newChar;
    for( unsigned int i(0); i < 26; ++i )
    {
        if( pos->children[i] != nullptr )
        {
            newChar = static_cast<char>(i+97);
            if( pos->children[i]->isWord )
            {
                std::cout << prefix+newChar << SEPARATOR;
            }
            recursivePrint( pos->children[i], prefix+newChar );
        }
    }
}

