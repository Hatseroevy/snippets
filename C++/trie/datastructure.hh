// Definition of Datastructure class for UDS/Tiraka homework 2 trie.
//
#ifndef DATASTRUCTURE_HH
# define DATASTRUCTURE_HH

# include <string>

// SEPARATOR is recommended to use when printing contents of the data
// structure
const char SEPARATOR = ' ';

class Datastructure
{

 public:

   Datastructure();

   ~Datastructure();

   // Returns true if word was added and false otherwise.
   bool add(std::string& word);

   // Returns true if word was removed and false otherwise.
   bool remove(std::string& word);

   // Returns true if word was found and false otherwise.
   bool search(std::string& word);

   // Solves and returns amount of words in datastructure.
   unsigned long amountOfWords();

   // Empties the datastructure.
   void empty();

   // Prints contents of the data structure.
   void print();

 private:
   // Copy constructor is forbidden
   Datastructure(const Datastructure&);
   // Assignment operator is forbidden
   Datastructure& operator=(const Datastructure&);

   // Students implementation comes here
   struct Node
   {
       Node* children[26];
       bool isWord;
   };

   // Initializes a new node by creating a new node, making it's children aa
   // nullpointers and returning the pointer to a created node.
   Node* initializeNewNode();

   // A recursive method used while printing the structure.
   // Parameters: pos = the node where we currently are
   //             prefix = the word "collected" so far meaning the letters
   void recursivePrint(Node* pos, std::string prefix);

   // Set all of the Trie-node bit to false.
   void recursiveEmpty(Node *pos);

   // A recursive method used while emptying the structure.
   // Goes through the nodes and their children to the tip of the branches
   // and destroys them while coming back.
   void releaseMemory(Node *pos);

   Node* root_;
   unsigned long wordCount_;
   
};

#endif
