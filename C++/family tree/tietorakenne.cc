#include <iostream>
#include "tietorakenne.hh"

Tietorakenne::Tietorakenne()
{
}


Tietorakenne::~Tietorakenne()
{
    // Poista-metodia kutsutaan mainissa, joten t��ll�
    // ei tarvitse kutsua tyhjennysmetodeja.
}


bool Tietorakenne::lisaa(Sukupuoli sukupuoli, string nimi, int ID, int synnyinvuosi, int kuolinvuosi)
{
    // Tarkistetaan l�ytyyk� jo annetun ID:n mukaista henkil��.
    auto it = sukupuu_.find( ID );

    if( it != sukupuu_.end() )
    {
       // ID l�ytyy jo sukupuusta, mit��n ei lis�t� sukupuuhun.
       return false;
    }

    // Annetulta ID:lt� ei l�ydy henkil��, joten luodaan solmu
    // annetuista tiedoista.
    Solmu* uusiSolmu = new Solmu;
    uusiSolmu->henkilo = new Henkilo;
    uusiSolmu->henkilo->sukupuoli = sukupuoli;
    uusiSolmu->henkilo->nimi = nimi;
    uusiSolmu->henkilo->aiti = nullptr;
    uusiSolmu->henkilo->isa = nullptr;
    uusiSolmu->henkilo->synnyinvuosi = synnyinvuosi;
    uusiSolmu->henkilo->kuolinvuosi = kuolinvuosi;
    uusiSolmu->tutkittu = false;

    // Lis�t��n solmu sukupuuhun.
    sukupuu_.insert( std::pair< int, Solmu* >( ID, uusiSolmu ) );
    return true;

}



// Etsii liiton kahden solmun v�lilt� ja palauttaa sen.
// Jos ei l�ydy, palauttaa nullptr.
Tietorakenne::Liitto* Tietorakenne::haeLiitto( const Solmu* s1, const Solmu* s2 )
{
   Liitto* liitto = nullptr;

   // Tutkitaan l�ytyyk� liittoa jo. Toisen osapuolen liittojen
   // tutkiminen riitt��, koska tieto on molemmilla.
   for( auto it = s1->henkilo->liitot.begin();
        it != s1->henkilo->liitot.end(); ++it)
   {
       if( (*it)->h1 == s2 || (*it)->h2 == s2 )
       {
          // L�ytyi, ei tutkita sen enemp��.
          liitto = (*it);
          break;
       }
   }
   return liitto;
}



// Luo liiton kahden solmun v�lille ja palauttaa sen.
Tietorakenne::Liitto* Tietorakenne::luoLiitto( Solmu* s1, Solmu* s2 )
{
   Liitto* liitto = new Liitto();
   liitto->h1 = s1;
   liitto->h2 = s2;
   // ei sortattavaa = sortattu.
   liitto->lapsetSortattu = true;

   // Molemmat saavat tiedon liitosta haltuunsa.
   s1->henkilo->liitot.push_back( liitto );
   s2->henkilo->liitot.push_back( liitto );

   return liitto;
}



// Valmiiksi annettujen parametrien nimist� huolimatta, liittoa luodessa
// voidaan henkil�iden id:t sy�tt�� kummin p�in tahansa,
// joten IDNainen ei ole aina nainen eik� IDMies ole aina mies.
bool Tietorakenne::lisaaLiitto(int IDMies, int IDNainen)
{
    auto miesIt = sukupuu_.find( IDMies );
    auto nainenIt = sukupuu_.find( IDNainen );

    if( miesIt == sukupuu_.end() || nainenIt == sukupuu_.end() )
    {
        // Ei l�ydy kaikkia liittoon tarvittavia henkill�it�.
        return false;
    }

    // Etsit��n olemassa olevaan liittoa.
    Liitto* liitto = haeLiitto( miesIt->second, nainenIt->second );
    if( liitto != nullptr )
    {
       // Liitto l�ytyy jo.
       return true;
    }

    // Liittoa ei ole viel�, joten muodostetaan sallainen.
    luoLiitto( miesIt->second, nainenIt->second);
    return true;

}


// Lisaa lapsen annetuille henkiloille, jos se suinkin on mahdollista.
bool Tietorakenne::lisaaLapsi(int IDLapsi, int IDNainen, int IDMies)
{
    auto lapsiIt = sukupuu_.find( IDLapsi );
    auto miesIt = sukupuu_.find( IDMies );
    auto nainenIt = sukupuu_.find( IDNainen );

    // Tarkistetaan l�ytyyk� kaikilla id:ill� henkil�t.
    if( miesIt == sukupuu_.end() || nainenIt == sukupuu_.end() ||
            lapsiIt == sukupuu_.end() )
    {
        return false;
    }

    // Jos vanhemmat ovat v��r�� sukupuolta tai lapsella on jo vanhemmat
    // (t�ss� riitt�� toisen vanhemman tarkastelu), ei voida tehd� mit��n.
    if( nainenIt->second->henkilo->sukupuoli != NAINEN ||
            miesIt->second->henkilo->sukupuoli != MIES ||
            lapsiIt->second->henkilo->isa != nullptr )
    {
        return false;
    }

    // Kaikki tarvittavat henkil�t l�ytyv�t joten muodostetaan suhteet.

    // Lapselle vanhemmat.
    lapsiIt->second->henkilo->isa = miesIt->second;
    lapsiIt->second->henkilo->aiti = nainenIt->second;

    // Tarkistetaan onko henkil�ill� jo liittoa.
    Liitto* liitto = haeLiitto( miesIt->second, nainenIt->second );

    // Jos liittoa ei ole viel�, tehd��n uusi.
    if( liitto == nullptr )
    {
        liitto = luoLiitto( miesIt->second, nainenIt->second );
    }

    // Lapsi liittoon mukaan.
    liitto->lapset.push_back( lapsiIt->second );
    liitto->lapsetSortattu = false;
    return true;

}


// Selvittaa kahden henkilon sukulaisuuden.
Kysely Tietorakenne::kysely(int ID1, int ID2)
{
    auto it1 = sukupuu_.find(ID1);
    auto it2 = sukupuu_.find(ID2);

    // Etsit��n id:t.
    if( it1 == sukupuu_.end() || it2 == sukupuu_.end() )
    {
        return INVALID_ID;
    }

    // Itse on itsens� sukulainen. Ei menn� turhaan tutkimaan enemp��.
    if(it1 == it2)
    {
        return SUKUA;
    }

    // Etsit��n kantavanhemmat molemmille, joita verrataan.
    bool sukua = false;
    sukua = sukututkimus( it1->second, it2->second );
    if( sukua )
    {
       return SUKUA;
    }

    return EI_SUKUA;

}


bool Tietorakenne::sukututkimus(Tietorakenne::Solmu *s1, Tietorakenne::Solmu *s2)
{
    bool sukua = false;

    // Ensimm�inen koskettelee itse��n, vanhempiaan, isovanhempiaan jne.
    kaannaBitit( s1, true );
    // Toinen tutkii onko h�nt�, vanhempia, isovanhempia jne. kosketeltu
    // ja ilmaisee tuloksen sukua-booleanissa.
    etsiBitti( s2, sukua );
    // Ensimm�inen piilottelee hiplailujaan.
    kaannaBitit( s1, false );

    return sukua;

}


// Kosketaan kaikkia, joista oma DNA on per�isin.
void Tietorakenne::kaannaBitit( Solmu* solmu, bool bitti )
{
    // K��nt�� bitin siihen mit� sen halutaan olevan.
    solmu->tutkittu = bitti;
    // Kosketaan kaikkia vanhempia ja niiden vanhem.
    if( solmu->henkilo->isa != nullptr)
    {
       kaannaBitit( solmu->henkilo->isa, bitti );
    }
    if( solmu->henkilo->aiti != nullptr)
    {
       kaannaBitit( solmu->henkilo->aiti, bitti );
    }

}


// Kiivet��n omia vanhempia pitkin yl�s, jos joku on
// ensimm�isess� vaiheessa tutkittu, on sukulaisuus varmennettu.
void Tietorakenne::etsiBitti( Solmu* solmu, bool& sukua )
{
   if( solmu->tutkittu ){
      sukua = true;
      return;
   }
   if( solmu->henkilo->isa != nullptr )
   {
      etsiBitti( solmu->henkilo->isa, sukua );
   }
   if( solmu->henkilo->aiti != nullptr )
   {
      etsiBitti( solmu->henkilo->aiti, sukua );
   }
}


// Vapautetaan muisti ja laitetaan osoittimia nollapointtereihin
// hyvien tapojen mukaan.
void Tietorakenne::poista()
{
    Henkilo* poistettava;

    while( !sukupuu_.empty() ){
        auto solmuIt = sukupuu_.begin();
        poistettava = solmuIt->second->henkilo;

        // Vapautetaan liittojen muisti vain toistelta puolelta.
        if( poistettava->sukupuoli == MIES )
        {
            for( auto liittoIt = poistettava->liitot.begin();
                liittoIt != poistettava->liitot.end(); ++liittoIt )
            {
                (*liittoIt)->h1 = nullptr;
                (*liittoIt)->h2 = nullptr;
                (*liittoIt)->lapset.clear();
                 delete (*liittoIt);
                (*liittoIt) = nullptr;
            }
        }
        poistettava->liitot.clear();
        delete poistettava;
        poistettava = nullptr;
        delete solmuIt->second;
        sukupuu_.erase( solmuIt );
    }
}


// Tulostaa annetun henkil�n sukupuun.
// Puolisot lis�ysj�rjestyksess�, lapset ik�j�rjestyksess�.
void Tietorakenne::tulosta(int ID)
{
    auto it = sukupuu_.find(ID);

    // Jos ei l�ydy, tulostetaan vain 'V'.
    if( it == sukupuu_.end() )
    {
        std::cout << "V" << std::endl;
        return;
    }

    std::cout << std::endl;
    // Menn��n penkomaan suku l�pitte.
    rekursiivinenTulosta( it->second, 0 );
}


// Tulostaa annetun henkil�n suhteet.
// Puolisot lis�ysj�rjestyksess�, lapset ik�j�rjestyksess�.
// Tarvittaessa j�rjestet��n lapset ennen tulostusta.
// syvyys-parametrill� saadaan aikaan oikean mittainen sisennys.
void Tietorakenne::rekursiivinenTulosta(const Tietorakenne::Solmu *paikka, unsigned int syvyys)
{
    // Sisennys kuntoon.
    std::cout << std::string(4*syvyys, ' ');
    tulostaHenkilotiedot( paikka->henkilo );

    Henkilo* puoliso;
    for( auto it = paikka->henkilo->liitot.begin();
         it != paikka->henkilo->liitot.end(); ++it)
    {

        // Katsotaan ensiksi kummassa paikassa (h1 vai h2) puolison tiedot
        // on, ettei tulosteta omia tietoja uudelleen.
        if( (*it)->h1 == paikka )
        {
            puoliso = (*it)->h2->henkilo;
        }
        else
        {
            puoliso = (*it)->h1->henkilo;
        }
        std::cout << std::string(4*syvyys, ' ') << "& ";
        tulostaHenkilotiedot( puoliso );

        // Sitten lasten tiedot

        // Ennen tulostetua j�rjestet��n mukulat syntym�vuoden mukaan,
        // jos on tarvetta.
        if( !(*it)->lapsetSortattu )
        {
            (*it)->lapset.sort( [](const Solmu *a, const Solmu *b){
              return (a->henkilo->synnyinvuosi < b->henkilo->synnyinvuosi);} );
            (*it)->lapsetSortattu = true;
        }

        // Edet��n lapsiin.
        for( auto lit = (*it)->lapset.begin(); lit != (*it)->lapset.end(); ++lit)
        {
            rekursiivinenTulosta( (*lit), syvyys+1 );
        }
    }  
}

void Tietorakenne::tulostaHenkilotiedot(const Tietorakenne::Henkilo *hlo)
{
     std::cout << hlo->nimi << " " << hlo->synnyinvuosi << " - ";
     // Tulostetaan kuolinvuosi vain jos sellainen on.
     // (eli kuolinvuosi ei ole nolla)
     if( hlo->kuolinvuosi != 0 )
     {
         std::cout << hlo->kuolinvuosi;
     }
     std::cout << std::endl;
}


