// tietorakenteen rajapinnan maarittely

#ifndef TIETORAKENNE_H
#define TIETORAKENNE_H

#include <string>
#include <list>
#include <map>

using std::string;

enum Sukupuoli { MIES, NAINEN };

enum Kysely { INVALID_ID, SUKUA, EI_SUKUA };

class Tietorakenne {

public:

  Tietorakenne();
  ~Tietorakenne();

  //lisaa uuden henkilon tietorakenteeseen
  bool lisaa(Sukupuoli sukupuoli, string nimi, int ID, int synnyinvuosi,
             int kuolinvuosi);

  //lisaa liiton kahden henkilon valille
  bool lisaaLiitto(int IDMies, int IDNainen);

  //lisaa lapsen annetuille henkiloille
  bool lisaaLapsi(int IDLapsi, int IDNainen, int IDMies);

  //selvittaa kahden henkilon sukulaisuuden
  Kysely kysely(int ID1, int ID2);

  //tyhjentaa tietorakenteen
  void poista();

  //tulostaa sukupuun alkaen annetusta henkiloista
  void tulosta(int ID);

private:

  struct Liitto;
  struct Solmu;
  struct Henkilo{
      Sukupuoli sukupuoli;
      string nimi;
      int synnyinvuosi;
      int kuolinvuosi;
      Solmu* isa;
      Solmu* aiti;
      // Henkil�n puolisosuhteet.
      std::list<Liitto*> liitot;
  };

  struct Liitto{
      // Liitossa olevat henkil�t
      Solmu* h1;
      Solmu* h2;
      // ja heid�n lapsensa.
      std::list<Solmu*> lapset;
      // Kertoo onko lapset jo j�rjestetty i�n mukaan.
      // Est�� turhan sorttaamisen.
      bool lapsetSortattu;
  };

  struct Solmu{
      Henkilo* henkilo;
      bool tutkittu;
  };

  // Etsii liiton annettujen solmujen v<E4>lilt<E4> ja palauttaa sen.
  // Jos liittoa ei l<F6>ydy, palauttaa nullptr
  Liitto* haeLiitto( const Solmu* s1, const Solmu* s2 );

  // Luo uuden liiton kahden solmun v<E4>lille ja palauttaa sen.
  Liitto* luoLiitto( Solmu* s1, Solmu* s2 );

  // Tulostaa annetun solmun suvun.
  // Tarpeen tullen my�s j�rjest�� lapset ik�j�rjestykseen.
  void rekursiivinenTulosta(const Solmu* paikka , unsigned int syvyys);

  void tulostaHenkilotiedot( const Henkilo* hlo );

  // Tutkii kahden henkil�n sukulaisuutta.
  bool sukututkimus(Solmu *s1 , Solmu *s2);

  // K��nt�� annetun solmun ja sen vanhempien tutkittu-bitin
  // parametrina annetuksi bitiksi
  void kaannaBitit(Solmu *solmu, bool bitti);

  // Etsii solmun vanhemmista trueksi k��nnetty� tutkittu-bitti�.
  // Kun sellainen l�ytyy, sukua-parametri vaihdetaan trueksi.
  void etsiBitti(Solmu *solmu, bool &sukua);

  // Integeriin tallennetaan henkil�n ID ja Solmussa on loput tiedot henkil�st�.
  // ID on yksiselitteinen, joten mapissa sit� k�ytet��n avaimena,
  // jolla henkil�it� haetaan.
  std::map<int, Solmu*> sukupuu_;

  // Kopiorakentaja estetty
  Tietorakenne(const Tietorakenne&);
  // Sijoitusoperaattori estetty
  Tietorakenne& operator=(const Tietorakenne&);
};

#endif
