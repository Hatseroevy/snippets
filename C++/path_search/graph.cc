#include "graph.hh"

Graph::Graph()
{
    for ( unsigned int x(0); x < GRAPH_WIDTH; ++x )
    {
        for( unsigned int y(0); y < GRAPH_HEIGHT; ++y )
        {

            std::vector< Graph::Location > neighbours;

            // neighbour in north
            if( y > 0 ){
                neighbours.push_back( Graph::Location( x, y-1 ) );
            }

            // neighbour in east
            if( x < GRAPH_WIDTH-1 ){
                neighbours.push_back( Graph::Location( x+1, y ) );
            }

            // neighbour in south
            if( y < GRAPH_HEIGHT-1 ){
                neighbours.push_back( Graph::Location( x, y+1 ) );
            }

            // neighbour in west
            if( x > 0 ){
                neighbours.push_back( Graph::Location( x-1, y ) );
            }

            edges_.emplace( Graph::Location( x, y ).id(), neighbours );
            weights_.emplace( Graph::Location( x,y ).id(), 1 );

        }
    }

    for( auto loc: edges_ )
    {
        std::vector<Graph::Location> edges = loc.second;
    }
}

Graph::~Graph(){
}

std::vector< Graph::Location > Graph::neighbours( Graph::Location location )
{
    std::vector<Graph::Location> edges = edges_.at( location.id() );
    return edges_.at( location.id() ); 
}

std::vector<Graph::Location> Graph::neighbours(unsigned int location)
{
    return edges_.at( location );
}

int Graph::cost(unsigned int id1, unsigned int id2 )
{
    std::vector<Graph::Location> neighbours = edges_.at(id1);

    for( unsigned int i(0); i < neighbours.size() ; ++i )
    {
        if( neighbours.at(i).id() == id2 ){
            return weights_.at(id2);
        }
    }

    return -1;
}

void Graph::setWeight( unsigned int id, int weight )
{
    weights_.at( id ) = weight;
}

unsigned Graph::getWeight(unsigned int id)
{
    return weights_.at(id);
}

