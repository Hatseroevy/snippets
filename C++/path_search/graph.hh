#ifndef GRAPH_HH
#define GRAPH_HH

#include <vector>
#include <unordered_map>

const unsigned int GRAPH_WIDTH = 30;
const unsigned int GRAPH_HEIGHT = 20;



class Graph
{
public:
    Graph();
    ~Graph();
    struct Location {
        unsigned int x_;
        unsigned int y_;
        int weight_;
        // -1 = infinite = unpassable
        Location( unsigned int x, unsigned int y, int weight = 1 ){ x_ = x; y_ = y; weight_ = weight; }
        unsigned int id(){ return x_ + y_*GRAPH_WIDTH; }
    };
    std::vector< Graph::Location > neighbours( Location location );
    std::vector< Graph::Location > neighbours( unsigned int location );
    int cost( unsigned int id1, unsigned int id2 );
    void setWeight(unsigned int id , int weight);
    unsigned getWeight(unsigned int id);

private:

    // location id, weight
    std::unordered_map< unsigned int, int > weights_;
    std::unordered_map< unsigned int, std::vector<Graph::Location> > edges_;
};

#endif // GRAPH_HH
