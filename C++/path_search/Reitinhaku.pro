#-------------------------------------------------
#
# Project created by QtCreator 2015-09-08T01:46:11
#
#-------------------------------------------------

QT       += core gui


CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Reitinhaku
TEMPLATE = app


SOURCES += main.cc\
        mainwindow.cc \
    searcharea.cc \
    guinode.cc \
    graph.cc

HEADERS  += mainwindow.hh \
    searcharea.hh \
    guinode.hh \
    graph.hh

FORMS    += mainwindow.ui
