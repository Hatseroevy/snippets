#ifndef NODE_HH
#define NODE_HH

#include <QGraphicsObject>

const unsigned int NODEWIDTH = 20;
const unsigned int NODEHEIGHT = 20;

const int WEIGHT_ONE = 1;
const int WEIGHT_TWO = 3;
const int WEIGHT_THREE = 5;
const int NOT_PASSABLE = -1;

struct Coordinate {
    unsigned int x;
    unsigned int y;
};

class GUINode : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit GUINode(unsigned int xCoord, unsigned int yCoord, int id, QGraphicsObject *parent = 0);

    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*);
    void flipStart();
    void flipGoal();
    void flipPassable();
    bool getPassable();
    void setVisited(bool visited);
    void setWaiting( bool waiting );
    void setIsPath(bool isPath);
    unsigned int getId();
    void setWeight( int weight );
    int getWeight();
    Coordinate* getCoordinate();


signals:
    void pressedNode( GUINode* node );
    void addVisited();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

public slots:

private:
    Coordinate* coord_;
    bool isPassable_;
    bool isStart_;
    bool isGoal_;
    bool visited_;
    bool waiting_;
    bool isPath_;
    int weight_;
    int id_;
};

#endif // NODE_HH
