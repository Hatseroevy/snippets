#include "guinode.hh"
#include <QPainter>

GUINode::GUINode(unsigned int xCoord, unsigned int yCoord, int id, QGraphicsObject *parent) :
    QGraphicsObject(parent), coord_( new Coordinate ), isPassable_(true), isStart_(false),
    isGoal_(false), visited_(false), waiting_(false), isPath_(false), weight_(1), id_(id)
{
    coord_->x = xCoord;
    coord_->y = yCoord;
    setPos( coord_->x*NODEWIDTH, coord_->y*NODEHEIGHT );
    setZValue( coord_->y );
}

QRectF GUINode::boundingRect() const
{
    return QRectF( 0, 0, NODEWIDTH, NODEHEIGHT );
}

void GUINode::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setPen(Qt::black);

    if( isStart_ ) {
        painter->setBrush( QColor(0, 200-weight_*5, 0 ));
    }
    else if ( isGoal_ ) {
        painter->setBrush( Qt::blue );
    }
    else if( isPath_ ) {
        painter->setBrush( QColor(255-weight_*5, 0, 0 ));
    }
    else if( !isPassable_ ) {
        painter->setBrush( Qt::black);
    }
    else if( visited_) {
        painter->setBrush( QColor(255-weight_*5, 150-weight_*5, 0 ));
    }
    else if( waiting_ ) {
        painter->setBrush( QColor( 220-weight_*5, 250-weight_*5, 0 ));
    }
    else  {
        if( weight_ == WEIGHT_ONE ) {
            painter->setBrush( Qt::white );
        }
        else {
            painter->setBrush( QColor(255-weight_*5,255- weight_*5, 255-weight_*5 ));
        }
    }

    painter->drawRect( 0, 0, NODEWIDTH, NODEHEIGHT );
}

void GUINode::flipStart()
{
    isStart_ = !isStart_;
}

void GUINode::flipGoal()
{
    isGoal_ = !isGoal_;
}

void GUINode::flipPassable()
{
    isPassable_ = !isPassable_;
}

bool GUINode::getPassable()
{
    return isPassable_;
}

void GUINode::setVisited( bool visited )
{
    if( !visited_ && visited ) {
        emit addVisited();
    }
    visited_ = visited;
}

void GUINode::setWaiting(bool waiting)
{
    waiting_ = waiting;
}

void GUINode::setIsPath(bool isPath)
{
    isPath_ = isPath;
}


Coordinate *GUINode::getCoordinate()
{
    return coord_;
}

unsigned int GUINode::getId()
{
    return id_;
}

void GUINode::setWeight(int weight)
{
    weight_ = weight;
}

int GUINode::getWeight()
{
    return weight_;
}

void GUINode::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    emit pressedNode( this );
}
