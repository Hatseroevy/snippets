/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <searcharea.hh>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    SearchArea *graphicsView;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *searchButton;
    QComboBox *comboBox;
    QPushButton *setStartButton;
    QPushButton *setGoalButton;
    QPushButton *setWeightButton;
    QPushButton *setPassableButton;
    QPushButton *removeWeightButton;
    QPushButton *removePassableButton;
    QPushButton *resetButton;
    QLabel *routeLenLabel;
    QLabel *routeLenValue;
    QLabel *visitedAmountLabel;
    QLabel *visitedAmontValue;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(900, 675);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        graphicsView = new SearchArea(centralWidget);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));
        graphicsView->setGeometry(QRect(20, 10, 771, 500));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(800, 10, 91, 330));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        searchButton = new QPushButton(verticalLayoutWidget);
        searchButton->setObjectName(QStringLiteral("searchButton"));

        verticalLayout->addWidget(searchButton);

        comboBox = new QComboBox(verticalLayoutWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        verticalLayout->addWidget(comboBox);

        setStartButton = new QPushButton(verticalLayoutWidget);
        setStartButton->setObjectName(QStringLiteral("setStartButton"));

        verticalLayout->addWidget(setStartButton);

        setGoalButton = new QPushButton(verticalLayoutWidget);
        setGoalButton->setObjectName(QStringLiteral("setGoalButton"));

        verticalLayout->addWidget(setGoalButton);

        setWeightButton = new QPushButton(verticalLayoutWidget);
        setWeightButton->setObjectName(QStringLiteral("setWeightButton"));

        verticalLayout->addWidget(setWeightButton);

        setPassableButton = new QPushButton(verticalLayoutWidget);
        setPassableButton->setObjectName(QStringLiteral("setPassableButton"));

        verticalLayout->addWidget(setPassableButton);

        removeWeightButton = new QPushButton(verticalLayoutWidget);
        removeWeightButton->setObjectName(QStringLiteral("removeWeightButton"));

        verticalLayout->addWidget(removeWeightButton);

        removePassableButton = new QPushButton(verticalLayoutWidget);
        removePassableButton->setObjectName(QStringLiteral("removePassableButton"));

        verticalLayout->addWidget(removePassableButton);

        resetButton = new QPushButton(verticalLayoutWidget);
        resetButton->setObjectName(QStringLiteral("resetButton"));

        verticalLayout->addWidget(resetButton);

        routeLenLabel = new QLabel(verticalLayoutWidget);
        routeLenLabel->setObjectName(QStringLiteral("routeLenLabel"));

        verticalLayout->addWidget(routeLenLabel);

        routeLenValue = new QLabel(verticalLayoutWidget);
        routeLenValue->setObjectName(QStringLiteral("routeLenValue"));

        verticalLayout->addWidget(routeLenValue);

        visitedAmountLabel = new QLabel(verticalLayoutWidget);
        visitedAmountLabel->setObjectName(QStringLiteral("visitedAmountLabel"));

        verticalLayout->addWidget(visitedAmountLabel);

        visitedAmontValue = new QLabel(verticalLayoutWidget);
        visitedAmontValue->setObjectName(QStringLiteral("visitedAmontValue"));

        verticalLayout->addWidget(visitedAmontValue);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 900, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        searchButton->setText(QApplication::translate("MainWindow", "Aloita haku", 0));
        setStartButton->setText(QApplication::translate("MainWindow", "Aseta alku", 0));
        setGoalButton->setText(QApplication::translate("MainWindow", "Aseta maali", 0));
        setWeightButton->setText(QApplication::translate("MainWindow", "Vaihda paino", 0));
        setPassableButton->setText(QApplication::translate("MainWindow", "Vaihda este", 0));
        removeWeightButton->setText(QApplication::translate("MainWindow", "Poista painot", 0));
        removePassableButton->setText(QApplication::translate("MainWindow", "Poista esteet", 0));
        resetButton->setText(QApplication::translate("MainWindow", "Tyhjenn\303\244", 0));
        routeLenLabel->setText(QApplication::translate("MainWindow", "Route length:", 0));
        routeLenValue->setText(QString());
        visitedAmountLabel->setText(QApplication::translate("MainWindow", "Visited total:", 0));
        visitedAmontValue->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
