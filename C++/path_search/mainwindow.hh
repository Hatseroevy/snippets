#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

enum SearchType{
    DIJKSTRA,
    A,
    GREEDY
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onSearchPressed();
    void setRouteLen( unsigned int value );
    void setVisitedAmount( unsigned int value );

private:
    Ui::MainWindow *ui;
    SearchType algorithm_;
};

#endif // MAINWINDOW_HH
