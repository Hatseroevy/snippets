Author: Nina Anttila

A Qt project made to visualize how path search algorithms (Dijsktra, Greedy best-first, A*) check their surroundings.

Dijsktras and A*'s original implementation from here: http://www.redblobgames.com/pathfinding/a-star/implementation.html#cplusplus

UI in finnish.