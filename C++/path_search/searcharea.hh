#ifndef SEARCHAREA_HH
#define SEARCHAREA_HH

#include <QGraphicsView>
#include <vector>
//#include <unordered_map>
#include "graph.hh"
#include <queue>
#include <utility>

class GUINode;
struct Coordinate;


template<typename T, typename Number=int>
struct PriorityQueue {
  typedef std::pair<Number, T> PQElement;

  class ElementCompare {
  public:
      bool operator ()(const PQElement& le, const PQElement& re ){
          return (le.first > re.first);
      }
  };

  std::priority_queue< PQElement, std::vector<PQElement >, ElementCompare > elements;

  inline bool empty() { return elements.empty(); }

  inline void put(T item, Number priority) {
    PQElement element = std::make_pair(priority, item);
    elements.emplace( element );
  }

  inline T get() {
    T best_item = elements.top().second;
    elements.pop();
    return best_item;
  }
};


class SearchArea: public QGraphicsView
{
    Q_OBJECT

public:

    explicit SearchArea(QWidget *parent = 0);
    ~SearchArea();
    void resetDrawArea();
    void dijkstra(Graph* graph, Graph::Location start, Graph::Location goal,
                   std::unordered_map<unsigned int, unsigned int> &came_from,
                   std::unordered_map<unsigned int, int> &cost_so_far );
    void a_star_search(Graph* graph, Graph::Location start, Graph::Location goal,
                       std::unordered_map<unsigned int, unsigned int> &came_from,
                       std::unordered_map<unsigned int, int> &cost_so_far );
    void startSearch( QString searchtype );
    void greedy_search(Graph *graph, Graph::Location start, Graph::Location goal, std::unordered_map<unsigned int, unsigned int> &came_from);

signals:
    void routeLength( unsigned int value );
    void visitedAmount( unsigned int value );

public slots:
    void onNodePressed(GUINode *node );
    void onSetStartPressed();
    void onSetGoalPressed();
    void onSetWeight();
    void onSetPassable();
    void onRemovePassables();
    void onRemoveWeights();
    void addVisited();
    void onReset();

private:
    // Alue, jolle nodet piirtyvät
    QGraphicsScene* drawArea_;
    // Nodet
    std::vector< std::vector <GUINode*> > guinodes_;
    Graph* graph_;
    GUINode* start_;
    GUINode* goal_;
    bool setStart_;
    bool setGoal_;
    bool setWeight_;
    bool setPassable_;
    unsigned int visited_;
};

#endif // SEARCHAREA_HH
