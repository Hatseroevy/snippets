#include "mainwindow.hh"
#include "ui_mainwindow.h"
#include "graph.hh"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), algorithm_(DIJKSTRA)
{
    ui->setupUi(this);
    ui->comboBox->insertItem(0, "Dijkstra");
    ui->comboBox->insertItem(1, "A*");
    ui->comboBox->insertItem(2, "Greedy");

    QObject::connect( ui->searchButton, SIGNAL(clicked()), this, SLOT(onSearchPressed()));
    QObject::connect( ui->setStartButton, SIGNAL(clicked()), ui->graphicsView, SLOT(onSetStartPressed()));
    QObject::connect( ui->setGoalButton, SIGNAL(clicked()), ui->graphicsView, SLOT(onSetGoalPressed()));
    QObject::connect( ui->setWeightButton, SIGNAL(clicked()), ui->graphicsView, SLOT(onSetWeight()) );
    QObject::connect( ui->setPassableButton, SIGNAL(clicked()), ui->graphicsView, SLOT(onSetPassable()) );
    QObject::connect( ui->removePassableButton, SIGNAL(clicked()), ui->graphicsView, SLOT(onRemovePassables()) );
    QObject::connect( ui->removeWeightButton, SIGNAL(clicked()), ui->graphicsView, SLOT(onRemoveWeights()) );
    QObject::connect( ui->graphicsView, SIGNAL(routeLength( unsigned int )), this, SLOT(setRouteLen( unsigned int )) );
    QObject::connect( ui->graphicsView, SIGNAL(visitedAmount( unsigned int )), this, SLOT(setVisitedAmount( unsigned int )) );
    QObject::connect( ui->resetButton, SIGNAL(clicked()), ui->graphicsView, SLOT(onReset()) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onSearchPressed()
{
    ui->comboBox->currentText();
    ui->graphicsView->startSearch( ui->comboBox->currentText() );
}

void MainWindow::setRouteLen(unsigned int value)
{
    ui->routeLenValue->setText( QString::number( value ) );
}

void MainWindow::setVisitedAmount(unsigned int value)
{
    ui->visitedAmontValue->setText( QString::number( value ) );
}
