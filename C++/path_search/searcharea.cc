#include "searcharea.hh"
#include "guinode.hh"
#include <QDebug>
#include <QThread>
#include <cmath>

SearchArea::SearchArea( QWidget *parent ) :
    QGraphicsView(parent), drawArea_( new QGraphicsScene(this) ), setStart_(false),
    setGoal_(false), setWeight_(0), setPassable_(0), visited_(0)
{
    graph_ = new Graph();
    setScene( drawArea_ );
    drawArea_->setSceneRect( 0, 0, NODEWIDTH*GRAPH_WIDTH+2, NODEHEIGHT*GRAPH_HEIGHT+2);
    drawArea_->setBackgroundBrush( Qt::gray );

    for( unsigned int i(0); i < GRAPH_WIDTH; ++i)
    {
        std::vector< GUINode* > nodes;

        for( unsigned int j(0); j < GRAPH_HEIGHT; ++j)
        {
            GUINode* node = new GUINode(i, j, (j*GRAPH_WIDTH+i) );
            QObject::connect( node, SIGNAL( pressedNode( GUINode*) ), this, SLOT( onNodePressed( GUINode*) ) );
            QObject::connect( node, SIGNAL( addVisited() ), this, SLOT( addVisited() ) );
            drawArea_->addItem( node );
            nodes.push_back( node );
        }

        guinodes_.push_back( nodes );
    }

    guinodes_.at(0).at(0)->flipStart();
    start_ = guinodes_.at(0).at(0);
    guinodes_.at(0).at(0)->flipGoal();
    goal_ = guinodes_.at(0).at(0);

    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
    show();


}

SearchArea::~SearchArea()
{
    for( unsigned int i( GRAPH_WIDTH ); i > 0; --i )
    {
        for( unsigned int j( GRAPH_HEIGHT ); j > 0; --j)
        {
            GUINode* node = guinodes_.at(i-1).at(j-1);
            delete node;
            node = nullptr;
        }

        guinodes_.at(i-1).clear();
    }

    start_ = nullptr;
    goal_ = nullptr;
}

void SearchArea::resetDrawArea()
{
    for( unsigned int x(0); x < GRAPH_WIDTH; ++x )
    {
        for( unsigned int y(0); y < GRAPH_HEIGHT; ++y )
        {
            guinodes_.at(x).at(y)->setVisited(false);
            guinodes_.at(x).at(y)->setIsPath(false);
            guinodes_.at(x).at(y)->setWaiting( false );
        }
    }
    drawArea_->update();
}


template<typename Location>
std::vector<Location> reconstruct_path( Location start, Location goal, std::unordered_map<Location, Location>& came_from )
{
  std::vector<Location> path;
  Location current = goal;
  path.push_back(current);

  while (current != start)
  {
    current = came_from[current];
    path.push_back(current);
  }

  std::reverse(path.begin(), path.end());

  return path;
}

void SearchArea::dijkstra( Graph* graph, Graph::Location start, Graph::Location goal,
                           std::unordered_map< unsigned int, unsigned int >& came_from,
                           std::unordered_map< unsigned int, int>& cost_so_far)
{
    PriorityQueue<unsigned int> frontier;
    frontier.put(start.id(), 0);
    came_from[ start.id() ] = start.id();
    cost_so_far[ start.id() ] = 0;
    GUINode* gnode = 0;

    while (!frontier.empty())
    {
      auto current = frontier.get();

      gnode = guinodes_.at(current%GRAPH_WIDTH).at(current/GRAPH_WIDTH);
      gnode->setVisited( true );

      if (current == goal.id()) {
        break;
      }

      for (auto next : graph->neighbours(current) )
      {
        int new_cost = cost_so_far[current] + graph->cost(current, next.id() );

        if ( !cost_so_far.count(next.id()) || new_cost < cost_so_far[next.id()] ) {
          if( graph->cost(current, next.id() ) != -1 ) {

            cost_so_far[next.id()] = new_cost;
            came_from[next.id()] = current;
            frontier.put(next.id(), new_cost);
            gnode = guinodes_.at( next.id()%GRAPH_WIDTH ).at( next.id()/GRAPH_WIDTH );
            gnode->setWaiting( true );

          }
        }
      }

      drawArea_->update( gnode->getCoordinate()->x*NODEWIDTH, gnode->getCoordinate()->y*NODEHEIGHT, NODEWIDTH, NODEHEIGHT );
    }
}

inline int heuristic( unsigned int a, unsigned int b)
{
  int x1, y1, x2, y2;
  x1 = a%GRAPH_WIDTH;
  x2 = b%GRAPH_WIDTH;
  y1 = a/GRAPH_WIDTH;
  y2 = b/GRAPH_WIDTH;

  return (abs(x1 - x2) + abs(y1 - y2));
}

void SearchArea::a_star_search(Graph *graph, Graph::Location start, Graph::Location goal, std::unordered_map<unsigned int, unsigned int> &came_from, std::unordered_map<unsigned int, int> &cost_so_far)
{
    PriorityQueue<unsigned int> frontier;
    frontier.put( start.id(), 0 );
    came_from[ start.id() ] = start.id();
    cost_so_far[ start.id() ] = 0;

    GUINode* gnode = 0;

    while (!frontier.empty())
    {
      auto current = frontier.get();

      gnode = guinodes_.at( current%GRAPH_WIDTH ).at( current/GRAPH_WIDTH );
      gnode->setVisited( true );

      if ( current == goal.id() ) {
        break;
      }

      for ( auto next : graph->neighbours( current ) )
      {
        int cost_to_next = graph->cost( current, next.id() );
        int new_cost = (cost_so_far[ current ] + cost_to_next);

        if ( !cost_so_far.count( next.id() ) || ( new_cost < cost_so_far[ next.id() ] ) ) {
            if( cost_to_next != -1 ) {

                int priority = new_cost + heuristic( next.id(), goal.id() );
                cost_so_far[ next.id() ] = new_cost;
                frontier.put( next.id(), priority );
                came_from[ next.id() ] = current;
                gnode = guinodes_.at( next.id()%GRAPH_WIDTH ).at( next.id()/GRAPH_WIDTH );
                gnode->setWaiting( true );

            }
        }
      }
    }
}


void SearchArea::greedy_search( Graph* graph, Graph::Location start, Graph::Location goal,
                           std::unordered_map< unsigned int, unsigned int >& came_from)
{
    PriorityQueue<unsigned int> frontier;
    frontier.put(start.id(), 0);
    came_from[ start.id() ] = start.id();
    GUINode* gnode = 0;

    while (!frontier.empty())
    {
      auto current = frontier.get();

      gnode = guinodes_.at( current%GRAPH_WIDTH ).at( current/GRAPH_WIDTH );
      gnode->setVisited( true );

      if (current == goal.id()) {
        break;
      }

      for (auto next : graph->neighbours( current ) )
      {
        if ( !came_from.count( next.id() ) ) {
          if( graph->cost( current, next.id() ) != -1 ) {

            int priority = heuristic( next.id(), goal.id() );
            frontier.put( next.id(), priority );
            came_from[ next.id() ] = current;
            gnode = guinodes_.at( next.id()%GRAPH_WIDTH ).at( next.id()/GRAPH_WIDTH );
            gnode->setWaiting( true );

          }
        }
      }
      drawArea_->update( gnode->getCoordinate()->x*NODEWIDTH, gnode->getCoordinate()->y*NODEHEIGHT, NODEWIDTH, NODEHEIGHT );
    }
}




void SearchArea::onNodePressed(GUINode *node)
{
    if( setStart_ ){

        start_->flipStart();
        drawArea_->update( start_->getCoordinate()->x*NODEWIDTH, start_->getCoordinate()->y*NODEHEIGHT, NODEWIDTH, NODEHEIGHT );
        node->flipStart();
        start_ = node;
        setStart_ = false;

    } else if ( setGoal_ ){

        goal_->flipGoal();
        drawArea_->update( goal_->getCoordinate()->x*NODEWIDTH, goal_->getCoordinate()->y*NODEHEIGHT, NODEWIDTH, NODEHEIGHT );
        node->flipGoal();
        goal_ = node;
        setGoal_ = false;

    } else if ( setPassable_ ){

        node->flipPassable();
        if( node->getPassable() ){
            graph_->setWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH, WEIGHT_ONE );
        }
        else {
            graph_->setWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH, NOT_PASSABLE );
        }

    } else if ( setWeight_ ){

        if( graph_->getWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH ) == WEIGHT_ONE ){
            graph_->setWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH, WEIGHT_TWO );
            node->setWeight(8);
        }
        else if( graph_->getWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH ) == WEIGHT_TWO ){
            graph_->setWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH, WEIGHT_THREE );
            node->setWeight(15);
        }
        else if( graph_->getWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH ) == WEIGHT_THREE ){
            graph_->setWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH, WEIGHT_ONE );
            node->setWeight(1);
        }

    }

    drawArea_->update( node->getCoordinate()->x*NODEWIDTH, node->getCoordinate()->y*NODEHEIGHT, NODEWIDTH, NODEHEIGHT );
}

void SearchArea::onSetStartPressed()
{
    setStart_ = true;
    setGoal_ = false;
    setWeight_ = false;
    setPassable_ = false;
}

void SearchArea::onSetGoalPressed()
{
    setStart_ = false;
    setGoal_ = true;
    setWeight_ = false;
    setPassable_ = false;
}

void SearchArea::onSetWeight()
{
    setStart_ = false;
    setGoal_ = false;
    setWeight_ = true;
    setPassable_ = false;
}

void SearchArea::onSetPassable()
{
    setStart_ = false;
    setGoal_ = false;
    setWeight_ = false;
    setPassable_ = true;
}

void SearchArea::onRemovePassables()
{
    for( unsigned int x(0); x < GRAPH_WIDTH; ++x )
    {
        for( unsigned int y(0); y < GRAPH_HEIGHT; ++y )
        {
            GUINode* node = guinodes_.at(x).at(y);

            if( !node->getPassable() ){
                graph_->setWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH, WEIGHT_ONE );
                node->flipPassable();
            }

        }
    }
    drawArea_->update();
}

void SearchArea::onRemoveWeights()
{
    for( unsigned int x(0); x < GRAPH_WIDTH; ++x )
    {
        for( unsigned int y(0); y < GRAPH_HEIGHT; ++y )
        {
            GUINode* node = guinodes_.at(x).at(y);
            graph_->setWeight( node->getCoordinate()->x + node->getCoordinate()->y*GRAPH_WIDTH, WEIGHT_ONE );
            node->setWeight(1);

        }
    }
    drawArea_->update();
}

void SearchArea::addVisited()
{
    visited_ += 1;
}

void SearchArea::onReset()
{
    resetDrawArea();
}

void SearchArea::startSearch(QString searchtype)
{
    resetDrawArea();
    std::unordered_map< unsigned int, unsigned int > came_from;
    std::unordered_map< unsigned int, int > cost_so_far;
    visited_ = 0;

    if( searchtype == "Dijkstra" ){
        dijkstra( graph_, Graph::Location(start_->getCoordinate()->x, start_->getCoordinate()->y),
                  Graph::Location(goal_->getCoordinate()->x, goal_->getCoordinate()->y), came_from, cost_so_far  );

    }

    else if( searchtype == "A*" ){
        a_star_search( graph_, Graph::Location(start_->getCoordinate()->x, start_->getCoordinate()->y),
                       Graph::Location(goal_->getCoordinate()->x, goal_->getCoordinate()->y), came_from, cost_so_far);
    }

    else if( searchtype == "Greedy" ){
        greedy_search(graph_, Graph::Location(start_->getCoordinate()->x, start_->getCoordinate()->y),
                      Graph::Location(goal_->getCoordinate()->x, goal_->getCoordinate()->y), came_from );
    }

    else {
        qDebug() << "SearchArea::startSearch: Something happened!";
        return;
    }

    std::vector<unsigned int> path = reconstruct_path(start_->getId(), goal_->getId(), came_from);

    for( unsigned int i(0); i < path.size(); ++i )
    {
        guinodes_.at(path.at(i)%GRAPH_WIDTH).at(path.at(i)/GRAPH_WIDTH)->setIsPath(true);
    }
    emit routeLength( path.size() );
    emit visitedAmount( visited_ );
    drawArea_->update();
}


