
#ifndef LIIKENNEVALO_HH
#define LIIKENNEVALO_HH

#include <pthread.h>

// Valoja hallinnoivan saikeen toiminta.
void* valotPaalle( void* valodata );


class Liikennevalo
{
 public:
   struct Valolukko{
     pthread_mutex_t valomutex;
     pthread_cond_t valoehtoA;
     pthread_cond_t valoehtoB;
     pthread_cond_t risteysehto;
   };

   Liikennevalo();
   ~Liikennevalo();

   // Luo saikeen valosaie_ ja laittaa liikennevalot toimimaan sen kautta.
   // palauttaa false, jos luonti aonnisistui, true, jos onnistui.
   bool luoSaie();

   // Asettaaa suunta_ = suunta.
   void asetaSuunta( char suunta );
  
   // Palauttaa suunta_.
   char palautaSuunta();

   // Kasvattaa haluttua jonoa yhden yksikon.
   // suunta = 'A' -> ++jonoA_, suunta = 'B' -> ++jonoB_.
   void lisaaJonoon( char suunta );

   // Vahentaa haluttua jonoa yhden yksikon.
   // suunta = 'A' -> --jonoA_, suunta = 'B' -> --jonoB_.
   void vahennaJonoa( char suunta );

   // Palauttaa lukko_.
   Valolukko* palautaLukko();

   // Jos jono = 'A', palauttaa jonoA_. Jos jono = 'B', palauttaa jonoB_.
   // Muuten palauttaa 0.
   unsigned int palautaJono( char jono );
 
private:

   // Tietysta suunnasta tulevien autojen maara, jotka jonottavat
   // risteysalueelle paasya.
   unsigned int jonoA_;
   unsigned int jonoB_;

   // Liikennevaloihin paasevia autoja hallinnoiva lukkosysteemi.
   Valolukko* lukko_;

   // Saie, joka hallinnoi valojen toimintaa. Pyorittaa valotPaalle-funktiota.
   pthread_t valosaie_;

   // Suunta, josta autot paasevat risteysalueelle.
   char suunta_;
};

#endif
