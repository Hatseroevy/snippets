
#ifndef TIETORAKENTEET_HH
#define TIETORAKENTEET_HH


// Auton saikeelle annettava datapaketti, jolla ohjataan
// auton toimintaa.
struct Data
{
  Car* automobile;
  Liikennevalo* valo;
  Lautta::Lauttalukko* lauttalukko;
};

#endif
