
#include "kaara.hh"
#include "liikennevalo.hh"
#include "lautta.hh"
#include "satunnaisluku.hh"
#include "tulostus.hh"
#include "data.hh"
#include <iostream>


Car::Car( unsigned int id ) : id_(id)
{
  // Arvotaan auton reitti.
  Random arpoja;
  alku_ = static_cast<char>(65 + arpoja.integer( 1, 30 ) % 2);
  loppu_ = static_cast<char>(67 + arpoja.integer( 1, 30 ) % 2);
}


// Auton säikeen koodi.
void* suorita(void* arg)
{
  Data* temp_data = (Data*)arg;
  Car* car = temp_data->automobile;
  Liikennevalo* valo = temp_data->valo;
  Liikennevalo::Valolukko* valolukko = valo->palautaLukko();
  Lautta::Lauttalukko* lauttalukko = temp_data->lauttalukko;

  {
    tulostus::lukko l;
    std::cerr << "Auto luotu " << car->palautaId() << ", reitti "
    << car->palautaAlku() << "->" << car->palautaLoppu() << std::endl;
  }

  {
    tulostus::lukko l;
    std::cerr << "Auto " << car->palautaId()
    << ", saapui liikennevaloihin" << std::endl;
  }


  // Jos auto saa lukon, se voi mennä risteysalueelle.
  // Muuten jaadaan jonottamaan.
  pthread_mutex_lock( &valolukko->valomutex );
  // Kasvatetaan valojen jonoa.
  valo->lisaaJonoon( car->palautaAlku() );
  if( car->palautaAlku() == 'A' )
  {
    pthread_cond_wait( &valolukko->valoehtoA, &valolukko->valomutex);
  }
  else if( car->palautaAlku() == 'B' )
  {
    pthread_cond_wait( &valolukko->valoehtoB, &valolukko->valomutex);
  }

  {
    tulostus::lukko l;
    std::cerr << "Auto " << car->palautaId() <<
    ", risteysalueella" << std::endl;
  }
  // Vapautetaan risteyksens vapautumisesta
  pthread_cond_signal( &valolukko->risteysehto );

  // Auto pääsi risteysalueelta pois ja vapauttaa lukon.
  {
  tulostus::lukko l;
  std::cerr << "Auto " << car->palautaId()
  << ", poistui liikennevaloista" << std::endl;
  }
  pthread_mutex_unlock( &valolukko->valomutex );



  // Lautalle menevät autot laitetaan wait-tilaan
  if( car->palautaLoppu() == 'D')
  {
    pthread_mutex_lock( &lauttalukko->lauttamutex );
    {
      tulostus::lukko l;
      std::cerr << "Auto " << car->palautaId()
      << ", jonottaa lautalle" << std::endl;
    }
    // Odotetaan lautalta ilmoitusta saapumisesta satamaan L.
    pthread_cond_wait( &lauttalukko->lauttaehtoL, &lauttalukko->lauttamutex );

    {
      tulostus::lukko l;
      std::cerr << "Auto " << car->palautaId()
      << ", nousee lautalle" << std::endl;
    }

    // Odotetaan kun on saavuttu satamaan D.
    pthread_cond_wait( &lauttalukko->lauttaehtoD, &lauttalukko->lauttamutex );
    pthread_mutex_unlock( &lauttalukko->lauttamutex );
  }

  // Auto pääsee tiensä päähän
  {
    tulostus::lukko l;
    std::cerr << "Auto " << car->palautaId()
    << ", poistuu " << car->palautaLoppu() << std::endl;
  }
  return reinterpret_cast<void*>( temp_data );
}


char Car::palautaAlku(){
  return alku_;
}

char Car::palautaLoppu(){
  return loppu_;
}

unsigned int Car::palautaId(){
  return id_;
}
