
#include "liikennevalo.hh"
#include "tulostus.hh"
#include <chrono>
#include <iostream>
#include <unistd.h>


Liikennevalo::Liikennevalo() : lukko_( new Valolukko ), suunta_('A')
{
  lukko_->valomutex = PTHREAD_MUTEX_INITIALIZER;
  lukko_->valoehtoA = PTHREAD_COND_INITIALIZER;
  lukko_->valoehtoB = PTHREAD_COND_INITIALIZER;
  lukko_->risteysehto = PTHREAD_COND_INITIALIZER;
}


Liikennevalo::~Liikennevalo()
{
  delete lukko_;
  lukko_ = 0;
}


bool Liikennevalo::luoSaie()
{
   auto status = pthread_create( &valosaie_, NULL, &valotPaalle, (void*)this );
   if( status != 0)
   {
     return false;
   }
   return true;
}


void* valotPaalle( void* valodata)
{
  Liikennevalo* valo = (Liikennevalo*)valodata;
  Liikennevalo::Valolukko* valolukko = valo->palautaLukko();
  std::chrono::system_clock::time_point alku;
  std::chrono::system_clock::time_point loppu;

  while( true )
  {
    // Otetaan talteen kellonaika nyt, jotta voidaan pitaa valoja
    // paalla tietty aika
    alku = std::chrono::system_clock::now();
    loppu = std::chrono::system_clock::now();
    char suunta = valo->palautaSuunta();


    // Kymmenen sekunnin ajan paastetaan autoja yksitellen vuorossa olevasta
    // suunnasta
    while( std::chrono::duration_cast<std::chrono::seconds>
        ( loppu - alku ).count() < 10 )
    {

       // Lukitaan vain, jos joku haluaa tulla.
       if( suunta == 'A' && valo->palautaJono('A') > 0 )
       {
         pthread_mutex_lock( &valolukko->valomutex );
         // Annetaan yhdelle autolle kulkulupa.
         pthread_cond_signal( &valolukko->valoehtoA );
         // Odotetaan autolta signaalia risteyksen vapautumisesta.
         pthread_cond_wait( &valolukko->risteysehto, &valolukko->valomutex );
         pthread_mutex_unlock( &valolukko->valomutex );
         valo->vahennaJonoa('A');
       }

       else if( suunta == 'B' && valo->palautaJono('B') > 0 )
       {
         pthread_mutex_lock( &valolukko->valomutex );
         pthread_cond_signal( &valolukko->valoehtoB );
         pthread_cond_wait( &valolukko->risteysehto, &valolukko->valomutex );
         pthread_mutex_unlock( &valolukko->valomutex );
         valo->vahennaJonoa('B');
       }

       loppu = std::chrono::system_clock::now();
    }

    // Kymmenen sekuntia mennyt, vaihdetaan suunta
    ++suunta;
    // Sallitaan suunnaksi vain 'A' tai 'B'.
    if( suunta == 'C' )
    {
      suunta = 'A';
    }
    valo->asetaSuunta( suunta );
    {
      tulostus::lukko l;
      std::cerr << "Liikennevalo vaihtui, vuorossa " << valo->palautaSuunta() 
      << std::endl;
    }
  }
  return 0;
}



Liikennevalo::Valolukko* Liikennevalo::palautaLukko()
{
  return lukko_;
}


void Liikennevalo::lisaaJonoon( char suunta )
{
  switch( suunta )
    {
      case 'A':
        ++jonoA_;
        break;
      case 'B':
        ++jonoB_;
        break;
    }
}


void Liikennevalo::vahennaJonoa( char suunta)
{
  switch( suunta )
    {
      case 'A':
        --jonoA_;
        break;
      case 'B':
        --jonoB_;
        break;
    }
}


void Liikennevalo::asetaSuunta( char suunta )
{
   suunta_ = suunta;
}


char Liikennevalo::palautaSuunta()
{
   return suunta_;
}


unsigned int Liikennevalo::palautaJono( char jono )
{
  switch( jono )
    {
      case 'A':
        return jonoA_;
      case 'B':
        return jonoB_;
    }
  // Tanne ei pitaisi paasta
  return 0;
}
