
#include <iostream>
#include <vector>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <chrono>         // std::chrono::seconds
#include "tulostus.hh"
#include "satunnaisluku.hh"
#include "liikennevalo.hh"
#include "lautta.hh"
#include "kaara.hh"
#include "data.hh"



// Pääohjelma luo 100 säiettä (autoa) satunnaisin väliajoin,
// lautan ja liikennevalon.
int main( int , char** ){

  {
    tulostus::lukko l;
    std::cerr << "Käynnistys." << std::endl;
  }
  
  // Luodaan arpa, jolta pyydetaan satunnaisia lukuja.
  Random arpoja;
  // Autojen saikeet.
  std::vector<pthread_t> saikeet;
  // Totuusarvo, joka kertoo liikennevalojen ja lautan saikeen luonnin
  // onnistumisesta.
  bool onnistui = false;

  // Saikeiden luonti.
  
  // Liikennevalo
  Liikennevalo* valo = new Liikennevalo();
  onnistui = valo->luoSaie();
  if( !onnistui )
  {
    perror( "pthread_create" );
    exit(1);
  }

  {
    tulostus::lukko l;
    std::cerr << "Liikennevalo päällä, vuorossa "
    << valo->palautaSuunta() << std::endl;
  }

  // Lautta
  Lautta* lautta = new Lautta();
  onnistui = lautta->luoSaie();
  if( !onnistui )
  {
    perror( "pthread_create" );
    exit(1);
  }

  {
    tulostus::lukko l;
    std::cerr << "Lautta liikkeellä" << std::endl;
  }


  // Luodaan autot
  for( unsigned int i = 1; i <= 100; i++ )
  {
    pthread_t saie;

    Data* data = new Data();
    data->automobile = new Car(i);
    data->valo = valo;
    data->lauttalukko = lautta->palautaLukko();

    auto status = pthread_create( &saie, NULL, &suorita, (void*) data );
    if( status != 0 ) {
      perror( "pthread_create" );
      exit(1);
    }
    saikeet.push_back( saie );

    // Arvotaan aika väliltä 1-5 sekuntia
    usleep(arpoja.integer( 1000000, 5000000 ));
  }

  // käydään läpi käynnistetyt säikeet ja kerro tutkitaan paluuarvo
  for( auto& thr : saikeet )
  {
     void* retval = 0;
     auto status = pthread_join( thr, &retval  );
     if( status != 0 ) {
       perror( "pthread_join" );
       exit(2);
    }

    auto data = reinterpret_cast<Data*>( retval );

    // Muistin vapautus.
    delete data->automobile;
    data->automobile = 0;
    delete data;
    data = 0;
  }

  // Tuhotaan liikennevalo
  delete valo;
  valo = 0;

  // Tuhotaan lautta
  delete lautta;
  lautta = 0;

  return 0;
}
