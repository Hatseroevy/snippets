
#ifndef LAUTTA_HH
#define LAUTTA_HH

#include <string>
#include <pthread.h>


// Lautan toiminnallisuus.
void* lauttaLiikkeelle( void* lauttadata);


class Lautta
{
 public:
   // Tarvittava mutex ja ehdot lautan toiminnalle ja signaloinnille
   // lautan liikkeistä satamien välillä
   struct Lauttalukko{
     pthread_mutex_t lauttamutex;
     pthread_cond_t lauttaehtoD;
     pthread_cond_t lauttaehtoL;
   };
   Lautta();
   ~Lautta();

   // Luo saikeen, jossa lauttaLiikkeelle-funktio suoritetaan.
   // Palauttaa false, jos luonti epaonnistui, true, jos onnistui.
   bool luoSaie();

   // Asettaa sijainti_ = sijainti.
   void asetaSijainti( char sijainti );
   
   // Palauttaa sijainti_.
   char palautaSijainti();

   // Palauttaa lukko_.
   Lauttalukko* palautaLukko();

 private:
   // Lautan sijainti. Joko 'L' tai 'D'.
   char sijainti_;

   // Saie, jossa lauttaLiikkeelle suoritetaan.
   pthread_t lauttasaie_;

   // Lukko, joka hallinnoi lautalle paasevia ja sielta lahtevia autoja.
   Lauttalukko* lukko_;
};

#endif
