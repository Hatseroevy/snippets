
#ifndef KAARA_HH
#define KAARA_HH


// Auton suoritus.
void* suorita( void* arg );


class Car
{
 public:
   // Rakentaja, tallentaa auton id_n.
   Car( unsigned int id );
   // Palauttaa id_.
   unsigned int palautaId();
   //Palauttaa alku_.
   char palautaAlku();
   // Palauttaa loppu_.
   char palautaLoppu();
 private:
   
   // Auton yksiloiva tunnus.
   unsigned int id_;
   // Auton lahtopaikka. Joko 'A' tai 'B'.
   char alku_;
   // Paikka, jonne auto menee. Joko 'C' tai 'D'.
   char loppu_;
};

#endif
