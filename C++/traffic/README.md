Authors: Nina Anttila, Lassi Kojo

A course assignment work using pthreads.

- Cars(kaara), threads, start from place A or B and travel to C or D.
- First they need to go to mutex traffic lights(liikennevalo).
	For 10 seconds cars from A are allowed to go, after that the cars from B are allowed to 	come for 20s. Then repeat.
- A car can only go to intersection if it's free and it's it's turn.
- Only one car at the time is allowed on the intersection to avoid collisions. (Safety first!)
- Then the cars leave the map through C or go to point L to wait for a mutex ferry(lautta).
- The ferry picks up every waiting car from L and takes them to D where they can exit.
	During the ride no new cars are picked up.

Unfortunately everything in this code is in Finnish :-)
Golang-version works quite the same way, except there are a few channels instead of mutex and conditions. And it's in English -> Helps to understand this maybe?