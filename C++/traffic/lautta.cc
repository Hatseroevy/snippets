
#include "lautta.hh"
#include "tulostus.hh"
#include <iostream>
#include <unistd.h>
#include <chrono> 


Lautta::Lautta() : sijainti_('L'), lukko_(new Lauttalukko)
{
  // Tarvittava mutex ja ehdot lautan toiminnalle ja signaloinnille
  // lautan liikkeistä satamien välillä
  lukko_->lauttamutex = PTHREAD_MUTEX_INITIALIZER;
  lukko_->lauttaehtoL = PTHREAD_COND_INITIALIZER;
  lukko_->lauttaehtoD = PTHREAD_COND_INITIALIZER;
}

Lautta::~Lautta()
{
  delete lukko_;
  lukko_ = 0;
}

bool Lautta::luoSaie()
{
   auto status = pthread_create( &lauttasaie_, NULL, &lauttaLiikkeelle,
       (void*) this );
   if( status != 0 )
   {
     return false;
   }
   return true;
}


void* lauttaLiikkeelle( void* lauttadata )
{
  Lautta* lautta = (Lautta*)lauttadata;
  Lautta::Lauttalukko* lauttalukko = lautta->palautaLukko();

  while( true )
  {

    // Seilataan 7 sekuntia.
    usleep( 7000000 );

    // Mennaan satamaan, jossa ei asken oltu ja ilmoitetaan kaikille
    // saapumista odottaville autoille.
    if( lautta->palautaSijainti() == 'L' )
    {
      lautta->asetaSijainti( 'D' );
      pthread_mutex_lock( &lauttalukko->lauttamutex );
      {
         tulostus::lukko l;
         std::cerr << "Lautta saapuu D" << std::endl;
      }
      pthread_cond_broadcast( &lauttalukko->lauttaehtoD );
      pthread_mutex_unlock( &lauttalukko->lauttamutex );
    }
    else
    {
      lautta->asetaSijainti( 'L' );
      pthread_mutex_lock( &lauttalukko->lauttamutex );
      {
         tulostus::lukko l;
         std::cerr << "Lautta saapuu L" << std::endl;;
      }
      pthread_cond_broadcast( &lauttalukko->lauttaehtoL );
      pthread_mutex_unlock( &lauttalukko->lauttamutex );
    }
  }
  return 0;
}


void Lautta::asetaSijainti( char sijainti)
{
  sijainti_ = sijainti;
}


char Lautta::palautaSijainti()
{
  return sijainti_;
}


Lautta::Lauttalukko* Lautta::palautaLukko()
{
  return lukko_;
}

