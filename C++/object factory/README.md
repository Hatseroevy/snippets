Author: Nina Anttila

A part of a course work.

An object factory, in this case a creature factory. Sub-classes of the Creature class (such as Lynx) register themselves with a static function and then the factory can create them even thought it doesn't really know what it's creating.