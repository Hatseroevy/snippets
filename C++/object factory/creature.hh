// Creature class

#ifndef CREATURE_HH
#define CREATURE_HH
#include <QString>
#include <QObject>
#include "coordinate.hh"



class Creature : public QObject
{
Q_OBJECT
public:

    struct CreatureData
    {
        int health;
        int attackPower;
        int defence;
        Coordinate coord;
        CreatureType type;
        QString name;
        QString description;
    };

    Creature( Creature::CreatureData* info );
    virtual ~Creature();

    // Other methods here

private:
    CreatureData* info_;
};

#endif // CREATURE_HH
