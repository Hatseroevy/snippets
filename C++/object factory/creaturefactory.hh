// CreatureFactory which creates any kind of sub-creatures which
// have registered themselves by using registerer-method.

#ifndef CREATUREFACTORY_HH
#define CREATUREFACTORY_HH

#include <QString>
#include "creature.hh"
#include <vector>
#include <functional>

class CreatureFactory
{
public:

    // Only use throught static instance.
    static CreatureFactory& instance();
    ~CreatureFactory() = default;

    // Searches for the right constructor by the given name in the info
    // and returns a new creature.
    Creature* newCreature( Creature::CreatureData* info );

    // Constructor for any Creature class will be stored with this
    typedef std::function<Creature*( Creature::CreatureData* info )> Constructor;

    // Registers Creature classes by saving their name and constructor
    void registerer( const QStringList& names, Constructor constructor );

private:

    // No copying or assigning
    CreatureFactory( const CreatureFactory& );
    CreatureFactory& operator =( const CreatureFactory& );

    // Only use throught static instance.
    CreatureFactory();

    // Structure for saving sub-creatures' names and constructors.
    struct CreatureInfo
    {
        QStringList names;
        CreatureFactory::Constructor constructor;
    };
    std::vector< CreatureInfo > creatures_;

};

#endif // CREATUREFACTORY_HH
