#include "creature.hh"

Creature::Creature( Creature::CreatureData *info ):
    info_( info )
{
}

Creature::~Creature()
{
    if( info_ != nullptr )
    {
        delete info_;
        info_ = nullptr;
    }
}

// Other methods here
