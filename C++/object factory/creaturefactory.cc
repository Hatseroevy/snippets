#include "creaturefactory.hh"

CreatureFactory::CreatureFactory( )
{
}


CreatureFactory &CreatureFactory::instance()
{
    static CreatureFactory instance;
    return instance;
}


Creature *CreatureFactory::newCreature(Creature::CreatureData* info)
{
    Creature* cr = nullptr;
    for( unsigned int c(0); c < creatures_.size(); ++c )
    {
        // Find the given name and create the right kind of sub-creature.
        if( creatures_.at(c).names.contains( info.name ) )
        {
            cr = creatures_.at(c).constructor( info );
        }
    }
    if( cr == nullptr )
    {
        // No creature found. Create a boogieman?
    }
    return cr;
}

void CreatureFactory::registerer(const QStringList &names, CreatureFactory::Constructor constructor)
{
    CreatureInfo cInfo;
    cInfo.names = names;
    cInfo.constructor = constructor;
    creatures_.push_back( cInfo );
}
