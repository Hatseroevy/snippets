#include "lynx.hh"
#include "creaturefactory.hh"

Lynx::Lynx(CreatureData *info ):
    Creature( info ), lives_(9)
{
}

Lynx::~Lynx()
{

}

// Register creture automatically to the factory
static bool registerMe = []()->bool
{
    // Give a namelist containing the names which can be used for creating a lynx and
    // a constructor for lynx.
    CreatureFactory::instance().registerer( ( QStringList() << "Lynx" << "lynx" << "bobcat" << "ilves" ),
                                                 []( Creature::CreatureData* info )->Creature* {
                                                    return new Lynx( info );
                                                 });
    return true;
}
();
