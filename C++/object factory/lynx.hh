// Lynx class

#ifndef LYNX_HH
#define LYNX_HH
#include "creature.hh"

class Lynx : public Creature
{
public:
    Lynx( Creature::CreatureData* info );
    virtual ~Lynx();

    // Other methods here

private:
    int lives_;
};

#endif // LYNX_HH
