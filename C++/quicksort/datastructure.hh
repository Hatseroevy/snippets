// Definition of Datastructure class for UDS/Tiraka homework 1 -


#ifndef DATASTRUCTURE_HH
#define DATASTRUCTURE_HH

#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>

const char SEPARATOR = ' ';
const std::string MSG_EMPTY_DS = "Data structure is empty.\n";

class Datastructure {

public:

   Datastructure();
   
   ~Datastructure();
   
   // Adds one value to the datastructure
   void add(std::string rank, unsigned int birthYear, unsigned int enlistingYear, std::string shirtColor, std::string name);
   
   // Finds and prints person with index
   void find(unsigned int index);
   
   // Finds and prints youngest person
   void youngest();
   
   // Finds and prints oldest person
   void oldest();
   
   // Empties the datastructure
   void empty();
   
   // returns the size of the datastructure
   size_t size();

private:
  
   // Copy constructor is forbidden
   Datastructure(const Datastructure&);
   // Assignment operator is forbidden
   Datastructure& operator=(const Datastructure&);

   // Sorts out the identities with a mildly-modified Quicksort
   void quicksort(long int left, long int right);

   // Returns the index where the sorted front part ends and sorts out the member.
   long int partition(long int left, long int right);

   // Prints out an identity according to the given index.
   void print( unsigned long index );

   // Swaps the places of two members in the identities_-vector.
   // Parameters (first and second) are the indexes of those who need to be
   // swapped with each other.
   void swap(unsigned long first, unsigned long second );

   // Checks if the identities are sorted and sorts them if they aren't.
   void sortIfNeeded();

   struct Identity{
       std::string rank;
       unsigned int birthYear;
       unsigned int enlistingYear;
       std::string shirtColor;
       std::string name;
   };
   std::vector<Datastructure::Identity*> identities_;
   bool sorted_;
};

#endif
