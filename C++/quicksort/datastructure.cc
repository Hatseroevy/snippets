#include "datastructure.hh"

Datastructure::Datastructure(): sorted_(false)
{}

Datastructure::~Datastructure()
{
    empty();
}

// Adds one value to the datastructure
void Datastructure::add(std::string rank, unsigned int birthYear,
        unsigned int enlistingYear, std::string shirtColor, std::string name)
{
    Identity* newIdentity = new Identity;
    newIdentity->rank = rank;
    newIdentity->birthYear = birthYear;
    newIdentity->enlistingYear = enlistingYear;
    newIdentity->shirtColor = shirtColor;
    newIdentity->name = name;

    identities_.push_back( newIdentity );
    sorted_ = false;
}

// Finds and prints person with index
void Datastructure::find(unsigned int index)
{
    sortIfNeeded();
    print(index-1);
}

// Finds and prints oldest person
void Datastructure::oldest()
{
    sortIfNeeded();
    print(0);
}

// Finds and prints youngest person
void Datastructure::youngest()
{
    sortIfNeeded();
    print(identities_.size()-1);
}

// Empties the datastructure
void Datastructure::empty()
{
    while( !identities_.empty())
    {
        delete identities_.at(identities_.size()-1);
        identities_.at(identities_.size()-1) = nullptr;
        identities_.pop_back();
    }
}

// returns the size of the datastructure
size_t Datastructure::size()
{
    return identities_.size();
}


void Datastructure::quicksort(long int left, long int right )
{
    long int p = 0;
    if( left < right )
    {
        p = partition( left, right );
        quicksort( left, p-1 );
        quicksort( p+1, right );
    }

}


// Finds the pivot element and switches the order of identities within the
// indexes left and right to descenting order.
long int Datastructure::partition( long int left, long int right )
{

    long int p = left;
    Identity* pivot = identities_.at(static_cast<unsigned long>(left));
    for(long int i = left+1 ; i <= right ; i++)
    {
        Identity* compared = identities_.at(static_cast<unsigned long>(i));
        if(compared->enlistingYear < pivot->enlistingYear ||
          (compared->enlistingYear == pivot->enlistingYear &&
           compared->birthYear < pivot->birthYear ))
        {
            p++;
            swap(static_cast<unsigned long>(i), static_cast<unsigned long>(p));
        }
    }
    swap(static_cast<unsigned long>(p), static_cast<unsigned long>(left));
    return p;
}


// Swaps the data from the first given place to the second and vice versa.
void Datastructure::swap(unsigned long first, unsigned long second)
{
    Identity* temp = nullptr;
    temp = identities_.at(first);
    identities_.at(first) = identities_.at(second);
    identities_.at(second) = temp;
}

// Checks if the identities arn't sorted and sorts them.
void Datastructure::sortIfNeeded()
{
    if( !sorted_ ){
        quicksort( 0, static_cast<long>(identities_.size()-1) );
        sorted_ = true;
    }
}


// Prints the data from a certain index if the datastructure is not empty.
void Datastructure::print(unsigned long index)
{
    if(identities_.empty())
    {
        std::cout << MSG_EMPTY_DS;
        return;
    }
    std::cout << identities_.at( index )->name << " "
        << identities_.at( index )->rank << " "
        << identities_.at( index )->birthYear << " "
        << identities_.at( index )->enlistingYear << " "
        << identities_.at( index )->shirtColor << std::endl;
}
