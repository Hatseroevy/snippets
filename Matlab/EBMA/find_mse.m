function [ y_shift, x_shift, mse_value, frame] = find_mse( block, prev_frame_block, next_frame_block )
%find_mse Etsii pienimm�n mse:n annetulle lohkolle ja palauttaa sen
%suuruuden, x- ja y-siirtym�t sek� numeroarvona sen, oliko l�ytetty mse
%edellisess�(1) vai seuraavassa(2) kuvassa
%
%   Parametrit:
%       block: tarkasteltava lohko.
%       prev_frame_block: edellisen kuvan alue, jota tarkastellaan.
%       next_frame_block: seuraavan kuvan alue, jota tarkastellaan.
% 
%   Paluuarvot:
%       y_shift: lohkon siirtym� y-suunnassa.
%       x_shift: lohkon siirtym� x-suunnassa.
%       mse_value: pienimm�n mse:n arvo.
%       frame: tieto siit�, l�ytyyk� mse edellisest� vai seuraavasta.
%       kuvasta, edelt�v� = 1, seuraava= 2.


    % Lohkon koon m��ritys
    block_height = size(block, 1);
    block_width = size(block, 2);
    mse_value = 255;
    y_shift = 0;
    x_shift = 0;
    frame = 0;
    
    % Verrataan lohkoa edelt�v�n ja seuraavan kuvan lohkoihin liikuttamalla
    % ikkunaa pikseli kerrallaan lohkon yli ja lasketaan jokaisesta 
    % kohdasta MSE.
    
    for i=1:block_height+1
        for j=1:block_width+1
            % Framesta luodaan lohkon kokoinen subblock
            subblock = prev_frame_block(i:i+block_height-1, j:j+block_width-1, :);
            
            % Lasketaan keskiarvo erotusten neli�st�.
            % new_mse = mean2( (double(block)-double(subblock)).^2 );
            % Kun ottaa vain punaisen kanavan (alla), saa esimerkin suhteen
            % identtisen tuloksen.
            new_mse = mean2( (double(block(:,:,1))-double(subblock(:,:,1))).^2 );
            
            if new_mse < mse_value
                mse_value = new_mse;
                x_shift = j - block_width/2 - 1;
                y_shift = i - block_height/2 - 1;
                frame = 1;
            end
        end
    end
    for i=1:block_height+1
        for j=1:block_width+1
        
            % Framesta luodaan lohkon kokoinen subblock
            subblock = next_frame_block(i:i+block_height-1, j:j+block_width-1, :);

            % Lasketaan keskiarvo erotusten neli�st�.
            % new_mse = mean2( (double(block)-double(subblock)).^2 );
            % Kun ottaa vain punaisen kanavan (alla), saa esimerkin suhteen
            % identtisen tuloksetn.
            new_mse = mean2( (double(block(:,:,1))-double(subblock(:,:,1))).^2 );
            
            if new_mse < mse_value
                mse_value = new_mse;
                x_shift = j - block_width/2 - 1;
                y_shift = i - block_height/2 - 1;
                frame = 2;
            end
        end
    end
end
