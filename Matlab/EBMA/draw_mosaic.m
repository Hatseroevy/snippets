function [] = draw_mosaic( block_height, block_width, Mosaic )
%draw_mosaic Piirt�� liikevektorien, sek� edellisen ja seuraavan framen
%avulla "nykyinen" kuva.
    figure;
    imshow(uint8(Mosaic));
    title( strcat( int2str(block_height),'x',int2str(block_width), ' lohkot'));
end

