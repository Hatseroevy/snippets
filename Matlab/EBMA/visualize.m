close all;

% Piirret��n kuva lohkoineen ja liikevektoreineen.
draw_vectors( block_height, block_width, cur_frame, LVK );
draw_mse( block_height, block_width, MSEkartta );
draw_mosaic(block_height, block_width, Mosaiikki);

% Samat pienill� lohkoilla.
draw_vectors( block_height_pienet, block_width_pienet, cur_frame, LVK_pienet );
draw_mse( block_height_pienet, block_width_pienet, MSEkartta_pienet );
draw_mosaic(block_height_pienet, block_width_pienet, Mosaiikki_pienet);

