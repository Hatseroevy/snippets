function [ Mosaic ] = calc_mosaic( block_height, block_width, prev_frame, next_frame, LVK )
%calc_mosaic Generoi liikevektorien, sek� edellisen ja seuraavan framen
%avulla "nykyinen" kuva.

    frame_width = size(prev_frame, 2);
    frame_height = size(prev_frame, 1);
    Mosaic = zeros(frame_height, frame_width, 3);
    
    for i=1:size(LVK, 1)
    	for j=1:size(LVK, 2)
            top = (i-1)*block_height + 1;
            bottom = top + block_height - 1;
            left = (j-1)*block_width + 1;
            right = left + block_width - 1;
       
            % Koska LVK n�ytt�� mist� block on nykyiseen kuvaan tullut, 
            % otetaan x- ja y-komponenteisa invertit, jotta ne saadaan 
            % piirrety� oikeille kohdilleen.
            y_component = -LVK(i,j,1);
            x_component = -LVK(i,j,2);
       
            % Siirtyneen lohkon sijainti nykyisess� kuvassa.
            new_top = (i-1)*block_height + 1 + y_component;
            new_bottom = new_top + block_height - 1;
            new_left = (j-1)*block_width + 1 + x_component;
            new_right = new_left + block_width - 1;
            
            % Tarkastellaan, meinataanko indeksoida ykk�st� pienemm�ll�
            if new_top < 1
                error = abs(top - new_top);
                top = top + error;
                new_top = 1;
            end
            if new_left < 1
                error = abs(left - new_left);
                left = left + error;
                new_left = 1;
            end

            if LVK(i,j,3) == 1
                % Otetaan piirrett�v� lohko talteen
                subblock = prev_frame(top:bottom, left:right, :);
            else
                subblock = next_frame(top:bottom, left:right, :);
            end
            % Piirret��n lohko kohdilleen.
            Mosaic(new_top:new_bottom, new_left:new_right, :) = subblock;
        end
    end
end

