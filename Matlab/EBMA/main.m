clear all;
close all;

% Luetaan framet muistiin
prev_frame = imread('rgbframe5206.png');
cur_frame = imread('rgbframe5207.png');
next_frame = imread('rgbframe5208.png');

% Määritetään lohkon koko
block_height = 30;
block_width = 32;

[MSEkartta, LVK] = calc_mse_lvk( block_height, block_width, prev_frame, cur_frame, next_frame );
Mosaiikki = calc_mosaic(block_height, block_width, prev_frame, next_frame, LVK);

% Tehdään samat pienemmillä lohkoilla.
% Määritetään lohkon koko
block_height_pienet = 20;
block_width_pienet = 20;

[MSEkartta_pienet, LVK_pienet] = calc_mse_lvk( block_height_pienet, block_width_pienet, prev_frame, cur_frame, next_frame );
Mosaiikki_pienet = calc_mosaic(block_height_pienet, block_width_pienet, prev_frame, next_frame, LVK_pienet);

run visualize;

