function [ MSEmap, LVK ] = calc_mse_lvk( block_height, block_width, prev_frame, cur_frame, next_frame )
%calc_mse_lvk Laskee nykyisen, edellisen ja seuraavan framen perusteella
%keskineli�virheen sek� liikevektorit kuvan lohkoille.

    frame_width = size(cur_frame, 2);
    frame_height = size(cur_frame, 1);
    % Edellisen ja seuraavan kuvan nollilla laajennus
    % Kuvat laajennetaan siten, ett� lopullinen koko on 4 kertaa alkuper�isen
    % kuvan kokoinen. Eli nollia on puolen blockin paksuisesti joka reunassa.
    next_frame = padarray(next_frame, [0.5*block_height, 0.5*block_width]);
    prev_frame = padarray(prev_frame, [0.5*block_height, 0.5*block_width]);

    rows = frame_height/block_height;
    cols = frame_width/block_width;
    
    MSEmap = zeros(frame_height/block_height, frame_width/block_width);
	LVK = zeros(frame_height/block_height, frame_width/block_width, 3);

    % K�yd��n lohko kerrallaan l�pi.
    for i = 0:rows-1
        for j = 0:cols-1
       
            % Alkuper�iskuvan blockin rajat/sijainti kuvan suhteen
            top = i*block_height+1;
            bottom = top + block_height - 1;
            left = j*block_width+1;
            right = left + block_width - 1;
       
            % Otetaan kuvista sopivan kokoiset lohkot tarastelua varten
            block = cur_frame( top:bottom, left:right, :);
            % Etsint�alueet otetaan edellisest� ja seuraavasta kuvasta, ja
            % ne ovat 4 kertaa isommat kuin etsitt�v� lohko.
            next_frame_block = next_frame(top:bottom+block_height, left:right+block_width, :);
            prev_frame_block = prev_frame(top:bottom+block_height, left:right+block_width, :);
       
            [y_shift, x_shift, mse, frame] = find_mse(block, prev_frame_block, next_frame_block);
       
            MSEmap(i+1,j+1) = mse;
            LVK(i+1,j+1,:) = [y_shift, x_shift, frame];
        end
    end

end

