function [] = draw_mse( block_height, block_width, MSEmap )
%draw_mse Visualisoidaan MSEkartta eli kuinka suuri keskineliövirhe on kunkin
% framen lohkon kohdalla.

    figure;
    imshow(imresize(uint8(MSEmap), [size(MSEmap, 1)*block_height size(MSEmap, 2)*block_width], 'nearest'));
    colormap(parula);
    colorbar;
    title( strcat( int2str(block_height),'x',int2str(block_width), ' lohkot'));

end

