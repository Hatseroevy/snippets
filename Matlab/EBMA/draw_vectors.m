function [] = draw_vectors( block_height, block_width, cur_frame, LVK )
%draw_vectors Piirt�� kuvan jakaen sen lohkoihin ja piirt�� lohkojen
%kohdalle liikevektorit.
%
%   Parametrit:
%       block_height, block_width: Lohkon koot.
%       cur_frame: piirrett�v� kuva.
%       LVK: liikevektorit (x,y,frame). Sis�lt�� tiedot, paljonko lohko on 
%       liikkunut edellisen/seuraavan framen ja nykyisen kuvan v�lill�.
% 
%   Paluuarvot:
%       ei ole

    figure;
	temp = cur_frame; % ei muuteta alkuper�ist� framea.
    % Muutetaan pikselit lohkojen reunalla mustiksi.
    temp( block_height:block_height:end-block_height, :, :) = 0;
    temp( :, block_width:block_width:end-block_width, :) = 0;
    imshow(temp);
    title( strcat( int2str(block_height),'x',int2str(block_width), ' lohkot' ) );

    hold on
    % Liikevektorien visualisointi
    for i=1:size(LVK, 1)
        for j=1:size(LVK, 2)
        y_component = LVK(i,j,1);
        x_component = LVK(i,j,2);
        y_pos = i*block_height-block_height/2;
        x_pos = j*block_width-block_width/2;
        % Jos lohko on liikkunut edellisest� kuvasta, piirret��n nuoli
        % sinisell�, muutoin punaisella.
        if LVK(i,j,3) == 1
            color = 'b';
        else
            color = 'r';
        end
        % Piirret��n nuoli kuvan keskelt� liikevektorin osoittamaan
        % suuntaan.
        quiver(x_pos, y_pos, x_component*10, y_component*10, color, 'MaxHeadSize', 1);
        end
    end
    hold off
end

