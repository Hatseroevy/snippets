var express = require('express');
var passport = require('passport');
var app = express();
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Comment = mongoose.model('Comment');

router.get('/comment/:id', function (req, res) {

  var id = req.params.id;

  Comment.findOne({'_id':id}, function( err, comment ){

    if( comment && req.user ){
      res.render('comment', {user: req.user.username, comment: comment});
    } 

    else {
      res.render('nopage');
    }
    
  });

});

module.exports = router;