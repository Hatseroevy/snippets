var express = require('express');
var passport = require('passport');
var app = express();
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');


router.get('/register', function(req, res) {
  var user;
  // Let's send only username because it might not be a good idea to send the hash as well..
  if( req.user ){ user = req.user.username; }
  res.render('auth', 
    {
      user: user, 
      post_url: 'register',
      message: 'Welcome! Go ahead and fill the fields below to register.'
    });
});

router.get('/registerError', function(req, res) {
  var user;
  if( req.user ){ user = req.user.username; }
  res.render('auth', 
    {
      user: user, 
      post_url: 'register',
      message: 'Woops, bad username or password. Try something else.'
    });
});

router.post('/register', function(req, res, next) {
  console.log('registering user');
  User.register(new User({username: req.body.username}), req.body.password, function(err) {
    if (err) {
      console.log('error while user register!', err);
      res.redirect('/registerError');
      return next(err);
    }

    console.log('user registered!');

    res.redirect('/');
  });
});

router.get('/login', function(req, res) {
  var user;
  if( req.user ){ user = req.user.username; }
  res.render('auth', 
    {
      user: user, 
      post_url: 'login',
      message: ''
    });
});

router.get('/loginError', function(req, res) {
  var user;
  if( req.user ){ user = req.user.username; }
    res.render('auth', 
      {
        user: user, 
        post_url: 'login', 
        message: 'Woops! Bad entry I believe. Try something else.'
      });
});

router.post('/login', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/loginError'
}));

router.get('/logout', function(req, res) {
  req.logout();
  res.render('auth',
      {
        post_url: 'login', 
        message: 'Come back soon!'
      });
});

module.exports = router;
