var express = require('express');
var passport = require('passport');
var app = express();
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');

/* GET home page. */
router.get('/', function (req, res) {
  var user;
  if( req.user ){ user = req.user.username; }
  res.render('index', {user: user});
});

/* GET home page. */
router.get('/api', function (req, res) {

  var user;
  if( req.user ){ user = req.user.username; }
  res.render('api', {user: user});

});

module.exports = router;
