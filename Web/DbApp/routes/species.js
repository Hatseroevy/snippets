var express = require('express');
var app = express();
var router = express.Router();
var mongoose = require('mongoose');
var Comment = mongoose.model('Comment');
var Species = mongoose.model('Species');
var User = mongoose.model('User');
var passport = require('passport');


/* GET species page. */

router.get('/species/:id', function (req, res) {

  var id = req.params.id;

  Species.findOne({'_id':id}, function( err, species ){

    if( species ){

      var user;
      var username;
      var userid;
      var comments = [];
      var checkedComments = [];
      var hasCommented = false;
      var commentIsOwn = false;
      var commentWriter = "Undefined user";
      var commentWriterIds = [];

      if( req.user ){
        username = req.user.username;
        userid = req.user._id;
        user = {username: username, userid: userid }; 
      }

      Comment.find({ 'topic':id }, function( err, results ){
        comments = results;
        for( i = 0; i < comments.length; i++ ){
          commentWriterIds.push(comments[i].user);
        }

        // Find the users to get their usernames....
        User.find( {'_id': {$in : commentWriterIds}}, function( err, users ){
          for( i = 0; i < comments.length; i++ ){
            for( j = 0; j < users.length; j++ ){
              if( comments[i].user.equals(users[j]._id) ){

                if( comments[i].user.equals( userid ) ){ 
                  commentIsOwn = true; 
                  hasCommented = true; 
                } else { commentIsOwn = false; }

                checkedComments.push({
                  comment: comments[i],
                  own: commentIsOwn,
                  writer: users[j].username
                });
              }
            }
          }
          res.render('species', { 
            species: species,
            comments: checkedComments,
            commented: hasCommented,
            user: user          
          });
        });

      });
    } 

    else {
      res.redirect('/species');
    }
    
  });
  
});


/* GET species list page. */
router.get('/species', function (req, res) {

    Species.find().sort( {genus: 1} ).exec ( function(err, species) {
    
    var results = [];

  	for( index = 0; index < species.length; ++index ){
      results.push( { genus: species[index].genus, species: species[index].species, url: "/species/" + species[index]._id } );
  	}

    var user;
    if ( req.user ){
      user = req.user.username;
    }

  	res.render('specieslist', { results: results, user: user });

  });


});

module.exports = router;