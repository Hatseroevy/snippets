'use strict';

var mongoose = require('mongoose'),
		Schema = mongoose.Schema,
		ObjectId = Schema.ObjectId;

var fields = {
	topic: { type: Schema.ObjectId }, // Species' id.
	user: { type: Schema.ObjectId }, // Writer's id.
	content: { type: String },
	created: { type: Date , default: Date.now } 
};

var commentSchema = new Schema(fields);

module.exports = mongoose.model('Comment', commentSchema);