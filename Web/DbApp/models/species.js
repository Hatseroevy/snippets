'use strict';

var mongoose = require('mongoose'),
		Schema = mongoose.Schema,
		ObjectId = Schema.ObjectId;

var careInfoFields = {
	general_hardness: { type: String },
	temperature: { type: String },
	pH: { type: String }
};

var careInfoSchema = new Schema(careInfoFields, { _id : false });

var speciesFields = {
	species: { type: String },
	genus: { type: String },
	careInfo: careInfoSchema
};

var speciesSchema = new Schema(speciesFields);

module.exports = mongoose.model('Species', speciesSchema);