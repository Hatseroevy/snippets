'use strict';

var mongoose = require('mongoose'),
		Schema = mongoose.Schema,
		ObjectId = Schema.ObjectId,
		passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema({

});

var options = ({missingPasswordError: "Foutief password"});
User.plugin(passportLocalMongoose, options);

module.exports = mongoose.model('User', User );