// Module dependencies.
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    User = mongoose.models.User,
    api = {};

// ALL
api.users = function (req, res) {
  User.find(function(err, users) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json({users: users});
    }
  });
};

// GET
api.user = function (req, res) {
  var id = req.params.id;
  User.findOne({ '_id': id }, function(err, user) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json({user: user});
    }
  });
};


// DELETE
api.deleteUser = function (req, res) {
  var id = req.params.id;
  User.findById(id, function (err, user) {
    return user.remove(function (err) {
      if (!err) {
        console.log("removed user");
        return res.status(204).send();
      } else {
        console.log(err);
        return res.status(500).json(err);
      }
    });
  });

};


router.get('/users', api.users);

router.route('/users/:id')
  .get(api.user)
  .delete(api.deleteUser);


module.exports = router;
