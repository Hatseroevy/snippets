var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    Comment = mongoose.models.Comment,
    api = {};

// ALL
api.comments = function (req, res) {
  Comment.find(function(err, comments) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json({comments: comments});
    }
  });
};

// GET
api.comment = function (req, res) {
  var id = req.params.id;
  Comment.findOne({ '_id': id }, function(err, comment) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json({comment: comment});
    }
  });
};

// POST
api.addComment = function (req, res) {

  if( !req.user ){
    return res.status(401).json({message:'Unauthorized'});
  }

  var comment;

  if(typeof req.body.comment == 'undefined'){
    return res.status(400).json({message: 'comment is undefined'});
  }

  comment = new Comment(req.body.comment);

  comment.save(function (err) {
    if (!err) {
      console.log("created comment");
      return res.status(201).json(comment.toObject());
    } else {
      console.log(err);
      return res.status(500).json(err);
    }
  });

};

// PUT
api.editComment = function (req, res) {
  var id = req.params.id;

  if( !req.user ){
    return res.status(401).json({message:'Unauthorized'});
  }

  Comment.findById(id, function (err, comment) {
    if( !comment.user.equals(req.user._id ) ){
      return res.status(401).json( {message:'Unauthorized'});
    }

    if(typeof req.body.comment["content"] != 'undefined'){
      comment["content"] = req.body.comment["content"];
    }

    return comment.save(function (err) {
      if (!err) {
        console.log("updated comment");
        return res.status(200).json(comment.toObject());
      } else {
       return res.status(500).json(err);
      }
      return res.status(200).json(comment);
    });
  });

};

// DELETE
api.deleteComment = function (req, res) {
  if( !req.user ){
    return res.status(401).json({message:'Unauthorized'});
  }
  var id = req.params.id;
  Comment.findById(id, function (err, comment) {
    if( !comment.user.equals(req.user._id ) ){
      return res.status(401).json( {message:'Unauthorized'});
    }
    return comment.remove(function (err) {
      if (!err) {
        console.log("removed comment");
        return res.status(204).send();
      } else {
        console.log(err);
        return res.status(500).json(err);
      }
    });
  });

};


router.get('/comments', api.comments);
router.post('/comments', api.addComment);

router.route('/comments/:id')
  .get(api.comment)
  .put(api.editComment)
  .delete(api.deleteComment);

module.exports = router;