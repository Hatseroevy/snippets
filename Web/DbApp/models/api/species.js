var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    Species = mongoose.models.Species,
    api = {};

// ALL
api.allSpecies = function (req, res) {
  Species.find(function(err, species) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json({species: species});
    }
  });
};

// GET
api.species = function (req, res) {
  var id = req.params.id;
  Species.findOne({ '_id': id }, function(err, species) {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json({species: species});
    }
  });
};

// POST
api.addSpecies = function (req, res) {

  var newSpecies;
  var genus;
  var species;
  var careInfo;

  if(typeof req.body.species == 'undefined'){
    return res.status(400).json({message: 'species is undefined'});
  }
  genus = req.body.species.genus;
  species = req.body.species.species;
  careInfo = req.body.species.careInfo;
  if( !genus || !species ){
    return res.status(400).json({message: 'species is undefined'});
  }
  var gpatt = /[A-Z][a-z]*/g;
  var spatt = /[a-z]*/g;
  if( !gpatt.test( genus) || !spatt.test( species ) ){
    return res.status(400).json({message: 'invalid species name'});
  }
  Species.findOne({ 'genus': genus, 'species': species }, function(err, species) {
    if (err) {
      return res.status(500).json(err);
    } else  if(species){
      return res.status(400).json({message: 'species already exists'});
    } else {

      newSpecies = new Species(req.body.species);
      if( !careInfo ){
        newSpecies["careInfo"] = {pH: '', general_hardness: '', temperature: ''};
      }
      newSpecies.save(function (err) {
        if (!err) {
          console.log("created species");
          return res.status(201).json({species: newSpecies.toObject()});
        } else {
          return res.status(500).json(err);
        }
      });
    }
  });
};

// PUT
api.editSpeciesName = function (req, res) {
  var id = req.params.id;
  if(typeof req.body.species == 'undefined'){
    return res.status(400).json({message: 'species is undefined'});
  }

  Species.findById(id, function (err, species) {

    if( !species ){
      return res.status(400).json({message: 'species id does not exist'});
    }


    if(typeof req.body.species["species"] != 'undefined'){
      var species = req.body.species["species"];
      var spatt = /[a-z]*/g;
      if( !spatt.test( species ) ){
        return res.status(400).json({message: 'invalid species name'});
      }
      species["species"] = req.body.species["species"];
    }

    if(typeof req.body.species["genus"] != 'undefined'){
      var genus = req.body.species["genus"];
      var gpatt = /[A-Z][a-z]*/g;
      if( !gpatt.test( genus ) ){
        console.log("Not a match");
        return res.status(400).json({message: 'invalid genus name'});
      }
      species["genus"] = req.body.species["genus"];
    }


    return species.save(function (err) {
      if (!err) {
        console.log("updated species name");
        return res.status(200).json({species: species.toObject()});
      } else {
       return res.status(500).json(err);
      }
      return res.status(200).json(species);
    });
  });
};


// PUT
api.editSpeciesInfo = function (req, res) {
  var id = req.params.id;

  Species.findById(id, function (err, species) {

    if(typeof req.body.careInfo != 'undefined'){
      if( req.body.careInfo.pH ){
        species["careInfo"].pH = req.body.careInfo.pH;
      }
      if( req.body.careInfo.general_hardness ){
        species["careInfo"].general_hardness = req.body.careInfo.general_hardness;
      }
      if( req.body.careInfo.temperature ){
        species["careInfo"].temperature = req.body.careInfo.temperature;
      }
    }


    return species.save(function (err) {
      if (!err) {
        console.log("updated species info");
        return res.status(200).json(species.toObject());
      } else {
       return res.status(500).json(err);
      }
      return res.status(200).json(species);
    });
  });

};

// DELETE
api.deleteSpecies = function (req, res) {
  var id = req.params.id;
  Species.findById(id, function (err, species) {
    return species.remove(function (err) {
      if (!err) {
        console.log("removed species");
        return res.status(204).send();
      } else {
        console.log(err);
        return res.status(500).json(err);
      }
    });
  });

};

router.get('/species', api.allSpecies);
router.post('/species', api.addSpecies);

router.route('/species/:id')
  .get(api.species)
  .put(api.editSpeciesName)
  .delete(api.deleteSpecies);

router.route('/species/:id/name')
  .put(api.editSpeciesName)

router.route('/species/:id/info')
  .put(api.editSpeciesInfo)

module.exports = router;