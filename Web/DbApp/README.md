Author: Nina Anttila

A course work.

An Express-application using MongoDB. Users can register/log in and out, add species into system and comment on them. The app also has an API to modify what's stored. jQuery used to make a nicer UI.

Node_modules are not included here. Install dependencies with 'npm install'. Then it shoud be good to go with 'npm start'. Also you need to have a Mongo database up and running.