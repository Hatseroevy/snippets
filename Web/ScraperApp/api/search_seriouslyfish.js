var express = require('express');
var router = express.Router();
var request = require('request');
var cheerio = require('cheerio');

/* GET  speciesdata. */
router.get('/search_seriouslyfish', function(req, res, next) {

  var species = req.query.speciesname;
  if( !species ){
    return res.status(400).json( {message: "Invalid species name provided."} );
  } 

  species = species.split( ' ' );
  // If the length isn't 2 then it's not a scientific name of a species.
  // Right now, the names such as Apistogramma sp. 'breitbinden' are not allowed. :|
  if( species.length !== 2 ){
    return res.status(400).json( {message: "Invalid species name provided."} );
  } 

  var url = 'http://www.seriouslyfish.com/species/' +species[0]+ '-'+ species[1];
	
  request( url, function( error, response, html ){

 		if( !error && response.statusCode == 200 ){

      var $ = cheerio.load(html);

  		$('.profile_title').filter(function(){

        var index = $(this);
        var profiledata = $(this).nextUntil('#comments');
        var data = [];
        var header = "";
        var text = [];
        var count = 0;

        profiledata.each( function(i, elem){

          count += 1;
          var tagname = $(this).prop("tagName").toLowerCase();

          if( tagname.localeCompare( "h1" ) == 0 ){
              
            header = "Common name";
            text.push( $(this).text() );

          } else if ( tagname.localeCompare( "h2" ) == 0){

            header = $(this).text();

            // Some headers have dirt on them ...
            if( header.indexOf("Top") !== -1 ){
              header = header.substring( 0, header.indexOf("Top") );
            }

          } else if ( tagname.localeCompare( "p" )  == 0){

            text.push( $(this).text() );

          }

          // Push when both fields full and all the paragraphs have been added.
          // Checking count in case there's nothing after the last paragrah
          // and doing compare in case there's something before the comments, 
          // such as references in <ol>-element.
          if( 0 < header.length && 0 < text.length && ( count == profiledata.length || 
              $(this).next().prop("tagName").toLowerCase().localeCompare("p") !== 0 ) ){
              data.push( { header: header, text: text } );
              header = "";
              text = [];
          }
            
          if( count == profiledata.length ){
            return res.status(200).json({ data: data });
          }
   			});
    	});

  	} else {

  		if( error ){
  			return res.status(500).json(error);
  		} else {
  			return res.status(404).json({message: 'No data available :('})
  		}

  	}
	}); 

});

module.exports = router;
