Author: Nina Anttila

A simple scraper app. It scrapes data from SeriouslyFish.com when given a scientific name for a fish species.

Node_modules are not included here. Install dependencies with 'npm install'. Then it shoud be good to go with 'npm start'.