
package main

import (
	"fmt"
	"math/rand"
	"time"
	"sync"
	"strconv"
)


// Car's information.
// Id, a starting place (either A or B), and a goal (either C or D)
// To get to the goal D, it need to go through L
type Car struct {
	id int
	start string
	end string
}


// Ferry is used to prevent anyone from falling into water.
// Mutex prevents can's getting on the ferry when it's not landed at the dock L.
// Condition L and D are signaled when the ferry lands on the corresponding dock.
type Ferry struct {
	mutex *sync.Mutex
	condL *sync.Cond
	condD *sync.Cond
}


// Traffiglights are used to make sure the right cars are getting on 
// the intersection at their time.
// trafficA and trafficB lets cars from corresponding directions to
// enter the intersection.
// fromA = true, when traffic from A is allowed, false when B is allowed
// to enter the intersection.
// queueA and queueB tells the amount of waiting cars.
type TraffigLights struct {
	trafficA chan bool
	trafficB chan bool
	intersection chan bool
	fromA bool
	queueA int
	queueB int
}


// First car goes to traffic lights where it waits the light to turn green.
// = it gets a value out of either trafficA or trafficB channel.
// Then it reserves intersection for itself, since only one at the time
// is allowed there.
// = it takes a value out and once it's left the area, it puts a new value in.
// After that it either leaves or goes for a ferry ride and then leaves.
func carOnTheGo( car Car, wg *sync.WaitGroup, ferry *Ferry, 
	lights *TraffigLights, printouts chan string ) {
	
	var message string

    message = "Car " + strconv.Itoa( car.id ) + " created, route: " + 
		car.start + "->" + car.end + "."
	printouts <- message

	// The first thing to do, is to go to traffic lights.
    message = "Car " + strconv.Itoa( car.id) + 
        " comes up to traffic lights."
    printouts <- message

    // Depending on where you want to go, go to the right line.
    if car.start == "A" {

    	// Tell the lights you're waiting.
    	lights.queueA += 1
    	// Wait for permission to go on.
    	<- lights.trafficA

    } else {

    	lights.queueB += 1
    	<- lights.trafficB

    }

    // No more than one car allowed on the intersection at the time to
    // prevent collisions. Then inform the channel the intersection is now 
    // yours by taking the value out of channel.
    <- lights.intersection

	message = "Car " + strconv.Itoa( car.id ) + " is on the intersection."
	printouts <- message

	message = "Car " + strconv.Itoa( car.id ) + " left the intersection."
	printouts <- message

    // Inform you don't need the channel anymore.
    lights.intersection <- true
    

    // If going to D, first need to go to L for a ferry ride.
    if car.end == "D" {

    	message = "Car " + strconv.Itoa( car.id ) + " is waiting for the ferry."
    	printouts <- message

    	ferry.mutex.Lock()

    	// Wait for the ferry's arrival to dock L.
    	ferry.condL.Wait()
    	message = "Car " + strconv.Itoa( car.id ) + " is getting on the ferry."
    	printouts <- message

    	// Wait for the ferry to arrive at dock D.
    	ferry.condD.Wait()

    	ferry.mutex.Unlock()

    }

    message = "Car " + strconv.Itoa( car.id ) + " exited from " + car.end
    printouts <- message

	// Inform waitgroup you have finished.
	wg.Done()

}


// Turns the lights green for either traffic coming from A or B.
func lightsOn( lights *TraffigLights, printouts chan string ) {

    var message string
    message = "Traffic lights are on! First to go are those coming from A."
    printouts <- message

	for {

		alku := time.Now()
		loppu := time.Now()

		// Wait for 5 seconds and then let the traffic from the other
		// direction to go.
		for loppu.Sub(alku) < 5*time.Second {

			// If someone's waiting and it's their turn, give the a signal.
			if lights.fromA && lights.queueA > 0 {

				lights.trafficA <- true
				lights.queueA -= 1
			}

			if !lights.fromA && lights.queueB > 0 {

				lights.trafficB <- true
				lights.queueB -= 1
			}

			loppu = time.Now()
		}

		// Light changes.
		lights.fromA = !lights.fromA

		message = "Signal changed, now it's "
		if lights.fromA {
			message += "A's turn."
		} else {
			message += "B's turn."
		}

		printouts <- message
	}
}


// Every 7 seconds ferry arrives to either one of the docks and 
// informs the waiting cars that they may climb on.
func ferryOnTheGo( ferry *Ferry, printouts chan string ){

	var message string
	message = "The ferry in on the go."
    printouts <- message

	for {

		time.Sleep( time.Duration( 7*time.Second ) )
		ferry.mutex.Lock()
		message = "The ferry arrives to L."
		printouts <- message
		// Inform everyone waiting.
		ferry.condL.Broadcast()
		ferry.mutex.Unlock()

		time.Sleep( time.Duration( 7*time.Second ) )
		ferry.mutex.Lock()
		message = "The ferry arrives to D."
		printouts <- message
		ferry.condD.Broadcast()
		ferry.mutex.Unlock()
	}
}



// Printers job is to print anything that's added to printouts channel.
func printer( printouts chan string ){

	// Listen constantly
	for {
		message := <- printouts
		fmt.Println( message )
	}
}



// Let's create 20 cars, a.k.a. goroutines, which move from A or B to C or D.
// If the car is going to D, before it gets there, it needs to wait for a ferry
// in L, which then will take it from L to D.
// Waitgroup waits for the cars to finish, making sure the programm doesn't finish
// before all the cars have exited.
// Traffic lights, the ferry and the printouts have their 
// own goroutines as well.

//  Map:
//	Ferry goes between L and D
//	'I' stands for intersection
//
//		L ~	~ ~ ~ ~ D
//		|
// A----I-----------C
//		|
//		B

func main() {
	
	var wg sync.WaitGroup
	wg.Add(20)

    printouts := make( chan string )
    go printer( printouts )

    var lights TraffigLights
    lights.intersection = make( chan bool, 1 )
    lights.trafficA = make( chan bool )
    lights.trafficB = make( chan bool )
    lights.intersection <- true
    lights.queueA = 0
    lights.queueB = 0
    lights.fromA = true
	go lightsOn( &lights, printouts )

	var ferry Ferry
	ferry.mutex = &sync.Mutex{}
	ferry.condD = sync.NewCond( ferry.mutex ) 
	ferry.condL = sync.NewCond( ferry.mutex )
	go ferryOnTheGo( &ferry, printouts );

 	var randInt int
    rand.Seed( time.Now().UTC().UnixNano() );
    var str1, str2 string

    for i := 1; i <= 20; i++ {

    	// Rand for starting and ending points.
    	randInt = rand.Intn( 30 );
    	if randInt % 2 == 1{
    		str1 = "A"
    	} else {
    		str1 = "B"
    	}
    	randInt = rand.Intn( 30 );
    	if randInt % 2 == 0 {
    		str2 = "C"
    	} else {
    		str2 = "D"
    	}
    	car := Car{ i, str1, str2 }
        go carOnTheGo( car, &wg, &ferry, &lights, printouts );
        // Create a new car after 0-3 seconds.
        time.Sleep( time.Duration( rand.Intn( 3 ) )*time.Second )
    }

    // Wait for every car to finish.
    wg.Wait(); 

}
