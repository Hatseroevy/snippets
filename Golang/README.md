Author: Nina Anttila

A bonus addition for a course assignment work.

- Cars (threads) start from place A or B and travel to C or D.
- First they need to go to traffic lights (channels).
	- For 5 seconds, cars from A are allowed to go, after that the cars from B are allowed to go for 5s. Then repeat.
- A car can only go to intersection if it's free and it's it's turn.
- Only one car at the time is allowed on the intersection to avoid collisions. (Safety first!)
- Then the cars leave the map through C or go to point L to wait for a ferry (mutex).
- The ferry picks up every waiting car from L and takes them to D where they can exit.
	- During the ride no new cars are picked up.
