//Public class joka pitää tallessa alusten statesja
public class ShipData {
  private PImage image_;
  private String spriteName_;
  private float speed_;
  private int shields_;
  private int maxShields_;
  private float acceleration_;
}

class Menu {
  private boolean start_;
  private boolean options_;
  private boolean hiScores_;
  private boolean quit_;
  private boolean gameStart_;
  private boolean hiScoresRead_;
  private boolean soundMute_;
  private boolean musicMute_;
  private boolean sounds_;
  private boolean music_;
  private boolean reset_;
  private boolean resolutionSelected_;

  private int startColor_;
  private int optionsColor_;
  private int hiScoresColor_;
  private int quitColor_;
  private int soundsColor_;
  private int musicColor_;
  private int resetColor_;
  private int resolutionColor_;

  private int selected_;
  private int optionsSelected_;
  private int menuState_;
  private int waitTime_;
  private int resolutionState_;

  final int MAINMENU = 0, NAMEINPUT = 1, WELCOME = 2, OPTIONS = 3, HISCORES = 4;
  final int selectedColor_ = 255;
  final int noSelectedColor_ = #1fd2ff;

  Hiscores hiscores;
  PImage menuSprite_;
  PImage nameInputSprite_;
  PImage levelScreenSprite_;
  PrintWriter output;
  BufferedReader reader;

  private String soundState_;
  private String musicState_;
  //String fullScreenState_;
  private String currResolution_;

  // Aluksen valinnassa tarvittavia muuttujia //
  private boolean newSelected_;
  private boolean spriteTurned_;
  // Kertoo aluksen valinnassa mikä alus on valittuna
  private int selectedShip_;
  private int nextSelectedShip_;
  ArrayList<ShipData> shipDatas_;
  private int imageWidth_;
  private int imageHeight_;
  PImage leftArrow_;
  PImage rightArrow_;
  private float scaleLeft_;
  private float scaleRight_;

  //Alustetaan ikkuna oikean kokoiseksi ja muuttujille annetaan arvot
  Menu() {
    textAlign(CENTER);
    readOptionsFile();
    startColor_ = selectedColor_;
    optionsColor_ = noSelectedColor_;
    hiScoresColor_ = noSelectedColor_;
    quitColor_ = noSelectedColor_;
    musicColor_ = noSelectedColor_;
    resetColor_ = noSelectedColor_;
    resolutionColor_ = noSelectedColor_;

    selected_ = 1;
    optionsSelected_ = 0;
    hiScoresRead_ = false;
    menuState_ = MAINMENU;
    gameStart_ = false;
    hiscores = new Hiscores();
    menuSprite_ = loadImage("sprites/menutesti.png");
    nameInputSprite_ = loadImage("sprites/nameinput2.png");
    levelScreenSprite_ = loadImage("sprites/pausescreen.png");
    changeResolutionState();

    selectedShip_ = 0;
    newSelected_ = true;
    spriteTurned_ = true;
    imageWidth_ = 60;
    imageHeight_ = 60;
    shipDatas_ = new ArrayList<ShipData>();
    readShips();
    leftArrow_ = loadImage("sprites/leftArrow2.png");
    rightArrow_ = loadImage("sprites/rightArrow2.png");
    scaleLeft_ = 1.0;
    scaleRight_ = 1.0;

    //Asteusten On/Off tilaa muuttavia funktioita
    if (!soundMute_) {
      soundState_ = "On";
    }
    else {
      soundState_ = "Off";
    }

    if (!musicMute_) {
      musicState_ = "On";
    }
    else {
      musicState_ = "Off";
    }
  }

  //Pelin aloitusbooleanin getteri
  boolean getGameStart() {
    return gameStart_;
  }

  //Menun getterifunktioita
  int getMenuState() {
    return menuState_;
  }

  void setMenuState(int n) {
    menuState_ = n;
  }

  void setOptionsState(int n) {
    optionsSelected_ = n;
  }

  int getOptionsState() {
    return optionsSelected_;
  }

  boolean getSoundMute() {
    return soundMute_;
  }

  boolean getMusicMute() {
    return musicMute_;
  }

  int getResolution() {
    return resolutionState_;
  }

  //Resetataan gameStart boolean, jotta peli saadaan uudestaan
  //alkuvalikkoon, kun se loppuu
  void resetGameStart() {
    gameStart_ = false;
  }

  //Lisätään pelaaja hiscoreihin
  void hiscoresAddCaller() {
    hiscores.addPlayer();
  }

  //hiscorejen luku
  void readHiscores() {
    hiScoresRead_ = hiscores.read();
  }

  //Muutetaan valitsimen arvoa kun liikutaan menussa
  void selectedUp() {
    ++selected_;
    if (selected_ > 4) {
      selected_ = 1;
    }
  }
  //Muutetaan valitsimen arvoa kun liikutaan menussa
  void selectedDown() {
    --selected_;
    if (selected_ < 1) {
      selected_ = 4;
    }
  }
  //Valitsimen paikan muutto asetuksissa
  void optionsUp() {
    ++optionsSelected_;
    if (optionsSelected_ > 4) {
      optionsSelected_ = 1;
    }
  }

  void optionsDown() {
    --optionsSelected_;
    if (optionsSelected_ < 1) {
      optionsSelected_ = 4;
    }
  }

  //Menurakenteen piirtäminen.
  void display() {
    imageMode(CENTER);

    switch (menuState_) {
      //Perus mainmenu-rakenne.
    case MAINMENU:
      //background(0);
      image(menuSprite_, width/2, height/2);
      textSize(50);
      fill(255);
      text("Game About Asteroids", width/2, height/2-220);
      textSize(20);
      fill(startColor_);
      text("Start game", width/2, height/2-75);
      fill(optionsColor_);
      text("Options", width/2, height/2-25);
      fill(hiScoresColor_);
      text("HiScores", width/2, height/2+25);
      fill(quitColor_);
      text("Quit", width/2, height/2+75);
      break;

      //Menu silloin, kun käyttäjältä kysytään nimeä.
    case NAMEINPUT:
      image(nameInputSprite_, width/2-2, height/2-35);
      textSize(20);
      fill(255);
      text("Enter your name and choose your ship", width/2, height/2-120);
      textSize(18);
      text("Name: ", width/2-130, height/2-68);
      fill(#1fd2ff);
      textAlign(LEFT);
      text("Ship Stats:", width/2+50, height/2-40);
      text("Speed: "+int(shipDatas_.get(selectedShip_).speed_), width/2+50, height/2-20);
      text("Acceleration: "+int(shipDatas_.get(selectedShip_).acceleration_*100), width/2+50, height/2);
      text("Shields: "+int(shipDatas_.get(selectedShip_).shields_), width/2+50, height/2+20);
      text("Max. Shields: "+int(shipDatas_.get(selectedShip_).maxShields_), width/2+50, height/2+40);
      // Jos uusi valinta on kesken
      if (newSelected_ == false) {
        // Jos kuvan koko on yli 0 ja kuva ei ole vielä kääntynyt
        if (imageWidth_ > 0 && spriteTurned_ == false) {
          // Pienennetään kuvan kokoa
          imageWidth_ -= 10;
        }
        else {
          // Asetetaan kuvan kääntymisestä kertova muuttuja todeksi
          spriteTurned_ = true;
          // Siirrytään piirtämään uutta kuvaa
          selectedShip_  = nextSelectedShip_;
          // Jos kuva ei ole vielä täysikokoinen
          if (imageWidth_ < 60) {
            // Kasvatetaan kuvan kokoa
            imageWidth_ += 10;
            scaleLeft_ = 1.0;
            scaleRight_ = 1.0;
          }
          else {
            // Asetetaan uudesta valinnasta kertova muuttuja todeksi
            newSelected_ = true;
          }
        }
      }
      // Piirretään valittu alus ja reunoille nuolet
      image(shipDatas_.get(selectedShip_).image_, width/2-93, height/2, imageWidth_, imageHeight_);
      image(leftArrow_, width/2-174, height/2, leftArrow_.width*scaleLeft_, leftArrow_.height*scaleLeft_);
      image(rightArrow_, width/2-10, height/2, rightArrow_.width*scaleRight_, rightArrow_.height*scaleRight_);
      break;

      //Nimen syötön jälkeen pelaajalle tulostetaan 2-sekunnin
      //tervetuloviesti, jonka jälkeen peli alkaa.
    case WELCOME:
      if (millis() - waitTime_ < 2000) {
        background(0);
        image(nameInputSprite_, width/2-2, height/2-35);
        textSize(20);
        fill(255);
        text("Welcome!", width/2, height/2-100);
        fill(#1fd2ff);
        textSize(18);
        text(playerName, width/2, height/2-80);
        image(shipDatas_.get(selectedShip_).image_, width/2-93, height/2, imageWidth_, imageHeight_);
        textAlign(LEFT);
        text("Ship Stats:", width/2+50, height/2-40);
        text("Speed: "+int(shipDatas_.get(selectedShip_).speed_), width/2+50, height/2-20);
        text("Acceleration: "+int(shipDatas_.get(selectedShip_).acceleration_*100), width/2+50, height/2);
        text("Shields: "+int(shipDatas_.get(selectedShip_).shields_), width/2+50, height/2+20);
        text("Max. Shields: "+int(shipDatas_.get(selectedShip_).maxShields_), width/2+50, height/2+40);
        textAlign(CENTER);
      }

      //Kun kaksi sekunttia on kulunut, lähtee peli käyntiin
      else {
        menuState_ = MAINMENU;
        gameStart_ = true;
      }
      break;

      //Asetusten piirtäminen
    case OPTIONS:
      textAlign(CENTER);
      image(menuSprite_, width/2, height/2);
      textSize(50);
      fill(255);
      text("Game About Asteroids", width/2, height/2-220);
      textSize(20);
      fill(soundsColor_);
      text("Sounds: " + soundState_, width/2, height/2-75);
      fill(musicColor_);
      text("Music: " + musicState_, width/2, height/2-25);
      fill(resolutionColor_);
      text("Resolution: " + currResolution_, width/2, height/2+25);
      fill(resetColor_);
      text("Reset", width/2, height/2+75);


      break;

      //Hiscroejen piirtäminen
    case HISCORES:
      // Jos menumusiikki ei soi, laitetaan se soimaan
      if (menuMusic.isPlaying() == false && !musicMute_ ) {
        menuMusic.rewind();
        menuMusic.play();
      }
      hiscores.display();
      break;
    }
  }
  //Liikkuminen menussa. Aktiivinen valinta  näytetään
  //harmaana ja ei-aktiiviset valkoisina.
  //Liikkuminen tapahtuu nuolinäppäimillä.
  void menuMover() {

    switch(menuState_) {

      //Päävalikossa liikkuminen
    case MAINMENU:
      if (selected_ == 1) {
        start_ = true;
        startColor_ = selectedColor_;
        options_ = false;
        optionsColor_ = noSelectedColor_;
        quit_ = false;
        quitColor_ = noSelectedColor_;
      }
      else if (selected_ == 2) {
        options_ = true;
        optionsColor_ = selectedColor_;
        start_ = false;
        startColor_ = noSelectedColor_;
        hiScores_ = false;
        hiScoresColor_ = noSelectedColor_;
      }
      else if (selected_ == 3) {
        hiScores_ = true;
        hiScoresColor_ = selectedColor_;
        options_ = false;
        optionsColor_ = noSelectedColor_;
        quit_ = false;
        quitColor_ = noSelectedColor_;
      }
      else if (selected_ == 4) {
        quit_ = true;
        quitColor_ = selectedColor_;
        hiScores_ = false;
        hiScoresColor_ = noSelectedColor_;
        start_ = false;
        startColor_ = noSelectedColor_;
        break;
      }

      //Asetuksissa liikkuminen
    case OPTIONS:

      if (optionsSelected_ == 1 || optionsSelected_ == 0) {
        sounds_ = true;
        music_ = false;
        reset_ = false;
        soundsColor_ = selectedColor_;
        musicColor_ = noSelectedColor_;
        resetColor_ = noSelectedColor_;
        resolutionColor_ = noSelectedColor_;
      }
      else if (optionsSelected_ == 2) {
        music_ = true;
        sounds_ = false;
        reset_ = false;
        musicColor_ = selectedColor_;
        soundsColor_ = noSelectedColor_;
        resetColor_ = noSelectedColor_;
        resolutionColor_ = noSelectedColor_;
      }
      else if (optionsSelected_ == 3) {
        sounds_ = false;
        reset_ = false;
        resolutionSelected_ = true;
        musicColor_ = noSelectedColor_;
        soundsColor_ = noSelectedColor_; 
        resetColor_ = noSelectedColor_;
        resolutionColor_ = selectedColor_;
      }
      else if (optionsSelected_ == 4) {
        music_ = false;
        sounds_ = false;
        reset_ = true;
        resolutionSelected_ = false;
        musicColor_ = noSelectedColor_;
        soundsColor_ = noSelectedColor_; 
        resetColor_ = selectedColor_;
        resolutionColor_ = noSelectedColor_;
      }

      break;
    }
  }

  //Entteriä painamalla menu suorittaa halutun
  //toiminnon. Toimintoja kutsutaan tällä funktiolla
  void menuSelector() {
    if (start_) {

      //Ensimmäisen entterin painalluksen jälkeen pelaaja
      //syöttää pelille nimimerkkinsä.
      if (menuState_ == NAMEINPUT) {
        menuState_ = WELCOME;
        playerName = typing;
        typing = "";
        waitTime_ = millis();
      }
      else {
        menuState_ = NAMEINPUT;
      }
    }

    if (options_) {
      menuState_ = OPTIONS;
    }

    if (hiScores_) {
      menuState_ = HISCORES;
    }

    if (quit_) {
      menuMusic.pause();
      select.pause();
      player.stop();
      exit();
    }
  }


  //Tämä funktio piirtää kenttien välissä laatikon, joka kertoo mikä kenttä on seuraavaksi alkamassa.
  void displayLevelNumber(int level) {

    background(0);
    image(levelScreenSprite_, width/2, height/2, levelScreenSprite_.width/2, levelScreenSprite_.height/2);
    text("Level "+level, width/2, height/2);
  }

  //Vaihdetaan mute joko pois tai päälle.
  void changeSoundMute() {
    if (soundMute_) {
      soundState_ = "On";
      soundMute_ = false;
    }
    else {
      soundState_ = "Off";
      soundMute_ = true;
    }
  }

  void changeMusicMute() {
    if (musicMute_) {
      musicState_ = "On";
      musicMute_ = false;
    }
    else {
      musicState_ = "Off";
      musicMute_ = true;
    }
  }

  //Alustetaan hiscoret uudestaan
  void resetHiscores() {
    hiscores.reset();
  }

  //Resoluution muuttaminen
  void changeResolution() {
    ++resolutionState_;
    if (resolutionState_ > 3) {
      resolutionState_ = 1;
    }
    changeResolutionState();
  }

  void changeResolutionState() {
    switch(resolutionState_) {

    case 1:
      currResolution_ = "1280x720";
      break;

    case 2:
      currResolution_ = "1600x900";
      break;

    case 3:
      currResolution_ = "1920x1080";
      break;
    }
  }

  //Asetusten tallettaminen tiedostoon.
  void writeOptionsToFile() {
    output = createWriter("data/info/options.txt");
    output.println(int(soundMute_));
    output.println(resolutionState_);
    output.println(int(musicMute_));
    output.close();
  }

  //Luetaan asteukset sisältävä tekstitiedosto ja otetaan sieltä
  //asetusten arvot talteen.
  void readOptionsFile() {
    reader = createReader("info/options.txt");
    try {
      soundMute_ = boolean(int(reader.readLine()));
      resolutionState_ = int(reader.readLine());
      musicMute_ = boolean(int(reader.readLine()));
    }
    catch (IOException e) {
      e.printStackTrace();
      soundMute_ = false;
      resolutionState_ = 1;
      musicMute_ = false;
    }
  }

  // Siirtyy edelliseen alukseen
  void decreaseSelected() {
    scaleLeft_ = 0.8;
    if (newSelected_ == true) {
      // Jos ollaan ensimmäisessä aluksessa, siirrytään loppuun
      if (selectedShip_ == 0) {
        nextSelectedShip_ = shipDatas_.size()-1;
      }
      // Muuten pienennetään valintaa
      else {
        nextSelectedShip_ = selectedShip_-1;
      }
      // Asetetaan uudesta valinnasta ja spriten kääntymisestä kertovat muuttujat epätodeksi
      newSelected_ = false;
      spriteTurned_ = false;
    }
  }

  // Siirtyy seuraavaan alukseen
  void increaseSelected() {
    scaleRight_ = 0.8;
    // Jos edellinen vaihto ollaan saatu loppuun
    if (newSelected_ == true) {
      // Jos ollaan jo viimeisessä aluksessa, siirrytään alkuun
      if (selectedShip_ == shipDatas_.size()-1) {
        nextSelectedShip_ = 0;
      }
      // Muuten kasvatetaan valintaa
      else {
        nextSelectedShip_ = selectedShip_+1;
      }
      // Asetetaan uudesta valinnasta ja spriten kääntymisestä kertovat muuttujat epätodeksi
      newSelected_ = false;
      spriteTurned_ = false;
    }
  }

  //Luetaan alukset tiedostosta.
  void readShips() {
    // Tyhjennetään vanhat datat pois ennen uusien lukemista
    while (shipDatas_.size () != 0) {
      shipDatas_.remove(0);
    }
    //Tiedoston nimi, jossa alusten tiedot ovat
    reader = createReader("info/ships.txt");
    boolean read = true;
    String line;
    String image;
    while (read) {
      int position = 0;
      //Tarkistetaan saadaanko tiedstoa luettua.
      try {

        //Yritetään lukea tiedostosta rivi muuttujaan line_
        line = reader.readLine();
      }
      catch (IOException e) {
        e.printStackTrace();
        line = null;
      }
      if (line == null) {
        read = false;
      }

      //Jos tiedostosta löytyy luettavaa tavaraa riviltä aletaan sitä tarkastelemaan
      else {

        //|-merkit toimivat ships-tiedostossa eri tietojen erottimena.
        //Etsitään ensimmäinen erotinmerkki
        position = line.indexOf("|");
        //Luetaan tiedostosta onko alus avattu
        if (line.substring(0, position).equals("unlocked")) {
          line = line.substring(position+1);
          // Luodaan uusi shipData, johon ladataan aluksen sprite ja spriten nimi
          ShipData newShipData  =new ShipData();
          position = line.indexOf("|");
          image = line.substring(0, position);
          newShipData.image_ = loadImage("sprites/" + image + ".png");
          newShipData.spriteName_ = image + ".png";
          line = line.substring(position+1);
          position = line.indexOf("|");
          newShipData.speed_ = float(line.substring(0, position));
          line = line.substring(position+1);
          position = line.indexOf("|");
          newShipData.shields_ = int(line.substring(0, position));
          line = line.substring(position+1);
          position = line.indexOf("|");
          newShipData.maxShields_ = int(line.substring(0, position));
          line = line.substring(position+1);
          newShipData.acceleration_ = float(line);
          // Lisätään shipData shipDatas vektoriin
          shipDatas_.add(newShipData);
        }
      }
    }
  }

  //Palautetaan aluksen tietoja
  String getSpriteName() {
    return shipDatas_.get(selectedShip_).spriteName_;
  }

  float getShipSpeed() {
    return shipDatas_.get(selectedShip_).speed_;
  }

  int getShipShields() {
    return shipDatas_.get(selectedShip_).shields_;
  }
  int getShipMaxShields() {
    return shipDatas_.get(selectedShip_).maxShields_;
  }
  float getShipAcceleration() {
    return shipDatas_.get(selectedShip_).acceleration_;
  }
}

