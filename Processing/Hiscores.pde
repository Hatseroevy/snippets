//Tätä luokkaa käytetään structin korvaajana, koska processingista ei löydy structia
public class playerData {
  public String name;
  public String ship;
  public int score;
}

//Hiscoresluokka, joka hoitaa hiscorejen lukemisen tiedostosta ja niiden tallettamisen sinne
class Hiscores {
  private ArrayList<playerData> hiscoresVector;
  BufferedReader reader;
  PrintWriter output;
  private String line_;
  private int position_;
  private String name_;
  private int score_;
  private String ship_;
  private Boolean read = true;
  PImage img_;
  PImage hiscoresHud_;

  //Luodaan ArrayList, johon hiscorelista talletetaan.
  Hiscores() {
    hiscoresVector = new ArrayList<playerData>();
    hiscoresHud_ = loadImage("sprites/hiscores.png");
  }

  //Luetaan hiscoret tiedostosta.
  boolean read() {
    //Tiedoston nimi, jossa hiscoret ovat
    reader = createReader("info/hiscores.txt");

    while (read) {
      //Tarkistetaan saadaanko tiedsotoa luettua.
      try {

        //Yritetään lukea tiedostosta rivi muuttujaan line_
        line_ = reader.readLine();
      }
      catch (IOException e) {
        e.printStackTrace();
        line_ = null;
      }
      if (line_ == null) {
        read = false;
      }

      //Jos tiedostosta löytyy luettavaa tavaraa riviltä aletaan sitä tarkastelemaan
      else {

        //|-merkit toimivat hiscores-tiedostossa eri tietojen erottimena.
        //Etsitään ensimmäinen erotinmerkki
        position_ = line_.indexOf("|");

        //Luetaan tiedostosta pelaajan pisteet
        score_ = int(line_.substring(0, position_));

        //Poistetaan line_-muuttujan alusta tavara ensimmäiseen erottimeen asti.
        line_ = line_.substring(position_+1);

        //Etsitään seuraava erotinmerkki
        position_ = line_.indexOf("|");

        //Talletetaan alus-spriten nimi tiedostoon ja tehdään samat temput kuin pisteille
        ship_ = line_.substring(0, position_);

        //Loppurivi on pelaajan nimeä.
        name_ = line_.substring(position_+1);

        //Luodaan uusi "structi" johon luetut tiedot talletetaan.
        playerData d = new playerData();
        d.name = name_;
        d.score = score_;
        d.ship = ship_;

        //Lisätään structi arraylistiin.
        hiscoresVector.add(d);
      }
    }

    //Kun luku on tehty palautetaan true, jolloin tiedostoa ei lueta joka ohjelman kierros uudestaan
    return true;
  }


  //add-funktio tarkastaa lisätäänkö ArrayListiin uusi alkio
  void addPlayer() {

    reader = createReader("info/playerinfo.txt");

    try {

      //Yritetään lukea tiedostosta rivi muuttujaan line_
      line_ = reader.readLine();
    }
    catch (IOException e) {
      e.printStackTrace();
      line_ = null;
      return;
    }

    position_ = line_.indexOf("|");

    //Luetaan tiedostosta pelaajan pisteet
    score_ = int(line_.substring(0, position_));

    //Poistetaan line_-muuttujan alusta tavara ensimmäiseen erottimeen asti.
    line_ = line_.substring(position_+1);

    //Etsitään seuraava erotinmerkki
    position_ = line_.indexOf("|");

    //Talletetaan alus-spriten nimi tiedostoon ja tehdään samat temput kuin pisteille
    ship_ = line_.substring(0, position_);

    //Loppurivi on pelaajan nimeä.
    name_ = line_.substring(position_+1);

    //Luodaan uusi "structi" johon luetut tiedot talletetaan.
    playerData d = new playerData();
    d.name = name_;
    d.score = score_;
    d.ship = ship_;

    //Käydään ArrayListiä läpi
    for (int i = 0; i < hiscoresVector.size(); ++i) {

      //Jos isommat pisteet löytyvät listään uusi alkio sen eteen.
      if (hiscoresVector.get(i).score < d.score) {

        hiscoresVector.add(i, d);

        //Listalla on 10 parasta tulosta, joten viimeisin poistetaan lisäyksen jälkeen.
        hiscoresVector.remove(hiscoresVector.size()-1);
        write();
        break;
      }
    }

    //Tyhjennetään pelaajan tiedot tiedostosta playerinfo.txt
    output = createWriter("data/info/playerinfo.txt");
    line_ = "Nothing here";
    output.println(line_);
    output.close();
  }


  //Kirjoitetaan hiscore ArrayListi tiedostoon
  void write() {

    //Kirjoittettavan tiedoston sijainti ja nimi
    output = createWriter("data/info/hiscores.txt");
    for (int i = 0; i < hiscoresVector.size(); ++i) {
      line_ = str(hiscoresVector.get(i).score);
      line_ += '|';
      line_ += hiscoresVector.get(i).ship;
      line_ += '|';
      line_+=  hiscoresVector.get(i).name;
      output.println(line_);
    }
    output.close();
  }

  //Näytetään hiscorelista kutsuttaessa.
  void display() {
    imageMode(CENTER);
    image(hiscoresHud_, width/2, height/2);
    text("Top Players:", width/2, height/2-261);
    textAlign(LEFT);
    for (int i = 0; i < hiscoresVector.size(); ++i) {
      img_ = loadImage("sprites/"+hiscoresVector.get(i).ship);
      text(i+1+".", width/2-178, height/2+i*50-192);
      text(hiscoresVector.get(i).name, width/2-128, height/2+i*50-192);
      text(hiscoresVector.get(i).score, width/2+145, height/2+i*50-192);
      image(img_, width/2+120, height/2+i*50-202, 30, 30);
      textSize(18);
    }
    textAlign(CENTER);
  }

  //Reset-funktio tyhjentää hiscore-listan
  void reset() {
    while (hiscoresVector.size () > 0) {
      hiscoresVector.remove(0);
    }
    playerData d = new playerData();
    d.name = "";
    d.score = 0;
    d.ship = "ship1.png";
    for (int i = 0; i < 10; ++i) {
      hiscoresVector.add(d);
    }
    write();
  }
}

