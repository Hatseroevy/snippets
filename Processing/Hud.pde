//Hud luokkaa käytetään tekstin ja muun tiedon ilmaisemiseen pelaajalle pelin aikana
//Hudi koostuu ruudun reunoilla näkyvistä erilaisita hudielementeistä
class Hud {
  private PImage scoreboard_;
  private PImage levelnumber_;
  private PImage timeleft_;
  private PImage pause_;
  private PImage energyHUD_;


  Hud() {
    scoreboard_ = loadImage("sprites/scoreboard.png");
    levelnumber_ = loadImage("sprites/levelnumber.png");
    timeleft_ = loadImage("sprites/timeleftbar.png");
    pause_ = loadImage("sprites/pausescreen.png");
    energyHUD_ = loadImage("sprites/energyhud.png");
  }

  //Displayfunktiota käytetään muiden HUD-funktioiden kutsumiseen
  void display(int score, int level, int energyBar, String weaponName, int ammo, int shields) {
    textSize(18);
    fill(#1fd2ff);
    displayScoreboard(score);
    displayLevelNumber(level);
    displayEnergyBar(energyBar);
    displayWeaponInfo(weaponName, ammo);
    displayShields(shields);
  }

  //Pelaajan pistetilanne piirretään näkyviin
  void displayScoreboard(int score) {
    textSize(18);
    fill(#1fd2ff);
    image(scoreboard_, width-114, 32);
    text("Score: "+score, width-150, 32);
  }

  //Kentän numeron piirto
  void displayLevelNumber(int level) {
    textSize(18);
    fill(#1fd2ff);
    image(levelnumber_, 108, 32);
    text("Level: "+level, 60, 32);
  }

  //Jäljellä olevan ajan piirto
  void displayTimeLeft(int timeLeft) {
    textSize(18);
    fill(#1fd2ff);
    textAlign(CENTER);
    image(timeleft_, width/2, 32);
    text("Time left: "+timeLeft, width/2, 32);
  }

  // Piirtää energybarin ruudulle
  void displayEnergyBar(int energyBar) {
    imageMode(CENTER);
    textAlign(CENTER);
    if (energyBar < 100) {
      fill(255, 0, 0);
    }
    else {
      fill(#1fd2ff);
    }
    imageMode(CENTER);
    image(energyHUD_, energyHUD_.width/2, height-energyHUD_.height/2-10);
    rect(25, height-150, 10, -energyBar/2);
    textSize(15);
    fill(#1fd2ff);
    text("Energy"+"\n"+"left", 50, height-130);
  }

  // Piirtää aseen tiedot ruudulle
  void displayWeaponInfo(String name, int ammo) {
    String ammoString;

    fill(#1fd2ff);
    textSize(15);
    textAlign(LEFT);
    if ( ammo < 0 ) {
      ammoString = "inf";
    }
    else {
      ammoString = str(ammo);
    }
    if(name.equals("Tractorbeam")) {
     name = "Tr.Beam";
    }
    text("Weapon: "+name, 20, height-93);
    text("Ammo : "+ammoString, 20, height-73);
    textAlign(CENTER);
  }

  // Piirtää ruudulle kuinka monta osumaa pelaaja voi ottaa
  void displayShields(int shields) {
    textSize(15);
    fill(#1fd2ff);
    textAlign(LEFT);
    text("Shields", 20, height-48);
    for (int i = 0; i < shields; ++i) {
      rect(20+10*i, height-43, 10, 10);
    }
    textAlign(CENTER);
  }

  //Kun peli laitetaan pauselle, piirretään tämä
  //Pauseikkuna keskelle ruutua
  void displayPause() {
    fill(255);
    textAlign(CENTER);
    image(pause_, width/2, height/2+30);
    textSize(22);
    text("Game Paused", width/2, height/2);
    textSize(16);
    text("Press escape to continue playing", width/2, height/2+30);
    text("Press q to return to the main menu", width/2, height/2+60);
    textSize(18);
  }

  //Piirtää esciä painettaessa tooltipin mitä pelaaja voi tehdä
  void displayEscToolTip() {
    textAlign(LEFT);
    textSize(15);
    fill(#1fd2ff);
    text("Press esc to return to main menu", 10, height-30);
    textAlign(CENTER);
  }
}

