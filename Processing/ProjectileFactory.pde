
// Luokka, joka hoitaa isäntäaluksen projectilejen
// luonnin. Jokaiselle alukselle on oma tehtaansa,
// koska jokainen asetyyppi tarvitsee oman laskurinsa
// millon voi luoda uuden ammuksen.

class ProjectileFactory {

  ProjectileFactory() {
  }

  // Luodut projectilet palautetaan Arraylistin kautta, sillä jotkin asetyypit
  // lähettävät matkaan kaksi tai kolme ammusta kerralla. Parametrien
  // spawnPos ja spawnRotin avulla voidaan määrittää mihin ammus luodaan ja mihin
  // suuntaan se kulkee.
  Projectile createProjectiles( String type, float spawnPosX, float spawnPosY, float spawnRot) {

    Projectile newProj = null;
    if ( type.equals("Regular") ) {
      newProj = new RegularProjectile( spawnPosX, spawnPosY, spawnRot);
    }
    else if ( type.equals("Missile") ) {
      newProj =  new Explosive( spawnPosX, spawnPosY, spawnRot);
    }
    else if ( type.equals("Laser") ) {
      newProj = new Beam( false, spawnPosX, spawnPosY, spawnRot);
    }
    else if ( type.equals("Tractorbeam") ) {
      newProj = new Beam( true, spawnPosX, spawnPosY, spawnRot);
    }
    else if ( type.equals("Mine") ) {
      newProj = new Mine( spawnPosX, spawnPosY, spawnRot);
    }
    else if ( type.equals("Railgun") ) {
      newProj = new Railgun( spawnPosX, spawnPosY, spawnRot);
    }

    return newProj;
  }
}

