// Luokka joka toteuttaa creditit ja boostit 
class PickUp {

  private int value_;
  private int type_;
  private PImage img_;
  private float posX_;
  private float posY_;
  private float deltaX_;
  private float deltaY_;
  private boolean collected_;
  private boolean destroy_;
  private color c_;
  private float alpha_;
  private float textSize_;
  private float maxSpeed_;
  private float tractorSpeed_;
  private String text_;

  PickUp(float posX, float posY, int value, int type) {
    collected_ = false;
    destroy_ = false;
    c_ = color(255, 255, 255);
    alpha_ = 255;
    textSize_ = 11;
    maxSpeed_ = 0.3;
    tractorSpeed_ = 2.0;
    value_ = value;
    type_ = type;
    posX_ = posX;
    posY_ = posY;

    // Annetaan pickupille satunnainen nopeus
    deltaX_ = random(-maxSpeed_, maxSpeed_);
    deltaY_ = random(-maxSpeed_, maxSpeed_);

    // Valitaan tyypin perusteella kuva ja tulostettava teksti
    // 0 vastaa credittiä, tästä eteenpäin tyypit ovat boosteja
    switch(type_) {
    case 0:
      img_ = loadImage("sprites/credit.png");
      text_ = "+ " + value_ + " credits";
      break;
    case 1:
      img_ = loadImage("sprites/boost.png");
      text_ = "double"+"\n"+"shot";
      break;
    case 2:
      img_ = loadImage("sprites/boost.png");
      text_ = "triple"+"\n"+"shot";
      break;
    case 3:
      img_ = loadImage("sprites/boost.png");
      text_ = "immortality";
      break;
    }
  }


  // Palauttaa creditin arvon
  int getValue() {
    return value_;
  }

  int getType() {
    return type_;
  }
  // Palauttaa creditin x-koordinaatin
  float getPosX() {
    return posX_;
  }

  // Palauttaa creditin y-koordinaatin
  float getPosY() {
    return posY_;
  }

  // Palauttaa pickupin (kuvan) koon. Koska kuva on neliö, riittää leveyden palauttaminen
  float getSize() {
    return img_.width;
  }

  // Piirtää pickuppia ruudulle
  void display() {
    // Jos pickuppia ei ole kerätty piirretään sen kuva    
    if (collected_ == false) {
      image(img_, posX_, posY_);
      // Muutetaan sijaintia deltan verran
      posX_ += deltaX_;
      posY_ += deltaY_;

      //Jos pickup liikkuu ulos pelialueelta ilmestyy
      //se pelialueen toiselle reunalle
      if (posY_ >= height+img_.width/2) {
        posY_ = 1-img_.width/2;
      }
      if (posX_ >= width+img_.width/2) {
        posX_= 1-img_.width/2;
      }
      if (posY_ <= 0-img_.width/2) {
        posY_ = (height-1)+img_.width/2;
      }
      if (posX_ <= 0-img_.width/2) {
        posX_= (width-1)+img_.width/2;
      }
    }
    // Jos pickup on kerätty, piirretään teksti joka kertoo sen arvon pelaajalle
    else {
      if (alpha_ > 0) {
        fill(c_, alpha_);
        textSize(textSize_);
        text(text_, posX_, posY_);
        alpha_ -= 4;
        textSize_ += 0.2;
      }
      // Lopuksi tuhotaan pickup lopullisesti
      else {
        destroy_ = true;
      }
    }
  }

  // Palauttaa onko pickup tuhottu
  boolean getDestroy() {
    return destroy_;
  }

  // Tarkistetaan törmääkö pickup annetun objektin kanssa
  boolean collision(float posX, float posY, float size) {
    // Lasketaan ero x -ja y-koordinaateissa
    int xDist = abs(int(posX_-posX));
    int yDist = abs(int(posY_-posY));
    // Lasketaan esineiden etäisyys toisistaan
    float dist = sqrt(pow(xDist, 2) + pow(yDist, 2));
    // Jos esineet ovat tarpeeksi lähellä toisiaan ne törmäävät, pickup kerätään ja palautetaan true
    if ( dist < (img_.width/2+size/2)-5 && collected_ == false) {
      collected_ = true;
      return true;
    }
    return false;
  }

  // Funktio, joka siirtää pickuppia kohti annettua pistettä. Käytetään tractor beamin kanssa
  void moveTowards( float posX, float posY ) {
    if ( posX > posX_ ) { 
      if ( deltaX_ < tractorSpeed_ ) {
        deltaX_+=0.05;
      }
    }
    if ( posX < posX_ ) { 
      if ( deltaX_ > -tractorSpeed_ ) {
        deltaX_-=0.05;
      }
    }
    if ( posY > posY_ ) { 
      if ( deltaY_ < tractorSpeed_ ) {
        deltaY_+=0.05;
      }
    }
    if ( posY < posY_ ) { 
      if ( deltaY_ > -tractorSpeed_ ) {
        deltaY_-=0.05;
      }
    }
  }
}

