// Luokka, joka huolehtii asteroidien luomisesta kentän alussa
// tai asteroidien hajotessa

class Factory {

  Factory() {
  }

  // Funktio, joka lisää kentälle asteroidejä kentän numeron perusteella
  void initialize(int level, boolean potato) {
    // Pisteet jonka mukaan kentälle asetetaan esineitä
    int spawnPoints = ceil(10*log(1+level));

    float x = 0;
    float y = 0;
    float size = 0;
    float spawn = 0;
    // Niin kauan kuin pisteitä riittää
    while (spawnPoints > 0) {
      // Avaruusaluksia voi ilmestyä vasta tietyn kentän jälkeen
      if (level > 2) {
        // Arvotaan mitä kentälle laitetaan
        spawn = int(random(1, 14));
        boolean collision = true;

        // Kentälle laitetaan asteroidi tietyillä arvotuilla numeroilla ja jos pisteitä on jäljellä alle 4
        if ((spawn != 1 && spawn != 2) || spawnPoints < 4) {
          // Arvotaan asteroidille koko
          size = random(35, 95);
          // Arvotaan asteroidille x ja y paikka, joka ei saa olla liian lähellä pelaajaa
          // tai törmätä toiseen asteroidiin
          while (collision == true) {
            collision = false;
            x = random(1, width);
            y = random(1, height);
            // Tarkistetaan onko koordinaatti sallitulla alueella pelaajan suhteen
            if ((x > (width/2)-250 && x < (width/2)+250) &&
              (y > (height/2)-180 && y < (height/2)+180 )) {
              collision = true;
            }
            else {
              // Tarkistetaan asteroidien keskinäiset törmäykset
              for (int j = 0; j < asteroids.size(); ++j) {
                xDist = abs(int(x-asteroids.get(j).getX()));
                yDist = abs(int(y-asteroids.get(j).getY()));
                if (xDist != 0 && yDist != 0) {
                  dist = pow(xDist, 2) + pow(yDist, 2);
                  if (sqrt(dist) < (size/2+asteroids.get(j).getSize()/2)+100) {
                    collision = true;
                  }
                }
              }
            }
          }  
          // Lisätään asteroidi vektoriin. Arvotun numeron perusteella
          // valitaan asteroidin tyyppi.

          // ********************************* //
          // Tyyppi 1 = tavallinen asteroidi 1 //
          // Tyyppi 2 = tavallinen asteroidi 2 //
          // Tyyppi 3 = jääasteroidi           //
          // Tyyppi 4 = timanttiasteroidi      //
          // ********************************* //
          
          // Jää asteroidin spawnaus
          if (spawn == 3 || spawn == 4) {
            asteroids.add(new IceAsteroid(x, y, size, potato, 3));
            spawnPoints = spawnPoints-3;
          }
          // Timanttiasteroideja voi spawnata vain kentän 3 jälkeen
          else if (spawn == 5 && level > 3) {
            asteroids.add(new DiamondAsteroid(x, y, size, potato, 4));
            spawnPoints = spawnPoints-2;
          }
          // Tavallisen asteroidin spawnaus
          else if (spawn == 6 ||  spawn == 7 ||spawn == 8) {
            asteroids.add(new Asteroid(x, y, size, potato, 2));
            spawnPoints--;
          }
          // Tavallinen asteroidi eri kuvalla
          else {
            asteroids.add(new Asteroid(x, y, size, potato, 1));
            spawnPoints--;
          }
        }
        else {
          // Arvotaan paikkaa kunnes ollaan sallitulla paikalla
          while (collision == true) {
            collision = false;
            x = random(1, width);
            y = random(1, height);
            // Tarkistetaan onko koordinaatti sallitulla alueella pelaajan suhteen
            if ((x > (width/2)-250 && x < (width/2)+250) &&
              (y > (height/2)-180 && y < (height/2)+180 )) {
              collision = true;
            }
            else {
              // Tarkistetaan avaruusalusten keskinäiset törmäykset
              for (int j = 0; j < enemyShips.size(); ++j) {
                xDist = abs(int(x-enemyShips.get(j).getX()));
                yDist = abs(int(y-enemyShips.get(j).getY()));
                if (xDist != 0 && yDist != 0) {
                  dist = pow(xDist, 2) + pow(yDist, 2);
                  if (sqrt(dist) < 100) {
                    collision = true;
                  }
                }
              }
            }
          }  
          // Lisätään avaruusalus vektoriin
          int shipSpawn = int(random(0, 2));
          // Miina-aluksia voi spawnata kentän 3 jälkeen
          if (shipSpawn == 0 && level > 3) {
            enemyShips.add(new MineShip(x, y, 75));
          }
          else {
            enemyShips.add(new EnemyShip(x, y, 75));
          }
          spawnPoints -= 4;
        }
      }
      else {
        boolean collision = true;
        // Arvotaan asteroidille koko
        size = random(35, 95);
        // Arvotaan asteroidille x ja y paikka, joka ei saa olla liian lähellä pelaajaa
        // tai törmätä toiseen asteroidiin
        while (collision == true) {
          collision = false;
          x = random(1, width);
          y = random(1, height);
          // Tarkistetaan onko koordinaatti sallitulla alueella pelaajan suhteen
          if ((x > (width/2)-250 && x < (width/2)+250) &&
            (y > (height/2)-180 && y < (height/2)+180 )) {
            collision = true;
          }
          else {
            // Tarkistetaan asteroidien keskinäiset törmäykset
            for (int j = 0; j < asteroids.size(); ++j) {
              xDist = abs(int(x-asteroids.get(j).getX()));
              yDist = abs(int(y-asteroids.get(j).getY()));
              if (xDist != 0 && yDist != 0) {
                dist = pow(xDist, 2) + pow(yDist, 2);
                if (sqrt(dist) < (size/2+asteroids.get(j).getSize()/2)+100) {
                  collision = true;
                }
              }
            }
          }
        }  
        // Lisätään asteroidi vektoriin
        asteroids.add(new Asteroid(x, y, size, potato, int(random(1, 2.99))));
        spawnPoints--;
      }
    }
  }

  void destroy(Asteroid asteroid, boolean potato) {

    // Jos kyseessä on jääasteroidi se hajoaa 10 pieneksi palaseksi
    if (asteroid.getType() == 3) {
      for (int i = 0; i < 10; i++) {
        float deltaX = 0;
        float deltaY = 0;
        // DeltaX pidetään minimissä ja deltaY:tä muutetaan tasaisesti
        if (i < 5) {
          deltaX = -2.0;
          deltaY = -2.0 + i*0.4;
        }
        else {
          deltaX = 2.0;
          deltaY = 2.0 - (i-5)*0.4;
        }
        // Luodaan uusi jääpala
        IceShard newShard = new IceShard(asteroid.getX(), asteroid.getY(), potato, deltaX, deltaY);
        iceShards.add(newShard);
      }
    }
    // Timantin hajotessa se tiputtaa normaalia enemmän credittejä
    else if ( asteroid.getType() == 4) {
      // Arvotaan credittien määrä
      int amount = int(random(3, 8));
      for (int i = 0; i < amount; i++) {
        // Arvotaan creditille arvo ja lisätään se pickupit taulukkoon
        int value = 10*int(random(1, 6));
        pickUps.add(new PickUp(asteroid.getX(), asteroid.getY(), value, 0));
      }
    }
    else {
      // Asteroidi hajoaa kahteen osaan. Arvotaan toisen koko väliltä vanhan koko / 2 ja vanhan koko -10
      float newSizeA = random(asteroid.getSize()/2, asteroid.getSize()-10);
      float newSizeB;

      // Haetaan hajonneen asteroidin tyyppi
      int type = asteroid.getType();
      // Jos ensimmäisen osan koko on alle 25, toisen koko arvotaan väliltä vanhan koko/2 ja vanhan koko
      if (newSizeA < 25) {
        newSizeB = random(asteroid.getSize()/2, asteroid.getSize());
      }
      // Muuten toisen osan koko arvotaan väliltä 25 ja vanhan koko / 2
      else {
        newSizeB = random(25, asteroid.getSize()/2);
      }

      // Jos asteroidin koko on  yli 25
      if (newSizeA > 25) {
        // Arvotaan uudet x ja y -koordinaattien muutosnopeudet vanhan perusteella
        float newDeltaXA = random(asteroid.getDeltaX()+0.1, asteroid.getDeltaX()+0.5);
        float newDeltaYA = random(asteroid.getDeltaY()+0.1, asteroid.getDeltaY()+0.5);
        // Luodaan uusi asteroidi
        Asteroid newAsteroid = new Asteroid(asteroid.getX()+(5+newSizeA/2), asteroid.getY()+(5+newSizeA/2), newSizeA, newDeltaXA, newDeltaYA, potato, type);
        // Asetetaan uuden asteroidin luomisaika, jotta se voidaan pitää hetken tuhoutumattomana
        newAsteroid.setIndestructible(millis());
        // Lisätään väliaikaiseen säilöön, josta asteroidi siirretään varsinaisten asteroidien mukaan, kun se voi tuhoutua
        tempAsteroids.add(newAsteroid);
      }
      if (newSizeB > 25) {
        float newDeltaXB = random(asteroid.getDeltaX()+0.1, asteroid.getDeltaX()+0.5);
        float newDeltaYB = random(asteroid.getDeltaY()+0.1, asteroid.getDeltaY()+0.5);
        Asteroid newAsteroid = new Asteroid(asteroid.getX()-0*(5+newSizeB/2), asteroid.getY()-0*(5+newSizeB/2), newSizeB, newDeltaXB, newDeltaYB, potato, type);
        newAsteroid.setIndestructible(millis());
        tempAsteroids.add(newAsteroid);
      }
    }
  }
  // Arpoo ilmestyykö creditti kun vihollisalus tuhotaan
  void destroy() {
    // Arvotaan numero jonka perusteella spawnataan pickuppi
    int spawn = int(random(1, 8));
    // Jos spawn on 2 tai 4 spawnataan creditti
    if (spawn == 2 || spawn == 4) {
      // Arvotaan sopiva arvo creditille
      int value = 10*int(random(1, 6));
      pickUps.add(new PickUp(asteroid.getX(), asteroid.getY(), value, 0));
    }
    // Jos spawn on 5 on kyseessä boosti
    if (spawn == 5) {
      // Arvotaan boostin tyyppi
      int type = int(random(1, 4));
      pickUps.add(new PickUp(asteroid.getX(), asteroid.getY(), 0, type));
    }
  }
}

