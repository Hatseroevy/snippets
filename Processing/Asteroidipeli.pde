public class rajahdus {
  public float x;
  public float y;
  public float size;
  public float maxSize;
}

import ddf.minim.*;

// Muuttujat joihin äänet tallennetaan
AudioPlayer explosion;
AudioPlayer plasma;
AudioPlayer menu;
AudioPlayer select;
AudioPlayer gameMusic;
AudioPlayer menuMusic;
Minim player;
PFont font;

SpaceShip ship;
// Muuttujat joihin tallennetaan väliaikaiset projectilet,
// asteroidit, alukset jne
Projectile projectile;
Asteroid asteroid;
Asteroid tempAsteroid;
EnemyShip enemyShip;
IceShard iceShard;
Menu mainMenu;
// Tehdas joka tuottaa asteroideja ja vihollisia
Factory factory;

Hud hud;
Shop shop;
PickUp pickUp;

ArrayList<Asteroid> asteroids;
ArrayList<Asteroid> tempAsteroids;
ArrayList<Animation> animations;
ArrayList<shoppingCart> weaponCart;
ArrayList<EnemyShip> enemyShips;
ArrayList<PickUp> pickUps;
ArrayList<IceShard> iceShards;

int gameState;

// Eri aikoja tallettavat muuttujat
int timeLeft;
int maxTime;
int pauseStartTime;
int pauseStopTime;
int pauseLength;
int waitTime;
int time;
int completeTime;

boolean paused;
boolean spacePressed;
String typing;
String playerName;
PImage backgroundpic;
PImage backgroundpic2;
int xDist;
int yDist;
float dist;
int level;
ArrayList<rajahdus> rajahdusTesti;
boolean levelCompleted;
boolean soundIsMute;
boolean musicIsMute;
int frameCounter;

int completeBuffer;
boolean getCompleteBuffer;
boolean displayCompletedText;
float levelCompletedTextSize;
int levelCompletedAlpha;

int selectedShip_;
private int shipIndestructible = 1000;

void setup() {
  // Alustetaan tarvittavat taulukot ja muuttujat
  rajahdusTesti = new ArrayList<rajahdus>();
  tempAsteroids = new ArrayList<Asteroid>();
  animations = new ArrayList<Animation>();
  weaponCart = new ArrayList<shoppingCart>();
  enemyShips = new ArrayList<EnemyShip>();
  pickUps = new ArrayList<PickUp>();
  asteroids = new ArrayList<Asteroid>();
  iceShards = new ArrayList<IceShard>();
  frameRate(60);
  paused = false;
  spacePressed = false;
  pauseLength = 0;
  time = 0;
  noCursor();
  textAlign(CENTER);
  // Ladataan pelin fontti
  font = loadFont("fonts/Airstrike-48.vlw");
  textFont(font, 18);
  mainMenu = new Menu();
  factory = new Factory();
  shop = new Shop();
  maxTime = 100;
  mainMenu.readHiscores();
  backgroundpic2 = loadImage("sprites/background3.png");
  changeResolution(mainMenu.getResolution());
  gameState = 0;
  typing = "";
  level = 1;
  player = new Minim(this);
  // Ladataan äänitiedostot muuttujiin
  plasma = player.loadFile("audio/plasma3.wav", 256);
  explosion = player.loadFile("audio/explosion1.wav", 512);
  menu = player.loadFile("audio/menu2.wav", 256);
  select = player.loadFile("audio/select1.wav", 256);
  gameMusic = player.loadFile("audio/Rolemusic_-_If_Pigs_Could_Sing.mp3", 256);
  menuMusic = player.loadFile("audio/BoxCat_Games_-_12_-_Passing_Time.mp3", 256);
  factory.initialize(2, false);
  hud = new Hud();
  levelCompleted = true;
  soundIsMute = mainMenu.getSoundMute();
  musicIsMute = mainMenu.getMusicMute();
  frameCounter = 0;

  getCompleteBuffer = true;
  completeBuffer = 0;
  displayCompletedText = false;
  levelCompletedTextSize = 600;
  levelCompletedAlpha = 0;

  selectedShip_ = 0;
}

void draw() {
  background(0);
  switch(gameState) {

    //Gamestate0 on käytössä silloin, kun ollaan mainmenussa
  case 0:
    try {
      // Jos menu musiikki ei soi ja ei olla mutella soitetaan musiikki
      if (menuMusic.isPlaying() == false && player != null && !musicIsMute) {
        menuMusic.rewind();
        menuMusic.play();
      }
      background(backgroundpic2);
    }
    catch (Exception e) {
      println("Poikkeus havaittu");
      background(0);
    }

    for (int i = 0; i < asteroids.size(); ++i) {
      asteroid = asteroids.get(i);
      asteroid.display();
    }
    //Piirretään mainmenu näkyville
    mainMenu.display();
    if (mainMenu.getMenuState() == 1 || mainMenu.getMenuState() == 3 || mainMenu.getMenuState() == 4) {
      //Näytetään tooltip tarvittaessa.
      hud.displayEscToolTip();
      textSize(18);
    }
    text(typing, width/2, height/2-80); 

    //Alustetaan peli pelitilaan
    if (mainMenu.getGameStart()) {
      gameState=1;
      setUpGame();
    }
    break;

    //Gamestate1 kuvaa sitä, että peli on pyörimässä.
  case 1:
    menuMusic.pause();
    //Jokaisen kentän alussa näytetään laatikko, jossa on kentän numero
    if (levelCompleted == true) {
      background(0);
      mainMenu.displayLevelNumber(level);
      if (millis() - completeTime > 2000) {
        levelCompleted = false;
      }
    }

    //Kun laatikko on näytetty peli pyörähtää käyntiin normaalisti.
    else {
      // Jos peli musiikki ei soi ja ei olla mutella, voidaan soittaa musiikki
      if (gameMusic.isPlaying() == false && !musicIsMute) {
        gameMusic.rewind(); 
        gameMusic.play();
      }
      background(backgroundpic);
      textSize(18);
      // Piirretään esineet
      displayPickups();
      ship.display();
      displayProjectiles();
      displayAsteroids();
      displayEnemyShips();
      displayIceShards();

      frameCounter += 1;
      // Frame countteria tarvitaan mm. huolehtimaan että törmäyksiä ei tarkisteta joka freimillä
      if (frameCounter > 60) {
        frameCounter = 0;
      }
      // Käydään läpi väliaikaiset asteroidit
      for (int i = tempAsteroids.size()-1; i >= 0; i--) {
        tempAsteroid = tempAsteroids.get(i);
        // Piirretään asteroidi ruudulle
        tempAsteroid.display();
        // Haetaan pelikello
        float current = millis();
        // Jos asteroidin luomisen ja nykyhetken välillä on yli 1500 millisekuntia poistetaan
        // asteroidi väliaikaisista ja lisätään tavallisten asteroidien joukkoon
        if ((current - tempAsteroid.getIndestructible()) > 200) {
          asteroids.add(tempAsteroid);
          tempAsteroids.remove(i);
        }
      }
      // Piirretään kaikki animaatiot
      for (int i = animations.size()-1; i >= 0; i--) {
        if (animations.get(i).display() == false) {
          animations.remove(i);
        }
      }

      //Näytetään peliaika ja kutsutaan hud-luokkaa, joka piirtää pelaajan HUDin
      displayTimeLeft();
      hud.display(ship.getPoints(), level, ship.getCurrentEnergy(), ship.getSelectedWeapon(), ship.getAmountOfSelectedWeapon(), ship.getShields());

      //Kun kaikki asteroidit on tuhottu näytetään teksti joka kertoo, että kenttä on pelattu läpi
      if (displayCompletedText) {
        textSize(levelCompletedTextSize);
        fill(255, 255, 255, levelCompletedAlpha);
        textAlign(CENTER);
        text("Level Completed!", width/2, height/2);
        if (levelCompletedTextSize > 80) {
          levelCompletedTextSize -= 20;
          levelCompletedAlpha +=9 ;
        }
        gameMusic.pause();
      }
    }

    break;

    //Piirretään kauppa näkyville
  case 2:
    background(backgroundpic2);
    gameMusic.pause();
    shop.display();
    break;
  }
}

//Funktio joka ottaa pelaajan näppäinten painallukset 
void keyPressed() {

  if (keyCode == ESC || key == ESC) {
    key = '§';
  } 
  switch(gameState) {
  case 0:

    //Päävalikon tila vaikuttaa siihen, mitä eri napinpainallukset tekevät.
    //Tila 0 on käytössä silloin, kun valikossa navigoidaan.
    if (mainMenu.getMenuState() == 0) {
      if (keyCode == DOWN) {
        if (!soundIsMute) {
          menu.play();
          menu.rewind();
        }
        mainMenu.selectedUp();
      }
      if (keyCode == UP) {
        if (!soundIsMute) {
          menu.play();
          menu.rewind();
        }
        mainMenu.selectedDown();
      }
      mainMenu.menuMover();
    }

    if (keyCode == ENTER && mainMenu.getMenuState() != 2 && mainMenu.getMenuState() != 4) {
      if (!soundIsMute && player != null) {
        select.play();
        select.rewind();
      }
      mainMenu.menuSelector();
    }

    if (mainMenu.getMenuState() == 4 && key == '§') {
      mainMenu.setMenuState(0);
    }

    //Menuvalikon tila 1 on käytössä silloin, kun pelaaja syöttää
    //ohjelmalle oman nimimerkkinsä pelin alkaessa.
    if (mainMenu.getMenuState() == 1) {
      writeName();
      // Jos tässä menussa painetaan vasemmalle tai oikealle, vaihdetaan valittua laivaa
      if (keyCode == LEFT) {
        mainMenu.decreaseSelected();
      }
      if (keyCode == RIGHT) {
        mainMenu.increaseSelected();
      }
    }

    // Siirrytään optionsseihin
    if (mainMenu.getMenuState() == 3) {
      if (key == '§') {
        mainMenu.setOptionsState(0);
        mainMenu.setMenuState(0);
        mainMenu.writeOptionsToFile();
        break;
      }
      if (keyCode == ENTER && mainMenu.getOptionsState() == 1 ) {
        mainMenu.changeSoundMute();
        soundIsMute = mainMenu.getSoundMute();
      }

      if (keyCode == ENTER && mainMenu.getOptionsState() == 2 ) {
        mainMenu.changeMusicMute();
        musicIsMute = mainMenu.getMusicMute();

        if (musicIsMute) {
          menuMusic.pause();
        }
      }

      if (keyCode == ENTER && mainMenu.getOptionsState() == 3 ) {
        mainMenu.changeResolution();
        changeResolution(mainMenu.getResolution());
      }

      if (keyCode == ENTER && mainMenu.getOptionsState() == 4) {
        mainMenu.resetHiscores();
      }

      // Jos äänet ovat päällä soitetaan liikkuessa ääniä
      if (keyCode == UP) {
        if (!soundIsMute) {
          menu.play();
          menu.rewind();
        }
        mainMenu.optionsDown();
      }

      if (keyCode == DOWN) {
        if (!soundIsMute) {
          menu.play();
          menu.rewind();
        }
        mainMenu.optionsUp();
      }

      mainMenu.menuMover();

      if (mainMenu.getOptionsState() == 0) {
        mainMenu.setOptionsState(1);
      }
    }

    break;


    //Pelin näppäimistö siinä kunnossa, jossa se on pelin pyöriessä.
  case 1:
    if (keyCode == LEFT) {
      ship.setPressed(1);
    }  

    if (keyCode == RIGHT) {
      ship.setPressed(2);
    }
    if (keyCode == UP) {
      ship.setPressed(0);
    }
    if (keyCode == DOWN) {
      ship.setPressed(3);
    }
    if (keyCode == ' ') {
      if (spacePressed == false) {
        // Toistetaan ampumisääni
        if (!soundIsMute) {
          plasma.play();
          // Kelataan ampumisääni alkuun
          plasma.rewind();
        }
        ship.setPressed(4);
        ship.shoot();
        spacePressed = true;
      }
    }

    if (key == '§') {
      pause();
    }

    // Jos escin jälkeen painetaan Q poistutaan pelistä
    if ((key == 'q' || key == 'Q') && paused) {
      gameMusic.pause();
      quitToMenu();
    }

    break;

  case 2:

    // Kaupassa liikutaan nuolilla
    if (keyCode == UP) {
      shop.selectedItemUp();
    }

    if (keyCode == DOWN) {
      shop.selectedItemDown();
    }

    if (keyCode == RIGHT && shop.getSelectedItem() == -1) {
      shop.tabRight();
    }
    if (keyCode == LEFT && shop.getSelectedItem() == -1) {
      shop.tabLeft();
    }

    //Jos pelaajalla on valittuna "Continue to next level" ja painetaan entteriä, käynnistetään seuraava kenttä
    if (keyCode == ENTER && shop.getContinue()) {

      //Kysytään kaupalta pelaajan ostoskori
      ship.getCart(shop.getBoughtWeapons());
      //powerUpCart = shop.getBoughtPowerUps();
      ship.setShields(shop.getShields());
      ship.setMaxEnergy(shop.getEnergy());


      //Päivitetänä pelaajan rahatilanne alukselle
      ship.setMoney(shop.getMoney());

      //Kaupan palautus alkutilaan.
      shop.reset();
      gameState = 1;
      completeTime = millis();
      setUpGame();
    }
    else if (keyCode == ENTER && shop.getSelectedItem() != -1) {
      shop.buy();
    }
    break;
  }
}
void keyReleased() {
  // Asettaa pelaajan alukselle näppäimen painetuksi
  // jolloin alkuksen tila muuttuu painetun näppäimen mukaan
  switch(gameState) {
  case 0:

    break;

  case 1:
    if (keyCode == LEFT) {
      ship.setReleased(1);
    }  

    if (keyCode == RIGHT) {
      ship.setReleased(2);
    }
    if (keyCode == UP) {
      ship.setReleased(0);
    }

    if (keyCode == DOWN) {
      ship.setReleased(3);
    }

    if (keyCode == ' ') {
      spacePressed = false;
      ship.setReleased(4);
    }

    // Valitse edellinen asetyyppi
    if (key == 'q' && !paused) {
      ship.changeProjType( false );
    }
    // Valitse seuraava asetyyppi
    if (key == 'w') {
      ship.changeProjType( true );
    }
    break;
  }
}

// Pysäyttää pelin
void pause() {
  if (!paused) {
    paused = true;
    pauseStartTime = millis();
    noLoop();
    hud.displayPause();
  }
  else {
    paused = false;
    pauseStopTime = millis();
    pauseLength = pauseLength+(pauseStopTime-pauseStartTime);
    loop();
  }
}

//Otetaan pelaajan nimimerkki talteen
void writeName() {
  //Backspace kumittaa
  if (keyCode == BACKSPACE && typing.length() > 1) {
    typing = typing.substring(0, typing.length() - 1);
  }

  else if (key == '§') {
    mainMenu.setMenuState(0);
    typing = "";
  }

  //Käyttäjän syöttämä teksti tallennetaan tilapäisesti
  //muuttujaan typing.
  else
    if (key != CODED && typing.length() < 16) typing += key;
}

void quitToMenu() {
  // Kun palataan main menuun täytyy pelin tila nollata
  mainMenu.resetGameStart();
  shop.resetOwned();
  paused = false;
  pauseStopTime = millis();
  pauseLength = pauseLength+(pauseStopTime-pauseStartTime);
  gameState = 0;
  level = 1;
  mainMenu.setMenuState(4);
  ship.writeToFile();
  mainMenu.hiscoresAddCaller();
  removeAsteroids();
  removeEnemyShips();
  factory.initialize(2, false);
  loop();
}

//Alustetaan peli tilaan, joka sillä on silloin kun ollaan main menussa
void setUpGame() {
  time = millis();
  pauseLength = 0;
  levelCompleted = true;
  timeLeft = maxTime;
  backgroundpic = loadImage("sprites/background2.png");
  backgroundpic.resize(width, height);
  removeAsteroids();
  removePickups();
  removeEnemyShips();
  completeBuffer = 0;
  getCompleteBuffer = true;
  displayCompletedText = false;
  levelCompletedTextSize = 600;
  levelCompletedAlpha = 0;
  mainMenu.readShips();
  //Poistetaan mahdolliset kentällä olevat ammukset
  while (ship != null && ship.projectiles () != 0) {
    ship.removeProjectile(0);
  }

  //Substring tarvitaan, koska pelaajan nimen edessä on jokin
  //mystinen rivinvaihto aluksi.
  if (level == 1) {
    playerName = playerName.substring(1, playerName.length());
    ship = new SpaceShip(playerName, mainMenu.getSpriteName(), mainMenu.getShipSpeed(), mainMenu.getShipShields(), mainMenu.getShipMaxShields(), mainMenu.getShipAcceleration());
    completeTime = millis();
  }
  factory.initialize(level, ship.getPotato());
  ship.resetPos();
}

//Ammusten piirto ruudulle
void displayProjectiles() {
  for (int i = 0; i < ship.projectiles(); ++i) {
    projectile = ship.returnProjectile(i);
    projectile.display();
    if (projectile.destroy()) {
      ship.removeProjectile(i);
    }
  }
}

//Asteroidien piirto ruudulle
void displayAsteroids() {

  // Muuttuja, joka kertoo onko kierroksella tuhottu asteroidi
  boolean destroyed = false;
  // Jos kierroksella on tuhottu asteroidi, aloitetaan läpikäyminen alusta
  for (int i = 0; i < asteroids.size(); i++) {
    if (destroyed == true) {
      i = 0;
    }

    asteroid = asteroids.get(i);
    asteroid.display();
    destroyed = false;
    float current = millis();
    // Asteroidin ja aluksen väliset törmäykset tarkistetaan vain joka 5:llä freimillä
    if (frameCounter % 5 == 0 && current - ship.getIndestructible() > shipIndestructible && !ship.isImmortal() ) {
      //Tarkistus osuuko asteroidi alukseen
      xDist = abs(int(ship.getX()-asteroid.getX()));
      yDist = abs(int(ship.getY()-asteroid.getY()));
      dist = pow(xDist, 2) + pow(yDist, 2);

      //Osuma asteroidin ja aluksen välillä
      if (sqrt(dist) < ship.getSize()/2.6+asteroid.getSize()/2.6) {
        if (ship.hit()) {
          // Toistetaan aluksen räjähdysääni
          if (!soundIsMute) {
            explosion.play();
            // Kelataan ääni alkuun
            explosion.rewind();
          }
          gameMusic.pause();
          // Nollataan pelin tila
          int shipDestroyTime = millis();
          while (millis ()-shipDestroyTime < 1000) {
          }
          mainMenu.resetGameStart();
          shop.resetOwned();
          ship.writeToFile();
          mainMenu.hiscoresAddCaller();
          level = 1;
          gameState = 0;
          mainMenu.setMenuState(4);
          removeAsteroids();
          removeEnemyShips();
          factory.initialize(2, false);
        }
        else {
          ship.setIndestructible(millis());
        }
      }
    }


    // Asteroidien ja ammusten väliset törmäykset tarkistetaan joka toisella freimillä
    if (frameCounter % 5 == 1 || frameCounter % 5 == 3) {
      //Tarkistetaan osuuko ammus asteroidiin.
      for (int j = 0; j < ship.projectiles(); ++j) {
        projectile = ship.returnProjectile(j);
        // Koska eri projectiletyypeille osuma tarkastellaan eri tavalla (säteet vs. papanat),
        // tarkastelu on projectilella itsellään.
        if ( projectile.hitsObject(asteroid.getX(), asteroid.getY(), asteroid.getSize()/2 ) ) {

          // Tractorbeamilla ei tehdä vahinkoa, vaan liikutetaan asteroideja.
          if ( projectile.getType() == "tractorbeam" ) {
            // Vedä asteroideja.
            asteroid.moveTowards( ship.getX(), ship.getY() );
          }
          // Laaseri sulattaa jää asteroideja
          else if ( projectile.getType() == "laserbeam" && asteroid.getType() == 3) {
            asteroid.melt();
            if (asteroid.getSize() < 15) {
              asteroids.remove(i);
              destroyed = true;
              ship.addScore();
              break;
            }
          }
          // Muut projectilet tekevät vahinkoa.
          else {
            // Osuman tapahtuessa aiheutetaan vahinkoa asteroidille ja
            // tuhotaan se, jos vahinkoa on tarpeeksi.       
            asteroid.causeDamage( projectile.getPower() );
            if ( asteroid.getLife() <= 0 ) {
              factory.destroy(asteroid, ship.getPotato());

              // Luodaan uusi räjähdys animaatio
              Animation anim = new Animation("asterExplo", 18, asteroid.getX(), asteroid.getY(), 2*asteroid.getSize());
              if (animations.size() < 2) {
                animations.add(anim);
              }
              // Arvotaan numero jonka perusteella spawnataan pickuppi
              int spawn = int(random(1, 8));
              // Jos spawn on 2 tai 4 spawnataan creditti
              if (spawn == 2 || spawn == 4) {
                // Arvotaan sopiva arvo creditille
                int value = 10*int(random(1, 6));
                pickUps.add(new PickUp(asteroid.getX(), asteroid.getY(), value, 0));
              }
              // Jos spawn on 5 on kyseessä boosti
              if (spawn == 5) {
                // Arvotaan boostin tyyppi
                int type = int(random(1, 4));
                pickUps.add(new PickUp(asteroid.getX(), asteroid.getY(), 0, type));
              }
              asteroids.remove(i);

              // Asetetaan tuhoutumisesta kertova muuttuja todeksi ja siirrytään ulos silmukasta
              destroyed = true;
              ship.addScore();
              break;
            }

            // Ammustyypistä riippuen, osuessaan kohteeseen ammus joko
            // tuhoutuu tai se räjähtää.
            if ( projectile.getDestroyed() ) {
              if ( projectile.getType() != "railgun" ) {
                ship.removeProjectile( j );
              }
            }
          }
        }
      }
    }
    // Jos kierroksella ei ole vielä tuhottu asteroidia
    if (destroyed == false) {
      // Asteroidien väliset törmäykset tarkistetaan vain joka 5:llä freimillä
      if (frameCounter % 5 == 4) {
        //Käydään läpi muiden asteroidien sijainti pelikentällä
        for (int j = 0; j < asteroids.size(); ++j) {
          if (destroyed == true) {
            j = 0;
          }
          // Jos verrataan itseensä voidaan jatkaa
          if (i == j) {
            continue;
          }
          // Lasketaan kahden asteroidin etäisyys
          xDist = abs(int(asteroid.getX()-asteroids.get(j).getX()));
          yDist = abs(int(asteroid.getY()-asteroids.get(j).getY()));

          if (xDist != 0 && yDist != 0) {
            dist = pow(xDist, 2) + pow(yDist, 2);

            //Asteroidit osuvat toisiinsa
            if (sqrt(dist) < asteroid.getSize()/2+asteroids.get(j).getSize()/2) {
              // Jos asteroidi ei ole timantti asteroidi se hajoaa törmäyksessä
              if (asteroid.getType() != 4) {
                // Tuhotaan asteroidi ja poistetaan se
                if (asteroid.getType() != 3) {
                  factory.destroy(asteroid, ship.getPotato());
                }
                asteroids.remove(i);
                destroyed = true;
                // Jos tuhottu asteroidi on ennen verrattavaa asteroidia täytyy sen indeksiä pienentää
                if (i < j) {
                  j = j-1;
                }
              }
              // Luodaan uusi animaatio räjähdykselle
              Animation anim = new Animation("asterExplo", 18, asteroid.getX(), asteroid.getY(), 2*asteroid.getSize());
              if (animations.size() < 2) {
                animations.add(anim);
              }
              // Jos toinen asteroidi ei ole timanttiasteroidi se tuhoutuu
              if (asteroids.get(j).getType() != 4) {
                if (asteroids.get(j).getType() != 3) {
                  factory.destroy(asteroids.get(j), ship.getPotato());
                }
                destroyed = true;
                asteroids.remove(j);
              }

              if (destroyed == true) {
                break;
              }
            }
          }
        }
      }
    }
  }

  //Tarkastetaan onko pelikentällä enää asteroideja tai aluksia
  if (asteroids.size() == 0 && tempAsteroids.size() == 0 && enemyShips.size() == 0) {

    if (getCompleteBuffer && timeLeft > 5) {
      completeBuffer = timeLeft-5;
      getCompleteBuffer = false;
      displayCompletedText = true;
    }
  }
}

// Piirtää vihollisalukset ruudulle
void displayEnemyShips() {
  for (int i = 0; i < enemyShips.size(); i++) {
    enemyShip = enemyShips.get(i);
    // Jos vihollisalusta ei ole tuhottu, piirretään se ruudulle
    if (enemyShip.getDestroyed() == false) {
      enemyShip.display();
    }
    else {
      // Jos alus on tuhottu ja sillä ei ole enää ammuksia, voidaan se poistaa kokonaan
      if (enemyShip.projectiles() == 0) {
        enemyShips.remove(i);
        continue;
      }
    }
    // Piirretään alusten ammukset ruudulle
    for (int j = 0; j < enemyShip.projectiles(); j++) {
      // Haetaan ammus
      projectile = enemyShip.returnProjectile(j);
      // Piirretään ammus ruudulle
      projectile.display();
      // Liikutetaan alusta
      projectile.move();

      float current = millis();
      // Jos ammus osuu pelaajaan tai menee ulos pelialueelta tuhotaan se
      if (projectile.hitsObject(ship.posX_, ship.posY_, ship.size_/2) && current - ship.getIndestructible() > shipIndestructible && !ship.isImmortal() ) {
        enemyShip.removeProjectile(j);
        // Jos pelaaja tuhoutuu ammuksesta
        if (ship.hit()) {
          int shipDestroyTime = millis();
          // nollataan pelin tila
          while (millis ()-shipDestroyTime < 1000) {
          }
          gameMusic.pause();
          mainMenu.resetGameStart();
          shop.resetOwned();
          ship.writeToFile();
          mainMenu.hiscoresAddCaller();
          level = 1;
          gameState = 0;
          mainMenu.setMenuState(4);
          removeAsteroids();
          removeEnemyShips();
          factory.initialize(2, false);
        }
        // Muuten asetetaan pelaaja tuhoutumattomaksi
        else {
          ship.setIndestructible(millis());
        }
      }
      // Tarkistetaan poistetaanko ammus
      if ( projectile.destroy()) {
        enemyShip.removeProjectile(j);
      }
    }
    float current = millis();
    // Vihollisaluksen ja aluksen väliset törmäykset tarkistetaan vain joka 5:llä freimillä
    if (frameCounter % 5 == 0 && enemyShip.getDestroyed() == false && (current - ship.getIndestructible() > shipIndestructible) && !ship.isImmortal() ) {
      //Tarkistus osuuko vihollinen alukseen
      xDist = abs(int(ship.getX()-enemyShip.getX()));
      yDist = abs(int(ship.getY()-enemyShip.getY()));
      dist = pow(xDist, 2) + pow(yDist, 2);

      //Osuma vihollisen ja aluksen välillä
      if (sqrt(dist) < ship.getSize()/2.6+enemyShip.getSize()/2.6) {
        ship.setIndestructible(millis());
        // Toistetaan aluksen räjähdysääni
        if (!soundIsMute) {
          explosion.play();
          // Kelataan ääni alkuun
          explosion.rewind();
        }
        // Jos alus tuhoutuu törmäyksessä peli loppuu
        if (ship.hit()) {
          gameMusic.pause();
          int shipDestroyTime = millis();
          while (millis ()-shipDestroyTime < 1000) {
          }
          // Nollataan pelin tila
          gameMusic.pause();
          mainMenu.resetGameStart();
          shop.resetOwned();
          ship.writeToFile();
          mainMenu.hiscoresAddCaller();
          level = 1;
          gameState = 0;
          mainMenu.setMenuState(4);
          removeAsteroids();
          removeEnemyShips();
          factory.initialize(2, false);
        }
        // Muuten alus asetetaan hetkeksi kuolemattomaksi
        else {
          ship.setIndestructible(millis());
        }
      }
    }
    // Avaruusalusten ja ammusten väliset törmäykset tarkistetaan joka toisella freimillä
    // Lisäksi törmäyksiä ei tarkisteta jos alus on tuhottu, mutta sillä on vielä piirrettäviä ammuksia
    if ((frameCounter % 5 == 1 || frameCounter % 5 == 3) && enemyShip.getDestroyed() == false) {
      //Tarkistetaan osuuko ammus avaruusalukseen.
      for (int j = 0; j < ship.projectiles(); ++j) {
        projectile = ship.returnProjectile(j);
        // Koska eri projectiletyypeille osum a tarkastellaan eri tavalla (säteet vs. papanat),
        // tarkastelu on projectilella itsellään.
        if ( projectile.hitsObject(enemyShip.getX(), enemyShip.getY(), enemyShip.getSize()/2 ) ) {
          // Osuman tapahtuessa aiheutetaan vahinkoa alukselle ja
          // tuhotaan se, jos vahinkoa on tarpeeksi.       
          enemyShip.causeDamage( projectile.getPower() );
          if ( enemyShip.getLife() <= 0 ) {
            factory.destroy();

            // Luodaan uusi räjähdys animaatio
            Animation anim = new Animation("asterExplo", 18, enemyShip.getX(), enemyShip.getY(), 2*enemyShip.getSize());
            if (animations.size() < 2) {
              animations.add(anim);
            }
            // Asetetaan alus tuhotuksi
            enemyShip.setDestroyed();
            ship.addScore();
            break;
          }

          // Ammustyypistä riippuen, osuessaan kohteeseen ammus joko
          // tuhoutuu tai se räjähtää.
          if ( projectile.getDestroyed() ) {
            ship.removeProjectile( j );
          }
        }
      }
    }
  }
  //Tarkastetaan onko pelikentällä enää asteroideja tai aluksia
  if (asteroids.size() == 0 && tempAsteroids.size() == 0 && enemyShips.size() == 0) {

    if (getCompleteBuffer && timeLeft > 5) {
      completeBuffer = timeLeft-5;
      getCompleteBuffer = false;
      displayCompletedText = true;
    }
  }
}

//Lasketaan jäljellä oleva aika, ennenkuin kenttä vaihtuu
void displayTimeLeft() {
  if (timeLeft > 0) {
    timeLeft = maxTime-(millis()-time-pauseLength)/1000-completeBuffer;
    hud.displayTimeLeft(timeLeft);
  }
  else {
    displayCompletedText = false;
    ++level;
    shop.setMoney(ship.getMoney());
    shop.setMaxShields(ship.getMaxShields());
    shop.setShields(ship.getShields());
    shop.setEnergy(ship.getMaxEnergy());
    shop.setAmmoAmounts(ship.ammoAmounts());
    shop.setShields(ship.getShields());
    gameState = 2;
    getCompleteBuffer = true;
    completeBuffer = 0;
    levelCompletedTextSize = 600;
    levelCompletedAlpha = 0;
  }
}

// Piirtää jääpalat
void displayIceShards() {
  // Käydään kaikki palat läpi
  for (int i = 0; i < iceShards.size(); i++) {
    iceShard = iceShards.get(i);
    // Piirretään pala
    iceShard.display();
    // Tarkistetaan osuuko jääpala pelaajan alukseen
    xDist = abs(int(ship.getX()-iceShard.getX()));
    yDist = abs(int(ship.getY()-iceShard.getY()));
    dist = pow(xDist, 2) + pow(yDist, 2);
    //Osuma jääpalan ja aluksen välillä
    if (sqrt(dist) < ship.getSize()/2.6+iceShard.getSize()/2.6 && millis()-ship.getIndestructible() > shipIndestructible && !ship.isImmortal() ) {
      // Jos pelaaja tuhoutuu
      if (ship.hit()) {
        // Tehdään pelin loppumiseen tarvittavat toimenpiteet
        gameMusic.pause();
        int shipDestroyTime = millis();
        while (millis ()-shipDestroyTime < 1000) {
        }
        // Nollataan pelin tila
        mainMenu.resetGameStart();
        shop.resetOwned();
        ship.writeToFile();
        mainMenu.hiscoresAddCaller();
        level = 1;
        gameState = 0;
        mainMenu.setMenuState(4);
        removeAsteroids();
        removeEnemyShips();
        factory.initialize(2, false);
      }
      // Jos pelaaja ei tuhoudu poistetaan jääpala
      else {
        ship.setIndestructible(millis());
        iceShards.remove(i);
        break;
      }
    }
    if ((frameCounter % 5 == 1 || frameCounter % 5 == 3)) {
      //Tarkistetaan osuuko ammus jääpalaan.
      for (int j = 0; j < ship.projectiles(); ++j) {
        projectile = ship.returnProjectile(j);
        // Koska eri projectiletyypeille osum a tarkastellaan eri tavalla (säteet vs. papanat),
        // tarkastelu on projectilella itsellään.
        if ( projectile.hitsObject(iceShard.getX(), iceShard.getY(), iceShard.getSize()/2 ) ) {   
          // Lisätään pisteitä pelaajalle
          ship.addScore();
          iceShards.remove(i);
          break;
        }

        // Ammustyypistä riippuen, osuessaan kohteeseen ammus joko
        // tuhoutuu tai se räjähtää.
        if ( projectile.getDestroyed() ) {
          ship.removeProjectile( j );
        }
      }
    }
    // Jos jääpala on liikkunut pois ruudulta se poistetaan
    if (iceShard.isOut() == true) {
      iceShards.remove(i);
    }
  }
}


void removeAsteroids() {
  //Poistetaan mahdolliset kentällä olevat asteroidit
  while (asteroids.size () != 0) {
    asteroids.remove(0);
  }

  // Poistetaan väliaikaiset asteroidit
  while (tempAsteroids.size () != 0) {
    tempAsteroids.remove(0);
  }

  // Poistetaan jääpalat
  while (iceShards.size () != 0) {
    iceShards.remove(0);
  }
}

// Funktio, joka muuttaa pelin resoluution
void changeResolution(int resolutionNumber) {
  int resolutionX = 852;
  int resolutionY = 480;
  switch(resolutionNumber) {

  case 1:
    resolutionX = 1280;
    resolutionY = 720;
    break;

  case 2:
    resolutionX = 1600;
    resolutionY = 900;
    break;

  case 3:
    resolutionX = 1920;
    resolutionY = 1080;
    break;
  }
  // Muutetaan peliruudun koko annetun resoluution mukaan
  background(0);
  frame.setResizable(true);
  frame.setSize(resolutionX, resolutionY);
  size(resolutionX, resolutionY);
  frame.setLocation(displayWidth/2-width/2, displayHeight/2-height/2);
  frame.setResizable(false);

  backgroundpic2.resize(width, height);
}

// Näyttää kaikki pickupit ja tarkistaa onko pelaaja kerännyt creditin
void displayPickups() {
  for (int i = 0; i < pickUps.size(); i++) {
    pickUp = pickUps.get(i);
    // Piirretään annettu pickup ruudulle
    pickUp.display();
    // Tarkistetaan törmääkö pelaaja pickuppiin
    if (pickUp.collision(ship.getX(), ship.getY(), ship.getSize())) {
      // Kasvatetaan pelaajan rahamäärää jos kyseessä on creditti määrää
      if (pickUp.getType() == 0) {
        ship.setMoney(ship.getMoney() + pickUp.getValue());
      }
      // Muuten kyseessä on boosti
      else {
        ship.boostPicked(pickUp.getType());
      }
    }
    if (pickUp.getDestroy()) {
      pickUps.remove(i);
    }
    // Pickuppien ja ammusten väliset törmäykset tarkistetaan joka toisella freimillä
    if (frameCounter % 5 == 1 || frameCounter % 5 == 3) {
      for (int j = 0; j < ship.projectiles(); ++j) {
        projectile = ship.returnProjectile(j);
        // Koska eri projectiletyypeille osuma tarkastellaan eri tavalla (säteet vs. papanat),
        // tarkastelu on projectilella itsellään.
        if ( projectile.getType() == "tractorbeam" ) {
          if ( projectile.hitsObject(pickUp.getPosX(), pickUp.getPosY(), pickUp.getSize()/2 ) ) {
            pickUp.moveTowards( ship.getX(), ship.getY() );
          }
        }
      }
    }
  }
}

// Poistaa kaikki pickupit pelialueelta
void removePickups() {
  for (int i = pickUps.size()-1; i >= 0; i--) {
    pickUps.remove(i);
  }
}

// Poistaa kaikki alukset pelialueelta
void removeEnemyShips() {
  for (int i = enemyShips.size()-1; i >= 0; i--) {
    // Poistetaan aluksen ammukset ennen aluksen tuhoamista
    enemyShips.get(i).removeProjectiles();
    enemyShips.remove(i);
  }
}

