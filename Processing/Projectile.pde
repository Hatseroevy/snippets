//Tätä luokkaa käytetään kuvaamaan pelaajan ohjeistaman
//aluksen ampumia ammuksia ja niiden käyttäytymistä.

class Projectile {

  private PImage img_;
  private float posX_;
  private float posY_;
  private float rotation_;
  //private float shipSize_;
  private float speed_;
  private int power_;
  private int energyUsage_;
  private String type_;
  private boolean destroyed_ = false;
  private boolean halted_ = false;
  private int damageRangeRad_;
  // posCorr_ ja rotCorr_ ovat käytössä, kun projectile on
  // osana moniosaista ammustyyppiä. Tällöin ammus ei lähde
  // suoraan aluksen nokan edestä, eikä välttämättä lähde
  // suoraan aluksen eteen.
  private float posCorr_;
  private float rotCorr_;

  //Luodaan uusi ammus aluksen nokan eteen.
  Projectile( float xPos, float yPos, float rotation ) {
    imageMode(CENTER);
    rotation_ = rotation;
    posX_ = xPos;
    posY_ = yPos;
  }

  // Voidaan myös rakentaa suoraan antamalla tyyppi ja voima.
  Projectile( String type, int power, float xPos, float yPos, float rotation ) {  
    imageMode(CENTER);
    rotation_ = rotation;
    posX_ = xPos;
    posY_ = yPos;
    type_ = type;
    power_ = power;
  }

  // getterit ja setterit luokkamuuttujille.
  void setType( String type ) {
    type_ = type;
  }

  void setImage( String filename ) {
    img_ = loadImage( "sprites/" + filename );
  }

  PImage getImage() {
    return img_;
  }

  String getType() {
    return type_;
  }

  void setSpeed( float speed ) {
    speed_ = speed;
  }

  float getSpeed() {
    return speed_;
  }

  void setRotation( float rotation ) {
    rotation_ = rotation;
  }

  float getRotation() {
    return rotation_;
  }

  void setPower( int power ) {
    power_ = power;
  }

  int getPower() {
    return power_;
  }

  void setEnergyUsage( int amount ) {
    energyUsage_ = amount;
  }

  int getEnergyUsage() {
    return energyUsage_;
  }

  void setX( float posX ) {
    posX_ = posX;
  }

  float getX() {
    return posX_;
  }

  void setY( float posY ) {
    posY_ = posY;
  }

  float getY() {
    return posY_;
  }

  void setDestroyed( boolean destroyed ) {
    destroyed_ = destroyed;
  }

  boolean getDestroyed() {
    return destroyed_;
  }

  void setHalted( boolean halted ) {
    halted_ = halted;
  }

  void setDamageRangeRad( int rad ) {
    damageRangeRad_ = rad;
  }

  int getDamageRangeRad() {
    return damageRangeRad_;
  }

  //Ammuksen piirtäminen peliin
  void display() {
    pushMatrix();
    translate(posX_, posY_);    
    image(img_, 0, 0);
    popMatrix();
    move();
  }

  //Liikutetaan ammusta eteenpäin
  void move() {
    if ( !halted_ ) {
      posX_ = posX_+(speed_*sin(rotation_));
      posY_ = posY_-(speed_*cos(rotation_));
    }
  }

  boolean destroy() {
    if (posX_ > width+img_.width/2 ||
      posX_ < 0-img_.width/2 ||
      posY_ > height+img_.height/2 ||
      posY_ < 0-img_.height/2) {
      return true;
    }
    else {
      return false;
    }
  }


  boolean hitsObject( float objPosX, float objPosY, float objRadius) {
    int xDist = abs(int(posX_-objPosX));
    int yDist = abs(int(posY_-objPosY));
    float dist = sqrt(pow(xDist, 2) + pow(yDist, 2));
    if ( dist < (objRadius+damageRangeRad_) ) {
      return true;
    }
    return false;
  }
}





/*
________________________________________________________________
 
 REGULAR PROJECTILE
 
 ________________________________________________________________
 */

// Tavallinen ammus tuhoutuu osuessaan.

class RegularProjectile extends Projectile {

  // xPos ja yPos ovat ammuksen luoneen aluksen sijainti. 
  RegularProjectile( float xPos, float yPos, float rotation) {
    super( "regular", 10, xPos, yPos, rotation);
    setImage("Projectile.png");
    setDamageRangeRad( getImage().width/2 );
    setSpeed(4.0);
  }

  boolean hitsObject( float objPosX, float objPosY, float objRadius) {
    boolean doesHit = super.hitsObject( objPosX, objPosY, objRadius );
    if ( doesHit ) {
      setDestroyed( true );
    }
    return doesHit;
  }
}





/*
________________________________________________________________
 
 EXPLOSIVE
 
 ________________________________________________________________
 */

// Räjähtävä ammus aiheuttaa nimensä mukaan räjähdyksen
// tuhoutuessaan, aiheuttaen vahinkoa kaikille räjähdys-
// alueelle osuneille kohteille.

class Explosive extends Projectile {

  int explosionRange_ = 150;
  boolean exploding_ = false;

  Explosive( float xPos, float yPos, float rotation) {
    super( "explosive", 20, xPos, yPos, rotation);
    setImage( "ExplosiveProjectile.png");
    setDamageRangeRad( getImage().width/2 );
    setSpeed(2.0);
  }

  void display() {

    pushMatrix();
    translate( getX(), getY());  
    if ( exploding_) {  
      image( getImage(), 0, 0, getDamageRangeRad(), getDamageRangeRad());
    } 
    else {
      image( getImage(), 0, 0, getImage().width, getImage().height );
    }
    popMatrix();
    move();
  }

  void move() {
    super.move();
    if ( exploding_ ) {
      int newRad = getDamageRangeRad()+4;
      setDamageRangeRad( newRad );
      setImage("ExplosiveCloud.png");
    }
  }

  boolean destroy() {
    if ( getDamageRangeRad() > explosionRange_ ) {
      return true;
    }
    return super.destroy();
  }

  boolean hitsObject( float objPosX, float objPosY, float objRadius) {
    boolean doesHit = super.hitsObject( objPosX, objPosY, objRadius );
    if ( doesHit ) {
      exploding_ = true;
      setPower( 2 );
      setHalted( true );
    }
    return doesHit;
  }
}





/*
________________________________________________________________
 
 BEAM
 
 ________________________________________________________________
 */

// Säteet on paljon energiaa kuluttava asetyyppi, joten sillä on 
// rajattu ammunta-aika. Laser- ja Tractorsäteen käytös ovat lähes
// samat, mutta aiheutettu vahinko, eloaika ja nopeus poikkeavat.
// Tractorsäde vetää asteroideja puoleensa, mutta tämä käsitellänä
// alus_testin puolella. Laser tuhoaa.

class Beam extends Projectile {

  // maximietäisyys on kuvaruudun halkaisija. sen pitemmältä ei tutkita osumia
  int maxDistance_ = int(sqrt(pow(width, 2) + pow(height, 2)));
  int distance_ = 0;
  boolean hitsObject_ = false;

  Beam( boolean isTractorBeam, float xPos, float yPos, float rotation) {   
    super( xPos, yPos, rotation );

    if ( isTractorBeam ) {
      super.setPower(0);
      super.setType( "tractorbeam" );
      setImage("TractorBeam.png");
      setDamageRangeRad( 20 );
      setSpeed(20.0);
      setEnergyUsage(1);
    } 

    else {
      super.setPower(20);
      super.setType( "laserbeam" );
      setImage( "LaserBeam.png" );
      setDamageRangeRad( 5 );
      setSpeed(40.0);
      setEnergyUsage(8);
    }
  }

  // Seuraa aina aluksen asentoa.
  void move() {
    if ( distance_ < maxDistance_ ) {
      distance_ += getSpeed();
    }
  }

  void display() {
    pushMatrix();
    translate(getX(), getY());
    rotate( getRotation() );
    image(getImage(), 0, -distance_/2, getImage().width, distance_ );
    popMatrix();
    move();
  }

  boolean destroy() {
    return false;
  }


  // Aloittaa tutkimalla annetun pisteiden väliltä (closerEnd ja furtherEnd) keskeltä
  // osuuko parametrinä annetun objekti (objectin keskikohta: objPos ja säde: objRadius )
  // säteen vahinkoaluelle.

  void recHitsObject( float objPosX, float objPosY, float objRadius, 
  float closerEndX, float closerEndY, float furtherEndX, float furtherEndY) {

    if ( hitsObject_ || 
      ( (abs(closerEndX-furtherEndX)) < 1 && (abs(closerEndY-furtherEndY)) < 1) ) { 
      return;
    }

    float middleX = (closerEndX + furtherEndX)/2;
    float middleY = (closerEndY + furtherEndY)/2;

    int xDist = abs(int(middleX-objPosX));
    int yDist = abs(int(middleY-objPosY));
    float dist = sqrt(pow(xDist, 2) + pow(yDist, 2));
    if ( dist < (objRadius+getDamageRangeRad()) ) {
      hitsObject_ = true;
    }
    recHitsObject(objPosX, objPosY, objRadius, closerEndX, closerEndY, middleX, middleY);
    recHitsObject(objPosX, objPosY, objRadius, middleX, middleY, furtherEndX, furtherEndY);
  }


  boolean hitsObject( float objPosX, float objPosY, float objRadius) {

    // Sädetyypin aseilla tutkitaan osumia koko säteen matkalta.
    hitsObject_ = false;
    float beamEndX = getX() + (distance_)*sin( getRotation() );
    float beamEndY = getY() - (distance_)*cos( getRotation() );
    recHitsObject(objPosX, objPosY, objRadius, getX(), getY(), beamEndX, beamEndY);
    return hitsObject_;
  }
}

/*
________________________________________________________________
 
 MINE
 
 ________________________________________________________________
 */

//Miina, joka leijuu avaruudessa ympäriinsä ja osuessaan
//johonkin kiinteään kohteeseen räjähtää, ja tuottaa vahinkoa
//ympärilleen.

class Mine extends Projectile {

  int explosionRange_ = 100;
  boolean exploding_ = false;
  private float rotation_ = 0;
  private float deltaRotation_ = (random(-0.01, 0.01));

  Mine( float xPos, float yPos, float rotation) {
    super( "mine", 30, xPos, yPos, rotation);
    setImage( "mine.png");
    setDamageRangeRad( getImage().width/2 );
    setSpeed(-0.2);
  }

  void display() {
    rotation_ += deltaRotation_;

    pushMatrix();
    translate( getX(), getY());  
    rotate(rotation_);
    if ( exploding_) {  
      image( getImage(), 0, 0, getDamageRangeRad(), getDamageRangeRad());
    } 
    else {
      image( getImage(), 0, 0, getImage().width, getImage().height );
    }
    popMatrix();
    move();
  }

  void move() {
    super.move();
    if ( exploding_ ) {
      setImage( "MineCloud.png");
      int newRad = getDamageRangeRad()+4;
      setDamageRangeRad( newRad );
    }
  }

  boolean destroy() {
    if ( getDamageRangeRad() > explosionRange_ ) {
      return true;
    }
    return super.destroy();
  }

  boolean hitsObject( float objPosX, float objPosY, float objRadius) {
    boolean doesHit = super.hitsObject( objPosX, objPosY, objRadius );
    if ( doesHit ) {
      exploding_ = true;
      setPower( 2 );
      setHalted( true );
    }
    return doesHit;
  }
}

/*
________________________________________________________________
 
 Railgun
 
 ________________________________________________________________
 */

//Railgun  ei tuhoudu, kun se osuu 

class Railgun extends Projectile {

  // xPos ja yPos ovat ammuksen luoneen aluksen sijainti. 
  Railgun( float xPos, float yPos, float rotation) {
    super( "railgun", 50, xPos, yPos, rotation);
    setImage("railgun.png");
    setDamageRangeRad( getImage().width/2 );
    setSpeed(20.0);
  }

  boolean hitsObject( float objPosX, float objPosY, float objRadius) {
    boolean doesHit = super.hitsObject( objPosX, objPosY, objRadius );


    return doesHit;
  }

  void display() {
    pushMatrix();
    translate(getX(), getY());
    rotate( getRotation() );
    image( getImage(), 0, 0, getImage().width, getImage().height );
    popMatrix();
    move();
  }
}

