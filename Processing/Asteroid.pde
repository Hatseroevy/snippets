//Luokka kuvaa pelialueella liikkuvia asteroideja,
//joita pelaajan tulee tuhota ja väistellä
class Asteroid extends Enemy {

  int type_;
  //Luodaan uusi asteroidi annettuun paikkaan
  //Satunnaisen kokosena ja satunnaisella nopeudella liikkuvaksi
  Asteroid(float x, float y, float size, boolean potato, int type) {
    // Kutsutaan kantaluokan rakentajaa
    super(x, y, size);
    type_ = type;
    // Arvotaan kulma jossa synnytään
    setRotation(random(-1, 1));
    // Arvotaan pyörimisnopeus
    setDeltaRotation(random(-0.03, 0.03));

    if (potato) {
      setImage("sprites/potato.png");
    }
    else {
      setImage("sprites/asteroid"+int(type_)+".png");
    }
    // Arvotaan x:n ja y:n muutosnopeudet
    setDeltaX(random(-getMaxSpeed(), getMaxSpeed()));
    setDeltaY(random(-getMaxSpeed(), getMaxSpeed()));
  }

  // Rakentaja, jota käytetään asteroidien hajotessa
  Asteroid(float x, float y, float size, float deltaX, float deltaY, boolean potato, int type) {
    // Kutsutaan kantaluokan rakentajaa
    super(x, y, size);
    // Asetetaan x:n ja y:n muutosnopeudet sekä kulma ja pyörimisnopeus
    setDeltaX(deltaX);
    setDeltaY(deltaY);
    setRotation(random(-1, 1));
    setDeltaRotation(random(-0.04, 0.04));
    type_ = type;

    if (potato) {
      setImage("sprites/potato.png");
    }
    else {
      setImage("sprites/asteroid"+int(type_)+".png");
    }
  }

  // Palauttaa asteroidin tyypin
  int getType() {
    return type_;
  }

  // Pienentää jää asteroidin kokoa 
  void melt() {
    if (type_ == 3) {
      setSize(getSize()-5);
    }
  }
}

