// Asteroidin erikoistyyppi jääasteroidi
class IceAsteroid extends Asteroid {

  //Luodaan uusi jääasteroidi annettuun paikkaan
  //Satunnaisen kokosena ja satunnaisella nopeudella liikkuvaksi
  IceAsteroid(float x, float y, float size, boolean potato, int type) {

    // Kutsutaan kantaluokan rakentajaa
    super(x, y, size, potato, type);
    // Asetetaan jääasteroidille normaalia vähemmän "elämää"
    setLife(5);
  }
}

