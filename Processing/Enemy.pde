// Kantaluokka kaikille vihollistyypeille
class Enemy {

  private float posX_;
  private float posY_;
  private float size_;
  private float deltaX_;
  private float deltaY_;
  private float rotation_;
  private float deltaRotation_;
  private float maxSpeed_ = 2;
  private int life_ = 15;
  private float indestructible_ = 0;
  PImage img_;

  //Luodaan uusi vihollinen annettuun paikkaan
  // annetun kokoisena
  Enemy(float x, float y, float size) {
    posX_ = x;
    posY_ = y;
    size_ = size;
  }
  //Vihollisten piirto peliauleelle
  void display() {
    rotation_ = rotation_ + deltaRotation_;
    pushMatrix();
    translate(posX_, posY_);
    rotate(rotation_);
    image(img_, 0, 0, size_, size_);
    popMatrix();
    move();
  }

  // Palauttaa x-koordinaatin
  float getX() {
    return posX_;
  }

  // Palauttaa y-koordinaatin
  float getY() {
    return posY_;
  }

  // Palauttaa koon
  float getSize() {
    return size_;
  }

  // Asettaa kuvan koon
  void setSize(float size) {
    size_ = size;
  }

  // Palauttaa x_n muutosnopeuden
  float getDeltaX() {
    return deltaX_;
  }

  // Palauttaa y:n muutosnopeuden 
  float getDeltaY() {
    return deltaY_;
  }

  // Palauttaa käännöskulman
  float getRotation() {
    return rotation_;
  }

  // Palauttaa maksiminopeuden
  float getMaxSpeed() {
    return maxSpeed_;
  }

  // Asettaa x:n muutosnopeuden
  void setDeltaX(float newDeltaX) {
    deltaX_ = newDeltaX;
  }

  // Asetaa y:n muutosnopeuden
  void setDeltaY(float newDeltaY) {
    deltaY_ = newDeltaY;
  }

  // Asettaa kulman
  void setRotation(float newRotation) {
    rotation_ = newRotation;
  }

  // Asettaa pyörimisnopeuden
  void setDeltaRotation(float newDeltaRotation) {
    deltaRotation_ = newDeltaRotation;
  }

  // Asettaa kuvan
  void setImage(String path) {
    img_ = loadImage(path);
  }

  // Palauttaa elinvoiman
  int getLife() {
    return life_;
  }

  // Vähentää elinvoimaa
  void causeDamage( int damage ) {
    life_ -= damage;
  }

  // Asettaa x-paikan
  void setPosX(float posX) {
    posX_ = posX;
  }

  // Asettaa y-paikan
  void setPosY(float posY) {
    posY_ = posY;
  }

  //Vihollisen liikutus
  void move() {
    posX_ = posX_+deltaX_;
    posY_ = posY_+deltaY_;

    //Jos vihollinen liikkuu ulos pelialueelta ilmestyy
    //se pelialueen toiselle reunalle
    if (posY_ >= height+img_.width/2) {
      posY_ = 1-img_.width/2;
    }
    if (posX_ >= width+img_.width/2) {
      posX_= 1-img_.width/2;
    }
    if (posY_ <= 0-img_.width/2) {
      posY_ = (height-1)+img_.width/2;
    }
    if (posX_ <= 0-img_.width/2) {
      posX_= (width-1)+img_.width/2;
    }
  }

  // Liikuttaa vihollista annettua paikkaa kohti
  void moveTowards( float posX, float posY ) {
    if ( posX > posX_ ) { 
      if ( deltaX_ < maxSpeed_ ) {
        deltaX_+=0.05;
      }
    }
    if ( posX < posX_ ) { 
      if ( deltaX_ > -maxSpeed_ ) {
        deltaX_-=0.05;
      }
    }
    if ( posY > posY_ ) { 
      if ( deltaY_ < maxSpeed_ ) {
        deltaY_+=0.05;
      }
    }
    if ( posY < posY_ ) { 
      if ( deltaY_ > -maxSpeed_ ) {
        deltaY_-=0.05;
      }
    }
  }

  // Asettaa ajan, jolloin vihollinen on luotu. Tätä käytetään pitämään vihollinen tuhoutumattomana luonnin jälkeen
  void setIndestructible(float time) {
    indestructible_ = time;
  }

  // Palauttaa vihollisen luomisajan
  float getIndestructible() {
    return indestructible_;
  }

  // Funktio, jonka avulla voidaan asettaa vihollisen
  // elämä
  void setLife(int life) {
    life_  = life;
  }
}

