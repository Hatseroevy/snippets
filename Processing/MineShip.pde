// Vihollisaluksesta erikoistettu tapaus eli alus joka tiputtaa miinoja
class MineShip extends EnemyShip {

  MineShip(float x, float y, float size) {
    // Kutsutaan kantaluokan rakentajaa
    super(x, y, 75);
    // Ladataan miina-aluksen kuva
    setImage("sprites/minedropperUfo.png");
  }

  // Oma erikoistettu liikkumisfunktio 
  void move() {
    // Jos x suuntainen nopeus on sallitulla välillä
    if (getDeltaX() > getTargetDeltaX()-0.03 && getDeltaX() < getTargetDeltaX()+0.03) {
      // Asetetaan nopeus saavutetuksi
      setTargetXAchieved(true);
    }
    else {
      // Muuten muutetaan nopeutta
      if (getDeltaX() < getTargetDeltaX()) {
        setDeltaX(getDeltaX()+0.01);
      }
      else {
        setDeltaX(getDeltaX()-0.01);
      }
    }
    // Jos y suuntainen nopeus on sallitulla välillä
    if (getDeltaY() > getTargetDeltaY()-0.03 && getDeltaY() < getTargetDeltaY()+0.03) {
      // Asetetaan nopeus saavutetuksi
      setTargetYAchieved(true);
    }
    else {
      // Muuten muutetaan nopeutta
      if (getDeltaY() < getTargetDeltaY()) {
        setDeltaY(getDeltaY()+0.01);
      }
      else {
        setDeltaY(getDeltaY()-0.01);
      }
    }
    // Jos molemmat nopeudet on saavutettu siirrytään ampumaan
    if (getTargetXAchieved() && getTargetYAchieved()) {
      // Miina-alus voi jättää kerrallaan neljä miinaa
      if (projectiles() < 4) {
        // Luodaan uusi miina
        addProjectile( pFact_.createProjectiles("Mine", getX(), getY(), 0));
        // Arvotaan uudet tavoite nopeudet
        setTargetDeltaX(random(-getMaxSpeed(), getMaxSpeed()));
        setTargetDeltaY(random(-getMaxSpeed(), getMaxSpeed()));
        setTargetXAchieved(false);
        setTargetYAchieved(false);
      }
    }

    setPosX(getX()+getDeltaX());
    setPosY(getY()+getDeltaY());

    //Jos alus liikkuu ulos pelialueelta ilmestyy
    //se pelialueen toiselle reunalle
    if (getY() >= height+img_.width/2) {
      setPosY(1-img_.width/2);
    }
    if (getX() >= width+img_.width/2) {
      setPosX(1-img_.width/2);
    }
    if (getY() <= 0-img_.width/2) {
      setPosY((height-1)+img_.width/2);
    }
    if (getX() <= 0-img_.width/2) {
      setPosX((width-1)+img_.width/2);
    }
  }
}

