// Luokka joka piirtää animaation ruudulle
class Animation {
  // Animaation kuvat
  PImage[] images;
  // Missä kuvassa mennään
  private int imageCount_ = 0;
  private float x_;
  private float y_;
  private float size_;
  private int frame = 0;
  private boolean frameChange = true;

  Animation(String filename, int imageCount, float x, float y, float size) {
    imageCount_ = imageCount;
    x_ = x;
    y_ = y;
    size_ = size;
    images = new PImage[imageCount];
    // Ladataan kuvat
    for (int i = 0; i < imageCount; ++i) {
      images[i] = loadImage("sprites/" + filename + "/" + filename + i + ".png");
    }
  }

  boolean display() {
    // Kuvaa ei vaihdeta joka piirto kerralla
    if (frameChange == true) {
      frameChange = false;
    }
    else {
      frameChange = true;
      frame = (frame+1);
    }
    
    if (frame < imageCount_) {
      image(images[frame], x_, y_, size_, size_);
      return true;
    }
    else {
      return false;
    }
  }
}

