// Luokka jota käytetään kun jääasteroidi hajoaa pieniksi palasiksi
class IceShard extends Enemy {

  IceShard(float x, float y, boolean potato, float deltaX, float deltaY) {
    super(x, y, 15);
    if (potato) {
      setImage("sprites/potato.png");
    }
    else {
      setImage("sprites/asteroid3.png");
    }
    // Arvotaan kulma jossa synnytään
    setRotation(random(-1, 1));
    // Arvotaan pyörimisnopeus
    setDeltaRotation(random(-0.05, 0.05));

    // Arvotaan x:n ja y:n muutosnopeudet
    setDeltaX(random(deltaX));
    setDeltaY(random(deltaY));
    // Asetetaan jääasteroidille normaalia vähemmän "elämää"
    setLife(1);
  }

  // Palauttaa tiedon siitä onko jääpala liikkunut ulos ruudulta
  boolean isOut() {
    if (getY() >= height+getSize()/2) {
      return true;
    }
    if (getX() >= width+getSize()/2) {
      return true;
    }
    if (getY() <= 0-getSize()/2) {
      return true;
    }
    if (getX() <= 0-getSize()/2) {
      return true;
    }
    return false;
  }
}

