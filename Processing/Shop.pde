//Luokka, jonka avulla aseiden tiedot saadaan talteen ArrayListiin
public class weaponData {
  public String name;
  public String type;
  public int speed;
  public int damage;
  public String fireRate;
  public int weaponCost;
  public int ammoCost;
  public int ammoCount;
  boolean owned;
  String ownedString;
}

//Power-uppien tiedot
public class powerUpData {
  public String name;
  public int cost;
  public int owned;
}

//Public class joka pitää ostettujen aseiden tietoja tallessa
public class shoppingCart {
  public String itemName;
  public int amount;
}


//Kauppaluokka. Pelaaja voi kenttien välissä käyttää ansaitsemaansa rahaa ostamalla
//kaupasta uusia aseita, panoksia tai poweruppeja
class Shop {

  private int shipMoney_;
  private int cost_;
  private int shopMenuState_;
  private int selectedItem_;
  private int selectedTab_;
  private boolean continue_;

  private boolean weaponsLoaded_;

  private ArrayList<weaponData> weaponList_;
  private ArrayList<powerUpData> powerUpList_;
  private ArrayList<shoppingCart> shoppingCart_;
  private BufferedReader reader;

  private int maxShields_;
  private int maxEnergy_;
  private int shipEnergy_;

  private PImage hud_;

  //Rakentaja. Luodaan kauppaolio ja asetetaan sen arvot oletusarvoihin
  Shop() {
    //Tähän arraylistiin talletetaan aseet
    weaponList_ = new ArrayList<weaponData>();
    powerUpList_ = new ArrayList<powerUpData>();
    shoppingCart_ = new ArrayList<shoppingCart>();
    reader();
    selectedItem_ = -1;
    selectedTab_ = 1;
    continue_ = false;
    maxShields_ = 0;
    maxEnergy_ = 500;
    shipEnergy_ = 0;
    hud_ = loadImage("sprites/shophud.png");

    weaponsLoaded_ = false;
  }

  //Asetetaan pelaajan rahamäärä
  void setMoney(int money) {
    shipMoney_ = money;
  }

  //Palautetaan pelaajan rahamäärä ostosten jälkeen
  int getMoney() {
    return shipMoney_;
  }

//asetetaan maksimisuojamäärä. Tätä enempää suojia ei
//voida ostaa
  void setMaxShields(int maxShields) {
    maxShields_ = maxShields;
  }

//asetetaan omistettujen suojien määrä
  void setShields(int shields) {
    powerUpList_.get(0).owned = shields;
  }

//Palautetaan omistettujen suojien määrä
  int getShields() {
    return powerUpList_.get(0).owned;
  }

//Asetetaan pelaajan energiamäärä
  void setEnergy(int energy) {
    shipEnergy_ = energy;
    powerUpList_.get(1).owned = energy;
  }

//Palautetaan energiamäärä
  int getEnergy() {
    return shipEnergy_;
  }

//Ammusten talletus ostoskoriin
  void setAmmoAmounts(ArrayList<shoppingCart> ammos) {
    for (int i = 0; i < ammos.size(); ++i) {
      for ( int j = 0; j < weaponList_.size(); ++j) {
        if (ammos.get(i).itemName.equals(weaponList_.get(j).name) ) {
          weaponList_.get(j).ammoCount = ammos.get(i).amount;
          break;
        }
      }
    }
  }

  //Liikkuminen kaupan tavaroissa yös/alas
  void selectedItemUp() {

    //Myös se vaikuttaa, millä kaupan välilehdellä ollaan
    //Aseiden tabi on 1 ja poweruppien 2
    if (selectedItem_ > -1 && selectedTab_ == 1) {
      --selectedItem_;
    }
    else if (selectedTab_ == 1) {
      selectedItem_ = weaponList_.size();
    }

    if (selectedItem_ > -1 && selectedTab_ == 2) {
      --selectedItem_;
    }
    else if (selectedTab_ == 2) {
      selectedItem_ = powerUpList_.size();
    }
  }

  //Voidaan asettaa valitsin haluttuun tavaran indeksiin
  //Funktio on ehkä turha, poista jos ei löydy käyttöä!!!!!!!!!!
  void setSelectedItem(int index) {
    selectedItem_ = index;
  }

  void selectedItemDown() {
    if (selectedItem_ < weaponList_.size() && selectedTab_ == 1) {
      ++selectedItem_;
    } 
    else if (selectedTab_ == 1) {
      selectedItem_ = -1;
    }
    if (selectedItem_ < powerUpList_.size() && selectedTab_ == 2) {
      ++selectedItem_;
    }
    else if (selectedTab_ == 2) {
      selectedItem_ = -1;
    }
  }

  //Välilehtien selaus oikealle ja vasemmalle
  void tabRight() {
    if (selectedTab_ < 2) {
      ++selectedTab_;
    }
  }

  void tabLeft() {
    if (selectedTab_ > 1) {
      --selectedTab_;
    }
  }

  //Palautetaan sillä hetkellä valitun asian indeksin numero.
  int getSelectedItem() {
    return selectedItem_;
  }

  //Palautetaan aselistan koko
  int getListSize() {
    return weaponList_.size();
  }

  //Funktio, joka hoitaa tavaran ostamisen ja sen lisäämisen ostoskoriin.
  void buy() {
    int price = 0;
    if (selectedTab_ == 1) {

      //Jos pelaaja omistaa jo aseen, on hinta yhden ammuspaketin hinta
      if (weaponList_.get(selectedItem_).owned) {
        price = weaponList_.get(selectedItem_).ammoCost;
      }
      //Muuten pelaaja joutuu ostamaan aseen, ja hinta on silloin aseen hinta.
      else {
        price = weaponList_.get(selectedItem_).weaponCost;
        //Kun ase ostetaan merkataan se pelaajan omistamaksi, jotta siihen voidaan
        //ostaa panoksia
      }
    }
    //Ollaankin powerup-tabissa, joten nyt käsitellään poweruppien hintoja
    else if (selectedTab_ == 2) {
      price = powerUpList_.get(selectedItem_).cost;
    }

    //Tarkastetaan riittävätkö pelaajan rahat ostoksen tekoon
    if (price <= shipMoney_) {

      if (selectedTab_ == 1) {
        if (!weaponList_.get(selectedItem_).owned) {
          weaponList_.get(selectedItem_).owned = true;
          weaponList_.get(selectedItem_).ownedString = "Yes";
        }
        //Rahat riittävät, joten ostos tapahtuu. Luodaan uusi ostos,
        //ja vähennetään pelaajan rahamäärää.

        shipMoney_ = shipMoney_ - price;

        //Ammukset talletetaan shoppingCart_ ArrayListiin.
        shoppingCart item = new shoppingCart();
        item.itemName = weaponList_.get(selectedItem_).name;
        item.amount = 10;
        weaponList_.get(selectedItem_).ammoCount += 10;

        //Käydään läpi ostoskorin sisältö
        for (int i = 0; i < shoppingCart_.size(); ++i) {

          //Jos ostoskorista löytyy jo saman niminen tuote, päivitetänä sen määrää
          if (shoppingCart_.get(i).itemName == item.itemName) {
            shoppingCart_.get(i).amount = shoppingCart_.get(i).amount + item.amount; 
            return;
          }
        }
        //Saman nimistä tuotetta ei löytynyt. Ludaan uusi tuote
        shoppingCart_.add(item);
      }

      //PowerUppien ostaminen
      else if (selectedTab_ == 2) {

        if (selectedItem_ == 0 && powerUpList_.get(selectedItem_).owned < maxShields_) {
          shipMoney_ -= price;
          ++powerUpList_.get(selectedItem_).owned;
        }
        else if (selectedItem_ == 1 && shipEnergy_ < maxEnergy_) {
          shipMoney_ -= price;
          shipEnergy_ += 25;
          powerUpList_.get(selectedItem_).owned = shipEnergy_;
        }
      }
    }
  }

  //Palautetaan ostoskorin sisältö
  ArrayList<shoppingCart> getBoughtWeapons() {
    return shoppingCart_;
  }


  //Tarkistetaan onko valinta sen napin kohdalla, joka käynnistää uuden kentän
  boolean getContinue() {
    return continue_;
  }

  //Funktio on ehkä turha, poista jos ei löydy käyttöä!!!!!!!!!!
  void setContinue(boolean bool) {
    continue_ = bool;
  }

  //Palautetaan kauppa alkutilaan kun ostokset on tehty
  //ja uusi kenttä alkaa
  void reset() {
    selectedItem_ = -1;
    selectedTab_ = 1;
    continue_ = false;
    while (shoppingCart_.size () != 0) {
      shoppingCart_.remove(0);
    }
  }

  //Omistettujen aseiden ja PowerUppien nollaaminen
  void resetOwned() {

    //Käydään PowerUp lista läpi ja merkataan kaikki sen sisältö siten, ettei
    //se ole vielä pelaajan ostamaa
    for (int i = 0; i < powerUpList_.size(); ++i) {
      powerUpList_.get(i).owned = 0;
    }

    //Sama juttu aselistalle. Nollataan myös pelaajan omistamisen panosten määrä.
    for (int i = 0; i < weaponList_.size(); ++i) {
      weaponList_.get(i).owned = false;
      weaponList_.get(i).ownedString = "No";
      weaponList_.get(i).ammoCount = 0;
    }
  }

  //Piirretään kauppa ruudulle
  void display() {

    image(hud_, width/2-10, height/2-95);
    //Pelaajan senhetkinen rahatilanne
    fill(255);
    text("Money: " +shipMoney_, 100, height-50);

    //Tarkistetaan onko asevälilehti aktiivinen vai ei
    //Aktiivinen välilehti värjätään valkoiseks ja ei aktiivinen harmaaksi
    if (selectedItem_ == -1 && selectedTab_ == 1) {
      fill(255);
    }
    else {
      fill(#1fd2ff);
    }
    textSize(25);
    text("Weapons", width/2-400, height/2-200);

    //Sama tarkastelu powreup-välilehdelle
    if (selectedItem_ == -1 && selectedTab_ == 2) {
      fill(255);
    }
    else {
      fill(#1fd2ff);
    }
    text("Power Ups", width/2-200, height/2-200);

    //Jos ollaan ase välilehdellä tulostetaan aseet ja niiden tiedot
    if (selectedTab_ == 1) {

      //Ylös tulostettavat tiedot, jotka selittävät mitä taulukon kentät tarkoittavat
      textAlign(LEFT);
      textSize(22);
      fill(#1fd2ff);
      text("Name:", width/2-550, height/2-150);
      text("Type:", width/2-425, height/2-150);
      text("Speed:", width/2-300, height/2-150);
      text("damage:", width/2-175, height/2-150);
      text("Fire"+"\n"+"rate:", width/2-50, height/2-150);
      text("Weapon"+"\n"+"cost:", width/2+75, height/2-150);
      text("Ammo"+"\n"+"cost:", width/2+200, height/2-150);
      text("Ammo"+"\n"+"owned:", width/2+325, height/2-150);
      text("Owned:", width/2+450, height/2-150);
      textSize(18);

      //Silmukka, joka tulostaa aseet ja niiden tiedot pelaajan nähtäville
      for (int i = 0; i < weaponList_.size(); ++i) {
        if (selectedItem_ == i) {
          fill(255);
        }
        else {
          fill(#1fd2ff);
        }
        text(weaponList_.get(i).name, width/2-550, height/2+i*40-100);
        text(weaponList_.get(i).type, width/2-425, height/2+i*40-100);
        text(weaponList_.get(i).speed, width/2-300, height/2+i*40-100);
        text(weaponList_.get(i).damage, width/2-175, height/2+i*40-100);
        text(weaponList_.get(i).fireRate, width/2-50, height/2+i*40-100);
        text(weaponList_.get(i).weaponCost, width/2+75, height/2+i*40-100);
        text(weaponList_.get(i).ammoCost, width/2+200, height/2+i*40-100);
        text(weaponList_.get(i).ammoCount, width/2+325, height/2+i*40-100);
        text(weaponList_.get(i).ownedString, width/2+450, height/2+i*40-100);
      }


      //Jos ollaan continue napin päällä värjätään se valkoiseksi
      //Tämä if else tarkistaa sen.
      if (selectedItem_ == weaponList_.size()) {
        fill(255);
        continue_ = true;
      }
      else {
        fill(#1fd2ff);
        continue_ = false;
      }
    }

    //Poweruppien välilehti valittuna. Toiminta samalla periaatteella kuin aseillakin
    else if (selectedTab_ == 2) {

      textAlign(LEFT);
      textSize(22);
      fill(#1fd2ff);
      text("Name:", width/2-550, height/2-150);
      text("Buying"+"\n"+"cost:", width/2-425, height/2-150);
      text("Owned:", width/2-300, height/2-150);
      textSize(18);

      for (int i = 0; i < powerUpList_.size(); ++i) {
        if (selectedItem_ == i) {
          fill(255);
        }
        else {
          fill(#1fd2ff);
        }
        text(powerUpList_.get(i).name, width/2-550, height/2+i*40-100);
        text(powerUpList_.get(i).cost, width/2-425, height/2+i*40-100);
        text(powerUpList_.get(i).owned, width/2-300, height/2+i*40-100);
      }
      textAlign(LEFT);
      textSize(18);

      if (selectedItem_ == powerUpList_.size()) {
        fill(255);
        continue_ = true;
      }
      else {
        fill(#1fd2ff);
        continue_ = false;
      }
    }

    text("Continue to next level", width/2+400, height-40);
    textAlign(CENTER);
    fill(#1fd2ff);
    textSize(18);
  }

  //Reader lukee tiedostosta aseiden tiedot arraylistiin
  void reader() {
    int position;

    String name_;
    String type_;
    int speed_;
    int damage_;
    String fireRate_;
    int weaponCost_;
    int ammoCost_;
    boolean read = true;
    String line_;
    int position_;

    //Tiedoston nimi, jossa aseet ovat
    reader = createReader("info/shop.txt");

    while (read) {
      //Tarkistetaan saadaanko tiedsotoa luettua.
      try {

        //Yritetään lukea tiedostosta rivi muuttujaan line_
        line_ = reader.readLine();
      }
      catch (IOException e) {
        e.printStackTrace();
        line_ = null;
      }
      if (line_ == null) {
        read = false;
      }

      //Jos tiedostosta löytyy luettavaa tavaraa riviltä aletaan sitä tarkastelemaan
      else {

        if (!weaponsLoaded_) {

          if (line_.equals("**********") == true ) {
            weaponsLoaded_ = true;
          }
          else {
            //|-merkit toimivat hiscores-tiedostossa eri tietojen erottimena.
            //Etsitään ensimmäinen erotinmerkki
            position_ = line_.indexOf("|");

            //Luetaan tiedostosta aseen nimi
            name_ = line_.substring(0, position_);

            //Poistetaan line_-muuttujan alusta tavara ensimmäiseen erottimeen asti.
            line_ = line_.substring(position_+1);

            //Etsitään seuraava erotinmerkki
            position_ = line_.indexOf("|");

            //Luetaan aseen tyyppi tiedostosta
            type_ = line_.substring(0, position_);

            line_ = line_.substring(position_+1);
            position_ = line_.indexOf("|");

            //Ammuksen liikkumisnopeus
            speed_ = int(line_.substring(0, position_));

            line_ = line_.substring(position_+1);
            position_ = line_.indexOf("|");

            //Ammuksen aiheuttama vahinko
            damage_ = int(line_.substring(0, position_));

            line_ = line_.substring(position_+1);
            position_ = line_.indexOf("|");

            //Tulitusnopeus
            fireRate_ = line_.substring(0, position_);

            line_ = line_.substring(position_+1);
            position_ = line_.indexOf("|");

            //Aseen hankintahinta
            weaponCost_ = int(line_.substring(0, position_));

            //Ammusten hinta
            ammoCost_ = int(line_.substring(position_+1));

            //Luodaan uusi "structi" johon luetut tiedot talletetaan.
            weaponData d = new weaponData();
            d.name = name_;
            d.type = type_;
            d.speed = speed_;
            d.damage = damage_;
            d.fireRate = fireRate_;
            d.weaponCost = weaponCost_;
            d.ammoCost = ammoCost_;
            d.owned = false;
            d.ammoCount = 0;
            d.ownedString = "No";

            //Lisätään structi arraylistiin.
            weaponList_.add(d);
          }
        }
        else {
          //Poweruppien lukeminen tiedostosta samaan tapaan kuin aseidenkin
          position_ = line_.indexOf("|");

          //Luetaan tiedostosta powerupin nimi
          name_ = line_.substring(0, position_);
          line_ = line_.substring(position_+1);
          int cost_ = int(line_);

          powerUpData p = new powerUpData();
          p.name = name_;
          p.cost = cost_;
          p.owned = 0;

          powerUpList_.add(p);
        }
      }
    }
  }
}

