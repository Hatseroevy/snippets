class EnemyShip extends Enemy {

  // Vihollis aluksilla on muiden tietojen lisäksi tieto omista ammuksistaan
  private Projectile projectile;
  private ArrayList<Projectile> projectiles_;
  ProjectileFactory pFact_;
  private float targetDeltaX_;
  private float targetDeltaY_;
  private boolean targetXAchieved_, targetYAchieved_;
  private boolean destroyed_;

  EnemyShip(float x, float y, float size) {
    // Kutsutaan kantaluokan rakentajaa
    super(x, y, 75);
    // Alus ei pyöri vielä syntyessään
    setDeltaRotation(0);
    setRotation(0);
    // Ladataan aluksen sprite
    setImage("sprites/phUfo.png");
    pFact_ = new ProjectileFactory();
    projectiles_ = new ArrayList<Projectile>();

    // Aluksi alukset eivät liiku
    setDeltaX(0);
    setDeltaY(0);

    // Arvotaan alukselle pyörimissuunta
    if (int(random(1, 3)) == 1) {
      setDeltaRotation(-0.02);
    }
    else {
      setDeltaRotation(0.02);
    }

    // Arvotaan tavoitenopeudet
    targetDeltaX_ = random(-getMaxSpeed(), getMaxSpeed());
    targetDeltaY_ = random(-getMaxSpeed(), getMaxSpeed());
    targetXAchieved_ = false;
    targetYAchieved_ = false;
  }

  void move() {

    // Jos x suuntainen nopeus on sallitulla välillä
    if (getDeltaX() > targetDeltaX_-0.03 && getDeltaX() < targetDeltaX_+0.03) {
      // Asetetaan nopeus saavutetuksi
      targetXAchieved_ = true;
    }
    else {
      // Muuten muutetaan nopeutta
      if (getDeltaX() < targetDeltaX_) {
        setDeltaX(getDeltaX()+0.01);
      }
      else {
        setDeltaX(getDeltaX()-0.01);
      }
    }
    // Jos y suuntainen nopeus on sallitulla välillä
    if (getDeltaY() > targetDeltaY_-0.03 && getDeltaY() < targetDeltaY_+0.03) {
      // Asetetaan nopeus saavutetuksi
      targetYAchieved_ = true;
    }
    else {
      // Muuten muutetaan nopeutta
      if (getDeltaY() < targetDeltaY_) {
        setDeltaY(getDeltaY()+0.01);
      }
      else {
        setDeltaY(getDeltaY()-0.01);
      }
    }
    // Jos molemmat nopeudet on saavutettu siirrytään ampumaan
    if (targetXAchieved_ && targetYAchieved_) {
      if (projectiles_.size() < 1) {
        // Lasketaan ero pelaajan ja aluksen x -ja y-koordinaattien välillä
        float xDiff = getX() - ship.getX();
        float yDiff = getY() - ship.getY();
        // Lasketaan pelaajan ja aluksen välinen kulma
        float angle = atan2(yDiff, xDiff)-0.5*PI;
        float shootAngle = random(angle-(PI/8), angle+(PI/8));
        projectiles_.add( pFact_.createProjectiles("Regular", getX(), getY(), angle));
      }
      else {
        // Arvotaan uudet tavoite nopeudet
        targetDeltaX_ = random(-getMaxSpeed(), getMaxSpeed());
        targetDeltaY_ = random(-getMaxSpeed(), getMaxSpeed());
        targetXAchieved_ = false;
        targetYAchieved_ = false;
      }
    }

    // Asetetaan uusi paikka
    setPosX(getX()+getDeltaX());
    setPosY(getY()+getDeltaY());

    //Jos alus liikkuu ulos pelialueelta ilmestyy
    //se pelialueen toiselle reunalle
    if (getY() >= height+img_.width/2) {
      setPosY(1-img_.width/2);
    }
    if (getX() >= width+img_.width/2) {
      setPosX(1-img_.width/2);
    }
    if (getY() <= 0-img_.width/2) {
      setPosY((height-1)+img_.width/2);
    }
    if (getX() <= 0-img_.width/2) {
      setPosX((width-1)+img_.width/2);
    }
  }
  // Palauttaa kuinka monta ammusta on ammuttuna
  int projectiles() {
    return projectiles_.size();
  }

  // Palauttaa ammuksen annetulla indeksillä
  Projectile returnProjectile(int i) {
    return projectiles_.get(i);
  }

  // Poistaa ammuksen annetulla indeksillä
  void removeProjectile(int i) {
    projectiles_.remove(i);
  }

  // Poistaa kaikki ammukset
  void removeProjectiles() {
    for (int i = projectiles_.size()-1; i > 0; i--) {
      projectiles_.remove(i);
    }
  }

  // Palauttaa tiedon siitä, onko alus tuhottu
  boolean getDestroyed() {
    return destroyed_;
  }

  // Asettaa aluksen tuhotuksi
  void setDestroyed() {
    destroyed_  = true;
  }

  // Asettaa tavoite x nopeuden
  void setTargetDeltaX(float target) {
    targetDeltaX_ = target;
  }

  // Asettaa tavoite y nopeuden
  void setTargetDeltaY(float target) {
    targetDeltaY_ = target;
  }

  // Asettaa x nopeuden tavoitteen saavutetuksi/saavuttamattomaksi
  void setTargetXAchieved(boolean achieved) {
    targetXAchieved_ = achieved;
  }
  // Asettaa y nopeuden tavoitteen saavutetuksi/saavuttamattomaksi
  void setTargetYAchieved(boolean achieved) {
    targetYAchieved_ = achieved;
  }

  // Palauttaa onko x nopeuden tavoite saavutettu
  boolean getTargetXAchieved() {
    return targetXAchieved_;
  }

  // Palauttaa onko y nopeuden tavoite saavutettu
  boolean getTargetYAchieved() {
    return targetYAchieved_;
  }

  // Palauttaa x nopdeuden tavoitteen
  float getTargetDeltaX() {
    return targetDeltaX_;
  }

  // Palauttaa y nopeuden tavoitteen
  float getTargetDeltaY() {
    return targetDeltaY_;
  }

  // Lisää ammuksen vektoriin
  void addProjectile(Projectile projectile) {
    projectiles_.add(projectile);
  }
}

