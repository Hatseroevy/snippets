public class StorageItem {
  private String type;
  private boolean unlocked;

  // -1 = unlimited
  // >0 = specific
  private int amount;
  private boolean isEnergyUser;
  private int reuseTime;
  private int lastTimeUsed;
}

public class Boost {
  private int boostType; 
  private int pickupTime;
}

class SpaceShip {

  PImage img_;
  PImage thruster_;
  private float rotation_ = 0;
  private float posX_;
  private float posY_;
  private float speedX_;
  private float speedY_;
  private float maxSpeed_;
  private float acceleration_;
  private float size_;
  private String playerName_;
  private String shipNumber_;
  private int points_;
  private int money_;
  private int shields_;
  private int maxShields_;
  private boolean leftPressed;
  private boolean forwardPressed;
  private boolean rightPressed;
  private boolean backPressed;
  // Voidaan vaihtaa myös toiseen nappiin, mutta ilmaisee kun
  // esim tractorbeam on painettuna päälle.
  private boolean spacePressed;
  private boolean isPotato_;
  // Laser/kilpi/tractorbeam/energiaa kuluttava ja nappia pohjassa pitäen toimiva juttu
  private Projectile energyUser_;
  private ArrayList<Projectile> projectiles_;
  private ArrayList<StorageItem> weapons_;
  private ArrayList<StorageItem> boosts_;
  private int maxEnergy_ = 200;
  private int currentEnergy_ = maxEnergy_;
  private int chosenType_ = 0;
  PrintWriter output_;
  private ProjectileFactory pFact_;
  private boolean doubleShot_;
  private boolean tripleShot_;
  private Boost activeBoost_;
  private float indestructible_ = 0;

  SpaceShip(String playerName, String shipNumber, float speed, int shields, int maxShields, float acceleration) {
    imageMode(CENTER);
    playerName_ = playerName;
    if (playerName_.equals("Alfa") == true) {
      img_ = loadImage("sprites/Alus.png");
      thruster_ = loadImage("sprites/thrust.png");
      shipNumber_ = "Alus.png";
      isPotato_ = false;
    }

    else {
      img_ = loadImage("sprites/"+shipNumber);
      shipNumber_ = shipNumber;
      thruster_ = loadImage("sprites/thrust.png");

      if (playerName_.equals("Potator") == true) {
        isPotato_ = true;
      }
    }

    size_ = img_.width-20;
    forwardPressed = false;
    leftPressed = false;
    rightPressed = false;
    projectiles_ = new ArrayList<Projectile>();
    points_ = 0;
    money_ = 0;
    weapons_ = new ArrayList<StorageItem>();
    boosts_ = new ArrayList<StorageItem>();
    pFact_ = new ProjectileFactory();

    maxSpeed_ = speed;
    shields_ = shields;
    maxShields_ = maxShields;
    acceleration_ = acceleration;

    doubleShot_ = false;
    tripleShot_ = false;

    weaponInit();
  }

  //Palautetaan aluksen tietoja
  ArrayList<StorageItem> getBoosts() {
    return boosts_;
  }

  ArrayList<StorageItem> getWeapons() {
    return weapons_;
  }

  String getSelectedWeapon() {
    return weapons_.get(chosenType_).type;
  }

  int getAmountOfSelectedWeapon() {
    return weapons_.get(chosenType_).amount;
  }

  int getCurrentEnergy() {
    return currentEnergy_;
  }

  int getMaxEnergy() {
    return maxEnergy_;
  }

  void setMaxEnergy(int maxEnergy) {
    maxEnergy_ = maxEnergy;
  }

  float getX() {
    return posX_;
  }

  float getY() {
    return posY_;
  }

  float getRotation() {
    return rotation_;
  }

  float getSize() {
    return size_;
  }
  int getPoints() {
    return points_;
  }

  //Muokataan aluksen rahatilannetta
  void setMoney(int money) {
    money_ = money;
  }

  //Palautetaan rahatilanne
  int getMoney() {
    return money_;
  }

  boolean getPotato() {
    return isPotato_;
  }

  //Suojien funktiot
  void setShields(int shields) {
    shields_ =  shields;
  }
  int getShields() {
    return shields_;
  }

  int getMaxShields() {
    return maxShields_;
  }

  //Osumafunktio. Jos palautetaan true, tarkoittaa se sitä,
  //että pelaaja on ottanut osuman ilman suojaa ja alus tällöin
  //tuhoutuu
  boolean hit() {
    --shields_;
    if (shields_ < 0) {
      return true;
    }
    return false;
  }

//Palautetaan aluksen paikka keskelle
  void resetPos() {
    posX_ = width/2;
    posY_ = height/2;
    speedX_ = 0;
    speedY_ = 0;
    rotation_ = 0;
    forwardPressed = false;
    backPressed = false;
    leftPressed = false;
    rightPressed = false;
  }

//Kasvatetaan aluksen pistemäärää
  void addScore() {
    ++points_;
    money_ = money_+10;
  }

//Aluksen pyöritysfunktiot
  void rotateLeft() {
    rotation_ = rotation_-0.1;
  } 

  void rotateRight() {
    rotation_ = rotation_+0.1;
  }

//Ampumisfunktio
  void shoot() {

    Projectile proj = null;
    // valittua ammustyyppiä ei ole varastossa.
    if ( weapons_.get(chosenType_).amount == 0 ) { 
      return;
    }

//Katsotaan voiko aseella ampua taas uudestaan, aseilla on eri ampumisnopeus
    int currentTime = millis();
    if ( (currentTime - weapons_.get(chosenType_).lastTimeUsed) < weapons_.get(chosenType_).reuseTime ) { 
      return;
    }

    // Laserit ja kilvet ovat käytössä vain yksi kappale kerrallaan.
    if ( (weapons_.get(chosenType_).isEnergyUser) ) {
      if ( currentEnergy_ <= 100  ) { 
        return;
      }
      proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, posX_+((size_/2+5)*sin(rotation_)), posY_-((size_/2+5)*cos(rotation_)), rotation_);
      if ( proj != null ) {
        energyUser_ = proj;
        proj = null;
      }
    }
    else {
      Projectile tempProj = null;
      // Miina spawnaa aluksen perään.   
      if ( weapons_.get(chosenType_).type.equals("Mine") ) {
        proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
        posX_+((-size_/2+5)*sin(rotation_)), posY_-((-size_/2+5)*cos(rotation_)), rotation_);

        // Boostitarkistus
        if (activeBoost_ != null ) {

          // Boostina double
          if (activeBoost_.boostType == 1 ) {
            // Vasen ammus
            proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
            posX_+((-size_/2+5)*sin(rotation_))-(20*cos(rotation_)), posY_-((-size_/2+5)*cos(rotation_)+20*sin(rotation_)), rotation_);
            projectiles_.add(proj);

            // Oikea ammus
            proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
            posX_+((-size_/2+5)*sin(rotation_))+(20*cos(rotation_)), posY_-((-size_/2+5)*cos(rotation_)-20*sin(rotation_)), rotation_);
            projectiles_.add(proj);
            proj = null;
          }
          // Boostina triple
          else if (activeBoost_.boostType == 2 ) {
            // Vasen ammus
            tempProj = proj;
            proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
            posX_+((-size_/2+5)*sin(rotation_))-(20*cos(rotation_)), posY_-((-size_/2+5)*cos(rotation_)+20*sin(rotation_)), rotation_+radians(30));
            projectiles_.add(proj);

            // Oikea ammus
            proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
            posX_+((-size_/2+5)*sin(rotation_))+(20*cos(rotation_)), posY_-((-size_/2+5)*cos(rotation_)-20*sin(rotation_)), rotation_-radians(30));
            projectiles_.add(proj);
            proj = tempProj;
          }
        }
      }

      // Muut ammukset spawnaavat nokan eteen
      else {
        proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
        posX_+((size_/2+5)*sin(rotation_)), posY_-((size_/2+5)*cos(rotation_)), rotation_);

        // Boostitarkastus
        if ( activeBoost_ != null ) {
          // Boostina double
          if (activeBoost_.boostType == 1 ) {
            // Vasen ammus
            proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
            posX_+((size_/2+5)*sin(rotation_))+(20*cos(rotation_)), posY_-((size_/2+5)*cos(rotation_)-20*sin(rotation_)), rotation_);
            if (proj != null) {
              projectiles_.add(proj);
            }
            // Oikea ammus
            proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
            posX_+((size_/2+5)*sin(rotation_))-(20*cos(rotation_)), posY_-((size_/2+5)*cos(rotation_)+20*sin(rotation_)), rotation_);
            if (proj != null) {
              projectiles_.add(proj);
            }
            proj = null;
          }
          // Boostina triple
          else if (activeBoost_.boostType == 2 ) {
            // Vasen ammus
            tempProj = proj;
            proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
            posX_+((size_/2+5)*sin(rotation_))+(20*cos(rotation_)), posY_-((size_/2+5)*cos(rotation_)-20*sin(rotation_)), rotation_+radians(30));
            if (proj != null) {
              projectiles_.add(proj);
            }
            // Oikea ammus
            proj = pFact_.createProjectiles(weapons_.get(chosenType_).type, 
            posX_+((size_/2+5)*sin(rotation_))-(20*cos(rotation_)), posY_-((size_/2+5)*cos(rotation_)+20*sin(rotation_)), rotation_-radians(30));
            if (proj != null) {
              projectiles_.add(proj);
            }
            proj = tempProj;
          }
        }
      }
    }
    if (proj != null) {
      projectiles_.add(proj);
    }

    if ( weapons_.get(chosenType_).amount != -1 ) {
      --weapons_.get(chosenType_).amount;
    }

    weapons_.get(chosenType_).lastTimeUsed = millis();
  }


  // Päivitetään käytössä oleva laaseri/kilpi oikeaan asentoon ja paikkaan.
  void updateEnergy() {
    if ( energyUser_ == null ) {
      if ( currentEnergy_ < maxEnergy_) {
        currentEnergy_ += 2;
      }
    }
    if ( energyUser_ != null ) {
      energyUser_.setRotation( rotation_ );
      energyUser_.setX( posX_+((size_/2+5)*sin(rotation_)) );
      energyUser_.setY( posY_-((size_/2+5)*cos(rotation_)) );
      currentEnergy_ -= energyUser_.getEnergyUsage();
      // Jos energia loppuu, ei energiaa kuluttavaa asetta/työkalua voi enää käyttää
    }
    if ( currentEnergy_ <= 0 ) {
      energyUser_ = null;
    }
  }


  void changeProjType( boolean rotForward ) {
    // Rullataan asevalintaa eteen tai taaks, kunnes saadaan seuraava
    // unlockattu.
    if ( rotForward ) {
      ++chosenType_;
      if (chosenType_ >= weapons_.size() ) {
        chosenType_ = 0;
      }
      while ( !weapons_.get (chosenType_).unlocked ) {
        ++chosenType_;
        if (chosenType_ >= weapons_.size() ) {
          chosenType_ = 0;
        }
      }
    }
    else {
      --chosenType_;
      if (chosenType_ < 0 ) {
        chosenType_ = weapons_.size()-1;
      }
      while ( !weapons_.get (chosenType_).unlocked ) {
        --chosenType_;
        if (chosenType_ < 0 ) {
          chosenType_ = weapons_.size()-1;
        }
      }
    }
  }

//Päivitetään aluksen paikka ruudulla
  void updatePosition() {
    if (posY_ >= height+img_.width/2) {
      posY_ = 1-img_.width/2;
    }
    if (posX_ >= width+img_.width/2) {
      posX_= 1-img_.width/2;
    }
    if (posY_ <= 0-img_.width/2) {
      posY_ = (height-1)+img_.width/2;
    }
    if (posX_ <= 0-img_.width/2) {
      posX_= (width-1)+img_.width/2;
    }
    posY_ = posY_-speedY_;
    posX_ = posX_+speedX_;
  }

  void display() {
    if (leftPressed == true) {
      rotateLeft();
    }
    if (rightPressed == true) {
      rotateRight();
    }
    if (forwardPressed == true) {
      float newSpeedX = speedX_+acceleration_*sin(rotation_);
      float newSpeedY = speedY_+acceleration_*cos(rotation_);
      if (newSpeedX < maxSpeed_ && newSpeedX > -maxSpeed_) {
        speedX_ = newSpeedX;
      }
      if (newSpeedY < maxSpeed_ && newSpeedY > -maxSpeed_) {
        speedY_ = newSpeedY;
      }
    }

    if (backPressed == true) {
      float newSpeedX = speedX_-acceleration_*sin(rotation_);
      float newSpeedY = speedY_-acceleration_*cos(rotation_);
      if (newSpeedX < maxSpeed_ && newSpeedX > -maxSpeed_) {
        speedX_ = newSpeedX;
      }
      if (newSpeedY < maxSpeed_ && newSpeedY > -maxSpeed_) {
        speedY_ = newSpeedY;
      }
    }

    updatePosition();
    if (forwardPressed) {
      displayThrust();
    }

    pushMatrix();
    translate(posX_, posY_);    
    rotate(rotation_);
    image(img_, 0, 0, img_.width, img_.height);
    popMatrix();

//Piirretään alukselle suoja osuman jälkeen
    if (millis() - indestructible_ < 1000) {
      fill(#1fd2ff, 250-(millis()-indestructible_)/4);
      noStroke();
      ellipse(posX_, posY_, 50, 50);
      fill(255);
      stroke(0);
    }

    // Päivitetään energia ja sen käyttäjä laser alkamaan oikeasta kohdasta ja 
    // osoittamaan oikeaaan suuntaan.
    updateEnergy();
    if (activeBoost_ != null) {
      updateBoost();
    }
  }

//Piirretään alukselle liekki kun se liikkuu eteenpäin
  void displayThrust() {
    pushMatrix();
    translate(posX_, posY_);    
    rotate(rotation_);
    image(thruster_, 0, 17);
    popMatrix();
  }

//Näppäinten painallukset
  void setPressed(int pressed) {
    if (pressed == 0) {
      forwardPressed = true;
    }
    else if (pressed == 1) {
      leftPressed = true;
    }
    else if (pressed == 2) {
      rightPressed = true;
    }
    else if (pressed == 3) {
      backPressed = true;
    }
    else if (pressed == 4) {
      spacePressed = true;
    }
  }

  void setReleased(int pressed) {
    if (pressed == 0) {
      forwardPressed = false;
    }
    else if (pressed == 1) {
      leftPressed = false;
    }
    else if (pressed == 2) {
      rightPressed = false;
    }
    else if (pressed == 3) {
      backPressed = false;
    }
    else if (pressed == 4) {
      spacePressed = false;
      if ( energyUser_ != null ) {
        energyUser_ = null;
      }
    }
  }

  // Palauttaa kuinka monta ammusta alusta on ammuttuna
  int projectiles() {
    if ( energyUser_ != null ) {
      return projectiles_.size() + 1;
    }
    return projectiles_.size();
  }

  // Palauttaa ammuksen annetulla indeksillä
  Projectile returnProjectile(int i) {
    if ( i >= projectiles_.size() ) {
      return energyUser_;
    }
    return projectiles_.get(i);
  }

  // Poistaa ammuksen annetulla indeksillä
  void removeProjectile(int i) {
    if ( i >= projectiles_.size() ) {
      energyUser_ = null;
      return;
    }
    projectiles_.remove(i);
  }

  // Poistaa parametrinä annetun ammuksen
  void removeProjectile( Projectile removed ) {
    if ( energyUser_ == removed ) {
      energyUser_ = null;
      return;
    }
    for (int i = 0; i < projectiles_.size()-1; ++i) {
      if ( projectiles_.get(i) == removed) {
        projectiles_.remove(i);
      }
    }
  }

  ArrayList<shoppingCart> ammoAmounts() {
    ArrayList<shoppingCart> ammos = new ArrayList<shoppingCart>();
    for (int i = 0; i < weapons_.size(); ++i ) {
      shoppingCart wData = new shoppingCart();
      wData.itemName = weapons_.get(i).type;
      wData.amount = weapons_.get(i).amount;
      ammos.add(wData);
    }
    return ammos;
  }

  int ammoAmountOfType( String type ) {
    for (int i = 0; i < weapons_.size(); ++i ) {
      if (weapons_.get(i).type == type) {
        return weapons_.get(i).amount;
      }
    }
    // Annettua tyyppiä ei löytynyt, -2 ilmoittakoon virheestä.
    // -2 ei ole sallittu ammusten määrä.
    return -2;
  }

  // Unlockaa parametrinä annetun aseen.
  // Palauttaa falsen, jos unlockaus epäonnistui, eli ase oli jo unlockattu tai
  // annettua tyyppiä ei ole aseiden joukossa,
  // muuten palauttaa truen.
  boolean unlockWeapon( String type ) {
    for (int i = 0; i < weapons_.size(); ++i ) {
      if (weapons_.get(i).type.equals(type)) {
        if ( weapons_.get(i).unlocked ) {
          return false;
        }
        weapons_.get(i).unlocked = true;
        return true;
      }
    }
    return false;
  }
  void getCart(ArrayList<shoppingCart> items) {
    for (int i = 0; i < items.size(); ++i) {
      String item = items.get(i).itemName;
      int amount = items.get(i).amount;
      if (unlockWeapon( item )) {
      }
      if (addAmmo(item, amount)) {
      }
      else {
      }
    }
  }
  // Lisää parametrinä annettuun aseeseen annetun määrän panoksia.
  // Jos lisäys epäonnistuu (määrä on neg, ase ei ole unlockattu tai sitä ei ole)
  // palauttaa falsen, muuten true.
  boolean addAmmo( String item, int amount ) {
    if (amount < 0) {
      return false;
    }
    for (int i = 0; i < weapons_.size(); ++i ) {
      if (weapons_.get(i).type.equals(item)) {
        if ( !weapons_.get(i).unlocked ) {
          return false;
        }
        weapons_.get(i).amount += amount;
        return true;
      }
    }
    // itemiä ei löytynyt listasta.
    return false;
  }

  // Toimii samalla tavalla kuin ylempi ammusten lisäys.
  boolean addBoost( String item, int amount ) {
    if (amount < 0) {
      return false;
    }
    for (int i = 0; i < boosts_.size(); ++i ) {
      if (boosts_.get(i).type.equals(item)) {
        if ( boosts_.get(i).unlocked ) {
          return false;
        }
        boosts_.get(i).amount += amount;
        return true;
      }
    }
    return false;
  }

//Boosti poimittu
  void boostPicked(int boost) {
    activeBoost_ = new Boost();
    activeBoost_.boostType = boost;
    activeBoost_.pickupTime = millis();
  }

//Päivitetään boostin tilaa
  void updateBoost() {
    //Boostit käytössä 5 sekunttia
    if (millis() - activeBoost_.pickupTime < 5000) {
      displayBoost(millis() - activeBoost_.pickupTime);
    } 
    //Boosti ei ole enää aktiivinen
    else {
      activeBoost_ = null;
    }
  }

//Näytetään boostin aikapalkki
  void displayBoost(int time) {
    fill(#1fd2ff);
    float timer = float(5000-time)/100;
    if (5000-time < 1000) {
      fill(255, 0, 0);
    }
    rect(posX_-20, posY_-20, timer, 5);

//Jos boosti on kuolemattomuus tulostetaan myös suojakupla
    if (activeBoost_.boostType == 3) {
      fill(#1fd2ff, 150);
      noStroke();
      ellipse(posX_, posY_, 50, 50);
      fill(255);
      stroke(0);
    }
  }

//Kerotaan onko alus kuolematon vai ei
  boolean isImmortal() {
    if (activeBoost_ != null) {
      if (activeBoost_.boostType == 3) {
        return true;
      }
    }
    return false;
  }

  //Kirjoitetaan pelaajan tiedot playerinfo.txt-tiedostoon, josta ne saadaan tarvittaessa
  //luettua hiscoreihin
  void writeToFile() {
    output_ = createWriter("data/info/playerinfo.txt");

    String temp = str(points_)+"|"+shipNumber_+"|"+playerName_;
    output_.println(temp);
    output_.close();
  }

  //Reader lukee tiedostosta aseiden tiedot arraylistiin
  void weaponInit() {

    int pos;
    StorageItem wData;
    //boolean read = true;
    String[] lines;
    String line;

    lines = loadStrings("info/weapondata.txt");
    // Aloitetaan alustamaan tietoja toiselta riviltä, koska
    // ensimmäinen rivi kertoo vain miten data on järjestetty tiedostoon.
    // Data on muotoa: type|unlocked|amount|isEnergyUser|reuseTime
    for (int i = 1; i < lines.length; ++i) {
      line = lines[i];

      wData = new StorageItem();

      // Luetaan tyyppi
      pos = line.indexOf("|");
      wData.type = line.substring(0, pos);

      line = line.substring(pos+1);

      // Luetaan onko ase avattu.
      pos = line.indexOf("|");
      wData.unlocked = boolean(int(line.substring(0, pos)));
      line = line.substring(pos+1);

      // Luetaan ammusten määrä.
      pos = line.indexOf("|");
      wData.amount = int(line.substring(0, pos));
      line = line.substring(pos+1);

      // Luetaan käyttääkö ase energiaa.
      pos = line.indexOf("|");
      wData.isEnergyUser = boolean(int(line.substring(0, pos)));
      line = line.substring(pos+1);

      // Luetaan aseen uudelleenkäyttöaika.
      pos = line.length();
      wData.reuseTime = int(line.substring(0, pos));

      weapons_.add(wData);
    }
  }
  // Asettaa ajan, jolloin alus on osunut johonkin on
  void setIndestructible(float time) {
    indestructible_ = time;
  }

  // Palauttaa ajan jolloin alukseen on osunut luomisajan
  float getIndestructible() {
    return indestructible_;
  }
}

